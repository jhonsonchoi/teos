package com.sccomz.comm.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.sccomz.comm.JavaApp;

public class HiveDBManager {
	public static Connection connectHive() throws SQLException, ClassNotFoundException {
		Class.forName(JavaApp.dbDriverHive());
//		String url = "jdbc:hive2://150.23.13.151:10000/default";
		return DriverManager.getConnection(JavaApp.dbUrlHive(), JavaApp.dbUserHive(), JavaApp.dbPwHive());
	}
	
	public static void close(Connection con, Statement stmt, ResultSet rs) {
		try { rs.close(); } catch (Exception e) {}
		try { stmt.close(); } catch (Exception e) {}
		try { con.close(); } catch (Exception e) {}
	}

}
