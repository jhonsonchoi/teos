package com.sccomz.schedule

import java.sql.Statement

import com.sccomz.rdb.OracleCon
import com.sccomz.schedule.real.ScheduleDaemonSql
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

/**
 * 스케쥴의 예상 워크로드를 구하여 stdout 에 출력한다.
 *
 * java -cp ../../lib/scala-library-2.11.8.jar:../../lib/teos-scala-1.0-SNAPSHOT.jar:../../lib/config-1.3.3.jar:../../lib/ojdbc7.jar:../../lib/slf4j-api-1.7.16.jar:../../lib/slf4j-log4j12-1.7.16.jar:../../lib/log4j-1.2.17.jar:../../conf com.sccomz.schedule.QueueSelector 1
 *
 */
object QueueSelector {

  val logger = LoggerFactory.getLogger(getClass)

  val cf = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: QueueSelector <schedule_id>")
      System.exit(1)
    }

    val schedule_id = args(0)

    logger.info(s"schedule_id=$schedule_id")

    val con = OracleCon.getCon()

    implicit val stmt = con.createStatement()

    try {
      execute(schedule_id)
    } catch {
      case e: Throwable => logger.error(e.getLocalizedMessage, e)
    } finally {
      stmt.close()
      con.close()
    }

    def execute(schedule_id: String)(implicit stmt: Statement): Unit = {
      val qry = ScheduleDaemonSql.selectBinRuCount(schedule_id)
//      println(qry)
      val rs = stmt.executeQuery(qry)
      val result =
        if (rs.next())
//        println(s"SCHEDULE_ID=${rs.getString("SCHEDULE_ID")}, BIN_X_CNT=${rs.getString("BIN_X_CNT")}, BIN_Y_CNT=${rs.getString("BIN_Y_CNT")}, AREA=${rs.getString("AREA")}, RU_CNT=${rs.getString("RU_CNT")}, JOB_WEIGHT=${rs.getString("JOB_WEIGHT")}, JOB_THRESHOLD=${rs.getString("JOB_THRESHOLD")}")
          rs.getInt("JOB_WEIGHT")
        else
          0

      print(result)
    }
  }
}
