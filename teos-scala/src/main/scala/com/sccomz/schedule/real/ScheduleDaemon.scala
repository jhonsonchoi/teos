package com.sccomz.schedule.real

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.lang.Runtime
import java.io.ByteArrayInputStream

import scala.collection.mutable.Map
import scala.collection.mutable.HashMap
import scala.collection._
import scala.sys.process._
import com.sccomz.comm.App
import com.sccomz.rdb.OracleCon
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

/*

import com.sccomz.schedule.real.ScheduleDaemon
ScheduleDaemon.updateBinRuInfo();



ScheduleDaemon.execute();


ExecuteJob.delStepLog("8460178");

ExecuteJob.executePostgreShell("8460178")
*/
object ScheduleDaemon {
  val logger = LoggerFactory.getLogger(ScenarioExecutor.getClass)

  val cf = ConfigFactory.load()

  val processor_1_command = cf.getString("processor-1.command")

  def main(args: Array[String]): Unit = {
    
    execute()
    //updateBinRuInfo();
  }

  def execute(): Unit = {
    try {
      while(true) {

        updateBinRuInfo()
        executeJob()

        Thread.sleep(1000*3)
      }

      println("execute end")

    } catch {
      case e : Exception =>
        e.printStackTrace()
    }
  }

  /*
   * 메인작업 : 잡을 생성한다
   *
   * todo 멀티쓰레드 워크플로 실행
   */
  def executeJob(): Unit = {
    val qry = ScheduleDaemonSql.selectSchedule10001()
    logger.debug(qry)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()
    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val wfId = rs.getString("SCHEDULE_ID")
      val scenarioId = rs.getString("SCENARIO_ID")
      logger.info(s"SCHEDULE_ID=$wfId : SCENARIO_ID=$scenarioId")
      //ExecuteJob.execute();
      //var scheduleId = "8462895";
      //val process = Process(s"""scala -classpath d:/fframe/workspace/teos/bin/teos.jar com.sccomz.schedule.real.ExecuteJob ${scheduleId}""").lineStream;
      val command = s"""${processor_1_command} ${wfId} ${scenarioId}"""
      println(command)
      //      val process = Process(command).lineStream;
//      ExecuteJob.execute(wfId, scenarioId)
      //println("process="+process);
    }

    rs.close()
    stmt.close()
    con.close()
  }

  /*
   * 사전작업 : 스케줄 테이블에 Bin갯수, Ru갯수, 빌딩크기 산출후 잡가중치를 저장한다
   */
  def updateBinRuInfo(): Unit = {

    val list = mutable.MutableList[WFRequest]()

    //------------------------------------------------------------------
    // [갱신:SCHEDULE, 삽입:SCHEDULE_EXT] 스케줄대상건의 BIN갯수 갱신
    //------------------------------------------------------------------
    // PROCESS_CD 10001(분석요청)이면서 BIN갯수 미세팅건이나 SCHEDULE_EXT 미생생된 경우

    var qry = ScheduleDaemonSql.selectBinRuCount("");
    logger.debug(qry)

    val con = OracleCon.getCon()
    var stmt = con.createStatement()

    val rs = stmt.executeQuery(qry)
    while (rs.next()) {
      list += WFRequest(rs.getString("SCHEDULE_ID"), rs.getString("BIN_X_CNT"), rs.getString("BIN_Y_CNT"), rs.getString("AREA"), rs.getString("RU_CNT"), rs.getString("JOB_WEIGHT"), rs.getString("JOB_THRESHOLD"))
    }

    rs.close()

    var scheduleId = ""; var binXCnt = ""; var binYCnt = ""; var ruCnt = ""; var jobWeight = ""; var jobThreshold = "";

    list.foreach { req =>
      //[갱신:SCHEDULE]
      qry = ScheduleDaemonSql.updateScheduleBinRuCnt(req.wfId, req.binXCnt, req.binYCnt, req.ruCnt)
      logger.debug(qry)
      stmt.execute(qry)

      //[삭제:SCHEDULE_EXT]
      qry = ScheduleDaemonSql.deleteScheduleExt(req.wfId)
      logger.debug(qry)
      stmt.execute(qry)

      //[삽입:SCHEDULE_EXT]
      qry = ScheduleDaemonSql.insertScheduleExt(req.wfId, req.jobWeight, req.jobThreshold)
      logger.debug(qry)
      stmt.execute(qry)
    }

    stmt.close()
    con.close()

  }



//def getProcessIds(key: String): util.ArrayList[Int] ={
//    import java.io.{BufferedReader, InputStreamReader}
//    var pids = new util.ArrayList[Int]()
//    val re = false
//    try {
//        val command = s"ps aux | grep '${key}'"
//        val process = Runtime.getRuntime.exec( Array("bash", "-c", command) )
//        val br = new BufferedReader(new InputStreamReader(process.getInputStream))
//        var line: String = br.readLine()
//        while (line != null){
//            println(line)
//            val tInputStringStream = new ByteArrayInputStream(line.getBytes)
//            // let string as input stream read the process id
//            val scanner = new Scanner(tInputStringStream)
//            val user = scanner.next()
//            val pid = scanner.nextInt()
//            scanner.close()
//            pids.add(pid)
//            line = br.readLine()
//        }
//        br.close()
//        pids
//    } catch {
//        case e: Exception =>
//        System.out.println(e.toString)
//        null
//    }
//}
//def killProcesses(key: String): Int={
//    var killNumbers = 0
//    val pids: List[Int] = getProcessIds(key)
//    println( pids )
//    pids.foreach(pid => {
//        val process = Runtime.getRuntime().exec(Array("bash", "-c", s"kill -9 ${pid}"))
//        if( process.waitFor() == 0)
//        	killNumbers += 1
//    })
//    killNumbers
//}
//killProcesses("infrastructure.TranswarpServer")


}

/**
 */
case class WFRequest(wfId: String, binXCnt: String, binYCnt: String, area: String, ruCnt: String, jobWeight: String, jobThreshold: String)