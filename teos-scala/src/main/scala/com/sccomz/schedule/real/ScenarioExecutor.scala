package com.sccomz.schedule.real

import com.sccomz.rdb.OracleCon
import org.slf4j.LoggerFactory

/**
 * 시나리오 실행 진입점
 *
 * 시나리오의 워크플로우(스케쥴) 목록을 구해, 동시에 실행
 */
object ScenarioExecutor {

  val logger = LoggerFactory.getLogger(ScenarioExecutor.getClass)

  /**
   * 시나리오의 워크플로우 목록 취득
   *
   * @param scenarioId 시나리오 아이디
   * @return 워크플로우 아이디 목록
   */
  def workflows(scenarioId: String): Array[String] = {
    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    val qry = ExecuteJobSql.selectScenarioInfo(scenarioId)
    val rs = OracleCon.executeQuery(stmt, qry)

    var wfIds = Array.empty[String]

    while (rs.next()) {
      wfIds = wfIds :+ rs.getString("SCHEDULE_ID")
    }

    rs.close()
    stmt.close()
    con.close()

    wfIds
  }

  def main(args: Array[String]): Unit = {
    val scenarioId = "5104574" // if (args.length < 1) "" else args(0);

    //---------------------------------------------------------------------------------------------
    println("ExecuteScenario : scenarioId = " + scenarioId);
    //---------------------------------------------------------------------------------------------

    val wfIds = workflows(scenarioId)

    // todo 멀티쓰레드 사용
    wfIds.foreach{
      wfId =>

        logger.info(s"schedule_id=$wfId, scenario_id=$scenarioId")

//        ExecuteJob.execute(wfId, scenarioId)
    }
  }

}
