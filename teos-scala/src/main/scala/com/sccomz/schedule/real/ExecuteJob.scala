package com.sccomz.schedule.real

import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement
import java.lang.Runtime
import java.io.File
import java.io.ByteArrayInputStream
import java.util.logging.Logger

import scala.collection.mutable.{HashMap, ListBuffer, Map}
import scala.collection._
import scala.sys.process._
import com.sccomz.comm.App
import com.sccomz.etl.extract.oracle.ExtractOraManager
import com.sccomz.etl.load.LoadPostManager
import com.sccomz.etl.load.LoadHdfsManager
import com.sccomz.etl.extract.post.ExtractLoadPostManager
import com.amazonaws.services.simpleworkflow.flow.core.TryCatch
import com.sccomz.analysis.d2.Make2DBinFile
import com.sccomz.etl.cache.LosResultCache
import com.sccomz.rdb.OracleCon
import com.typesafe.config.ConfigFactory

 

/*

import com.sccomz.schedule.real.ExecuteJob

# ru 6
ExecuteJob.execute("8463246");
ps -ef | grep '5113972'


ExecuteJob.execute("8460178");
ps -ef | grep 5105173

ExecuteJob.execute("8463290");
ps -ef | grep '5115566'




ExecuteJob.executePostgreShell("8460064");
ExecuteJob.executeEtlOracleToHdfs("8460064");
ExecuteJob.execute("8460064");

ExecuteJob.executeEtlOracleToHdfs("8463235","SC051"); 

ExecuteJob.executeEtlOracleToHdfs("8460966","SC001");
ExecuteJob.executeEtlOracleToHdfs("8460968","SC001");
ExecuteJob.delStepLog("8460178");
ExecuteJob.executePostgreShell("8460178")

sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh 5113766

*/

object ExecuteJob {

  //val logger = LoggerFactory.getLogger("StatDailyBatch")
//  var scheduleId = "";
//  var scenarioId = "";
//  var typeCd = "";
//  var currentTypeStepCd = "01";

  

  def main(args: Array[String]): Unit = {
    val scheduleId = if (args.length < 1) None else Some(args(0))
    val scenarioId = if (args.length < 2) None else Some(args(1))
    //---------------------------------------------------------------------------------------------
    if (scheduleId.isDefined) println("ExecuteJob : scheduleId = " + scheduleId);
    if (scenarioId.isDefined) println("ExecuteJob : scenarioId = " + scenarioId);
    //---------------------------------------------------------------------------------------------

    if (scenarioId.isDefined)
      execute(scheduleId.get, scenarioId.get)
    else if (scheduleId.isDefined)
      execute(scheduleId.get)
  }

  def delStepLog(scheduleId:String) = {
    val qry = ExecuteJobSql.deleteScheduleStepAll(scheduleId)
    val con = OracleCon.getCon()
    val stmt = con.createStatement()
    OracleCon.executeUpdate(stmt, qry)
    stmt.close()
    con.close()
  }

  /**
   * 구 스텝은 정상 상태로 변경
   * 신 스텝은 추가
   * @param scheduleId
   * @param typeStepCd
   * @return
   */
  def insStepLog(scheduleId: String, typeStepCd: String): String = {
//    var prevTypeStepCd = if(typeStepCd=="01") "00" else if(typeStepCd=="02") "01" else if(typeStepCd=="03") "02" else if(typeStepCd=="04") "03" else if(typeStepCd=="05") "04"
//    else if(typeStepCd=="06") "05" else if(typeStepCd=="07") "06" else if(typeStepCd=="08") "07" else if(typeStepCd=="09") "08" else if(typeStepCd=="10") "09" else "00"

    val prevTypeStepCd =
    typeStepCd match {
      case "01" => "00"
      case "02" => "01"
      case "03" => "02"
      case "04" => "03"
      case "05" => "04"
      case "06" => "05"
      case "07" => "06"
      case "08" => "07"
      case "09" => "08"
      case "10" => "09"
      case _ => "00"

    }

   val con = OracleCon.getCon()
    val stmt = con.createStatement()

    var qry = ExecuteJobSql.insertScheduleStep(scheduleId, typeStepCd)
    OracleCon.executeUpdate(stmt, qry)

    qry = ExecuteJobSql.updateScheduleStep(scheduleId, prevTypeStepCd, "정상")
    OracleCon.executeUpdate(stmt, qry)

    stmt.close()
    con.close()

    typeStepCd
  }

  def updStepErrLog(scheduleId:String, typeStepCd: String) = {
    val qry = ExecuteJobSql.updateScheduleStep(scheduleId, typeStepCd, "오류")

    val con = OracleCon.getCon()
    val stmt = con.createStatement()
    OracleCon.executeUpdate(stmt, qry)
    stmt.close()
    con.close()
  }

  def updScheduleProcessCd(scheduleId:String, processCd:String, processMsg:String) = {
    val qry = ExecuteJobSql.updateScheduleProcessCd(scheduleId, processCd, processMsg)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    OracleCon.executeUpdate(stmt, qry)

    stmt.close()
    con.close()
  }

  def selMaxStep(scheduleId:String) = {
    val qry = ExecuteJobSql.selectScheduleStep(scheduleId)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    val rs = OracleCon.executeQuery(stmt, qry)
    rs.next()
    val result = rs.getString("MAX_TYPE_STEP_CD")

    rs.close()
    stmt.close()
    con.close()

    result
  }

  def selectTypeCd(scheduleId:String) = {
    val qry = ExecuteJobSql.selectScheduleInfo(scheduleId)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    val rs = OracleCon.executeQuery(stmt, qry)
    rs.next()
    val result = rs.getString("TYPE_CD")

    rs.close()
    stmt.close()
    con.close()

    result
  }

  def selectScenario(scenarioId: String): MyScenario = {
    val qry = ExecuteJobSql.selectScenarioInfo(scenarioId)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    val rs = OracleCon.executeQuery(stmt, qry)

    val schedules = ListBuffer.empty[MySchedule]

    while (rs.next()) {

      val scheduleId = rs.getString("SCHEDULE_ID")
      val typeCode = rs.getString("TYPE_CD")
      schedules += MySchedule(scheduleId, scenarioId, typeCode)
    }

    rs.close()
    stmt.close()
    con.close()

    MyScenario(scenarioId, schedules.toList)
  }


  def selectRUByScenario(scenarioId: String): List[MyRU] = {
    val qry = ExecuteJobSql.selectRUInfo(scenarioId)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    val rs = OracleCon.executeQuery(stmt, qry)

    val rus = ListBuffer.empty[MyRU]

    while (rs.next()) {

      val ruId = rs.getString("RU_ID")
      rus += MyRU(ruId)
    }

    rs.close()
    stmt.close()
    con.close()

    rus.toList
  }


  def selectSchedule(scheduleId: String): MySchedule = {
    val qry = ExecuteJobSql.selectScheduleInfo(scheduleId)

    val con = OracleCon.getCon()
    val stmt = con.createStatement()

    val rs = OracleCon.executeQuery(stmt, qry)

    rs.next()

    val scenarioId = rs.getString("SCENARIO_ID")
    val typeCode = rs.getString("TYPE_CD")

    rs.close()
    stmt.close()
    con.close()

    MySchedule(scheduleId, scenarioId, typeCode)
  }

  /**
   * 워크플로우 실행 편의기능.
   * 시나리오 아디 없이 워크플로 아이디만 있을 경우
   *
   * @param wfId
   */
  def execute(wfId:String): Unit = {
    val schedule = selectSchedule(wfId)
    execute(wfId, schedule.scenarioId)
  }

  /**
   * 워크플로우 실행
   *
   * @param wfId
   * @param scenarioId
   */
  def execute(wfId:String, scenarioId: String): Unit = {
    //---------------------------------------------------------------------------------------------
    println(s"ExecuteJob : scheduleId = ${wfId}, scenarioId = ${scenarioId}");
    //---------------------------------------------------------------------------------------------

    var currentStep = ""

    try {
      // todo 한번에 시나리오아이디와 타입코드를 얻도록 변경
      val typeCd = selectTypeCd(wfId)
      
      updScheduleProcessCd(wfId,"20003","분석중")
      
      delStepLog(wfId);
      currentStep = insStepLog(wfId, "00")

/*
      println("01 : ETL OracleToHdfs 실행")
      currentStep = insStepLog(scheduleId, "01")
      executeEtlOracleToHdfs(scheduleId, currentStep, scenarioId);
      
      println("02 : ETL OracleToPostgre 실행")
      currentStep = insStepLog(scheduleId, "02")
      executeEtlOracleToPostgre(scheduleId, currentStep, scenarioId);
*/

      //executeEtlHdfsToPostgre(scheduleId,typeStepCd);
//      var isLoof = true;
/*
      println("03 : Postgre shell 실행")
      currentStep = insStepLog(scheduleId, "03");
*/
      while(currentStep != "99") {
        currentStep = executeStep(currentStep, wfId, scenarioId)

        currentStep = selMaxStep(wfId)

        Thread.sleep(1000*3);
      }

      updScheduleProcessCd(wfId,"20004","분석완료");


/*
        currentStep match {
          case "03" =>
            executePostgreShell(scenarioId, scenarioId)
            println("04 : ETL Postgre to HDFS 실행")
            insStepLog(scheduleId, "04")
          case "04" =>
            executeEtlPostgreToHdfs(scheduleId, scenarioId)
            println("05 : Spark Eng 실행")
            insStepLog(scheduleId, "05")
          case "05" =>
            executeSparkEngJob(scheduleId, scenarioId)
            println("06 : MakeBin 실행")
            insStepLog(scheduleId, "06")
          case "06" =>
            executeSparkMakeBinFile(scheduleId, scenarioId)

            updScheduleProcessCd(scheduleId,"20004","분석완료");
            isLoof=false
        }
*/

//      }
    } catch {
      case ex: Exception => {updStepErrLog(wfId, currentStep);println(ex);sys.exit()}
    }
  }

  /**
   * 워크플로 정의
   *
   * @param currentStep
   * @param wfId
   * @param scenarioId
   * @return
   */
  def executeStep(currentStep: String, wfId: String, scenarioId: String) = {
    println(s"executeStep step=${currentStep}, scheduleId=${wfId}, scenarioId=${scenarioId}")

    val nextStep = currentStep match {
      case "00" =>
        insStepLog(wfId, "01")
      case "01" =>
        executeEtlOracleToHdfs(wfId, currentStep, scenarioId)
        insStepLog(wfId, "02")
      case "02" =>
        executeEtlOracleToPostgre(wfId, currentStep, scenarioId)
        insStepLog(wfId, "03")
      case "03" =>
        executePostgreShell(scenarioId, scenarioId)
        insStepLog(wfId, "04")
      case "04" =>
        executeEtlPostgreToHdfs(wfId, scenarioId)
        insStepLog(wfId, "05")
      case "05" =>
        insStepLog(wfId, "06")
      case "06" =>
        executeSparkMakeBinFile(wfId, scenarioId)
        insStepLog(wfId, "99")
    }

    nextStep
  }

  def executeEtlOracleToPostgre(scheduleId:String, scenarioId: String, typeStepCd: String): Unit = {
    println("***** executeEtlOracleToPostgre")
    try {
//      ExtractOraManager.extractOracleToPostgreIns(scheduleId);
//      LoadPostManager.oracleToPostgreAll(scheduleId);
    } catch {
      case e:Throwable=>
        e.printStackTrace()
        updStepErrLog(scheduleId, typeStepCd);
    }
  }
  def executeEtlOracleToHdfs(scheduleId:String, scenarioId: String, typeStepCd: String): Unit = {

    println("***** executeEtlOracleToHdfs")
    try {
//      ExtractOraManager.extractOracleToHadoopCsv(scheduleId)
//      LoadHdfsManager.oracleToHdfs(scheduleId, scenarioId);
    } catch {
      case e:Throwable=>
        e.printStackTrace()
        updStepErrLog(scheduleId, typeStepCd)
    }
  }
  def executeEtlHdfsToPostgre(scheduleId:String, scenarioId: String, typeStepCd:String): Unit = {
    println("***** executeEtlHdfsToPostgre")
    try {
      LosResultCache.execute(scheduleId);
    } catch {
      case ex: Throwable => throw ex
    }
  }

  def executePostgreShell(scheduleId: String, scenarioId: String): Unit = {
    println("executePostgreShell start");
    var str = "";    
    //var res0 = Process(s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 Seq('pkill', '-9', '-ef', 'anal_los_job_dis.sh 5105173')").lineStream;
    
    //var res0 = Process(s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 pkill -9 -ef 'anal_los_job_dis.sh 5105173'").lineStream;
    //var res0 = Process(s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 pkill -9 -ef 5105173").lineStream;

    
    try {
      str = s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 pkill -9 -ef 'anal_los_job_dis.sh ${scenarioId}'";
      println(str); var res0 = Process(str).lineStream;
    } catch {
      case ex: Exception => println("")
    } 
    
    str = s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh ${scenarioId}";
    println(str); var res = Process(str).lineStream;
    
    // proc = Process(Seq("java", "-jar", appJar.toString), baseDir)
    
    //var res = Process(s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh 5105173").lineStream;
    //pkill -9 -ef 'anal_los_job_dis.sh 5105173'
    //gis01/bin/anal_los_job_dis.sh 5105173 
    // ps -ef | grep 'anal_los_job_dis.sh 5105173'
    // kill -9 `ps -ef | grep 'anal_los_job_dis.sh 5105173' | awk '{print $2}'`
    //sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh 5105173
    
    //var res = Process(s"sh /home/icpap/sh/execPostgre.sh ${scheduleId}").lineStream;
    //var scheduleId = "8460064";
    //var scenarioId = "5104574";

    //var res = Process(s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 hadoop fs -df -h").lineStream;

    //var res = Process(s"ssh postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh ${scenarioId}").lineStream;
    //var res = Process(s"ssh icpap@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh ${scheduleId}").lineStream;
    //var res = Process(s"ssh icpap@teos-cluster-dn1 /home/icpap/sh//execPostgre.sh 111").lineStream;
    // sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 hadoop fs -df -h
    // res.waitFor()
    //ssh postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh 5113566
    //var logFile = new File(s"sh /home/icpap/sh/${scheduleId}_end_log.txt");
    var logFile = new File(s"/home/icpap/log/${scheduleId}_end_log.txt");

    //8463233	5113566

    //chmod 777 /gis01/bin/anal_los_job_dis.sh
    //var res = Process(s"sshpass postgres@teos-cluster-dn1 /gis01/bin/alos_job_dis.sh 11").lineStream;
    //sshpass postgres@teos-cluster-dn1 /gis01/bin/alos_job_dis.sh 11
    //sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 ls -al
    //sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 sh /gis01/bin/alos_job_dis.sh 11
    //sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 cat /gis01/bin/alos_job_dis.sh
    //var res = Process(s"sshpass postgres@teos-cluster-dn1 /gis01/bin/alos_job_dis.sh 11").lineStream;
    //sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh 5113766

    //while(!logFile.exists()){
    //  Thread.sleep(1000*1);
    //  println("ing...");
    //}

    println("executePostgreShell end");
  }

  def executeEtlPostgreToHdfs(scheduleId:String, scenarioId: String): Unit = {
    ExtractLoadPostManager.monitorJobDis(scheduleId, scenarioId,true);
  }

  def executeSparkEngJob(scheduleId:String, scenarioId: String): Unit = {
    // com.sccomz.job.spark
    //EngManager.execute(scheduleId);
    val process = Process(s"""spark2-submit --jars /home/icpap/lib/ojdbc7.jar,/home/icpap/lib/postgresql4.jar,/home/icpap/lib/hiveJdbc11.jar --class com.sccomz.schedule.real.ExecuteJob /home/icpap/bin/teos.jar ${scheduleId} ${scenarioId}""").lineStream;
    
  }

  def executeSparkMakeBinFile(scheduleId:String, scenarioId: String): Unit = {
    // com.sccomz.serialize
//     MakeBinFile.executeEngResult(scheduleId);
  }

}

/**
 *
 * @param id
 * @param scenarioId
 * @param scheduleType
 */
case class MySchedule(id: String, scenarioId: String, scheduleType: String)

case class MyScenario(id: String, schedules: List[MySchedule])

case class MyRU(id: String)