package com.sccomz.schedule.real

object ExecuteJobSql {

  def insertScheduleStep(scheduleId:String, typeStepCd:String) = {
    s"""
      |INSERT INTO SCHEDULE_STEP
      |SELECT
      |       A.SCHEDULE_ID   AS SCHEDULE_ID
      |     , A.TYPE_CD       AS TYPE_CD
      |     , '${typeStepCd}' AS TYPE_STEP_CD
      |     , SYSDATE         AS START_DT
      |     , SYSDATE         AS END_DT
      |     , 0               AS PROCESS_TIME
      |     , '진행중'          AS PROCESS_LOG
      |     , SYSDATE         AS REG_DT
      |     , 'ADMIN'         AS REG_USER_ID
      |     , SYSDATE         AS MOD_DT
      |     , 'ADMIN'         AS MOD_USER_ID
      |FROM   SCHEDULE A
      |WHERE  A.SCHEDULE_ID = ${scheduleId}
      |""".stripMargin
  }

  def selectScheduleStep(scheduleId:String) = {
    s"""
      |  SELECT TYPE_STEP_CD
      |  FROM   SCHEDULE_STEP
      |  WHERE  SCHEDULE_ID = ${scheduleId}
      |  """.stripMargin
  }

  def selectScenarioStep(scenarioId:String) = {
    s"""
      |SELECT TYPE_STEP_CD
      |FROM   SCENARIO_STEP
      |WHERE  SCENARIO_ID = ${scenarioId}
      |""".stripMargin
  }

  def insertScenarioStep(scenarioId:String, typeStepCd:String) = {
    s"""
      |INSERT INTO SCENARIO_STEP
      |SELECT
      |       A.SCENARIO_ID   AS SCENARIO_ID
      |     , 'D'             AS TYPE_CD
      |     , '${typeStepCd}' AS TYPE_STEP_CD
      |     , SYSDATE         AS START_DT
      |     , SYSDATE         AS END_DT
      |     , 0               AS PROCESS_TIME
      |     , '진행중'          AS PROCESS_LOG
      |     , SYSDATE         AS REG_DT
      |     , 'ADMIN'         AS REG_USER_ID
      |     , SYSDATE         AS MOD_DT
      |     , 'ADMIN'         AS MOD_USER_ID
      |FROM   SCENARIO A
      |WHERE  A.SCENARIO_ID = ${scenarioId}
      |""".stripMargin
  }

  def updateScenarioStep(scenarioId:String, typeStepCd:String, processLog:String) = {
    s"""
      |UPDATE SCENARIO_STEP
      |SET    END_DT       = SYSDATE
      |     , PROCESS_TIME = TRUNC(ROUND((SYSDATE-START_DT)*24*60*60))
      |     , PROCESS_LOG  ='${processLog}'
      |     , MOD_DT       = SYSDATE
      |     , TYPE_STEP_CD ='${typeStepCd}'
      |WHERE  SCENARIO_ID  = ${scenarioId}
      |""".stripMargin
  }

  def deleteScenarioStep(scenarioId:String) = {
    s"""
      |DELETE FROM SCENARIO_STEP
      |WHERE  SCENARIO_ID = ${scenarioId}
      |""".stripMargin
  }

  def selectScenarioInfo(scenarioId:String) = {
    s"""
SELECT SCHEDULE_ID
     , TYPE_CD
FROM   SCHEDULE
WHERE  SCENARIO_ID = ${scenarioId}
"""
  }

  def selectRUInfo(scenarioId:String) = {
    s"""
SELECT RU_ID
FROM   RU
WHERE  SCENARIO_ID = ${scenarioId}
"""
  }

  def getSchedule(scheduleId:String) = {
    s"""
      |SELECT *
      |FROM   SCHEDULE
      |WHERE  SCHEDULE_ID = ${scheduleId}
      |""".stripMargin
}

  def selectScheduleInfo(scheduleId:String) = {
    s"""
       |SELECT SCHEDULE_ID
       |     , SCENARIO_ID
       |     , TYPE_CD
       |FROM   SCHEDULE
       |WHERE  SCHEDULE_ID = ${scheduleId}
       |""".stripMargin
  }

  def selectSchedulesInfo(scenarioId:String) = {
    s"""
      |SELECT SCHEDULE_ID
      |     , SCENARIO_ID
      |     , TYPE_CD
      |FROM   SCHEDULE
      |WHERE  SCENARIO_ID = ${scenarioId}
      |""".stripMargin
  }

  def updateScheduleStep(scheduleId:String, typeStepCd:String, processLog:String) = {
    s"""
      |UPDATE SCHEDULE_STEP
      |SET    END_DT       = SYSDATE
      |     , PROCESS_TIME = TRUNC(ROUND((SYSDATE-START_DT)*24*60*60))
      |     , PROCESS_LOG  ='${processLog}'
      |     , MOD_DT       = SYSDATE
      |     , TYPE_STEP_CD ='${typeStepCd}'
      |WHERE  SCHEDULE_ID  = ${scheduleId}
      |""".stripMargin
  }

  def updateScheduleProcessCd(scheduleId:String, processCd:String, processMsg:String) = {
    s"""
      |UPDATE SCHEDULE
      |SET    PROCESS_CD   ='${processCd}'
      |     , PROCESS_MSG  ='${processMsg}'
      |     , MODIFY_DT    = SYSDATE
      |WHERE  SCHEDULE_ID  = ${scheduleId}
      |""".stripMargin
  }

  def updateScenarioState(scenarioId:String, processCd:String, processMsg:String) = {
    s"""
      |UPDATE SCHEDULE
      |SET    PROCESS_CD   ='${processCd}'
      |     , PROCESS_MSG  ='${processMsg}'
      |     , MODIFY_DT    = SYSDATE
      |WHERE  SCENARIO_ID  = ${scenarioId}
      |""".stripMargin
  }


def deleteScheduleStep(scheduleId:String, typeStepCd:String) = {
s"""
DELETE FROM SCHEDULE_STEP
WHERE  SCHEDULE_ID = ${scheduleId}
AND    TYPE_CD     ='${typeStepCd}'
"""
}

  def deleteScheduleStep(scheduleId:String) = {
    s"""
      |DELETE FROM SCHEDULE_STEP
      |WHERE  SCHEDULE_ID = ${scheduleId}
      |""".stripMargin
  }

  def deleteScheduleStepByScenario(scenarioId:String) = {
    s"""
      |DELETE FROM SCHEDULE_STEP
      |WHERE  SCHEDULE_ID IN (SELECT SCHEDULE_ID FROM SCHEDULE WHERE SCENARIO_ID = ${scenarioId})
      |""".stripMargin
  }

def deleteScheduleStepAll(scheduleId:String) = {
s"""
DELETE FROM SCHEDULE_STEP
WHERE  SCHEDULE_ID = ${scheduleId}
"""
}


}