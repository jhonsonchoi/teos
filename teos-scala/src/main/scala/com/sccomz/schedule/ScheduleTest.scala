package com.sccomz.schedule

import com.sccomz.analysis.AbstractJob
import com.sccomz.rdb.OracleCon
import com.sccomz.wf.{HiveSchema, MyEnv}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession

object ScheduleTest extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 2) {
      println("usage: ScheduleMain <schedule_id> <table>")
      System.exit(1)
    }

    val scheduleId = args(0)
    val table = args(1)

    logInfo(s"schedule_id=$scheduleId table=$table")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val schedule = MyEnv.selectSchedule(scheduleId)


    println(s"RESULT_NR_2D_LOS_RU=$RESULT_NR_2D_LOS_RU")

    implicit val spark = SparkSession.builder()
      .appName(s"ScheduleMain $scheduleId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    try {

      val df = spark.sql(s"select count(*) from $table where schedule_id=$scheduleId")
      df.show()

    } finally {
      try { stmt.close() }
      try { conn.close() }
      spark.close()
    }
  }

}
