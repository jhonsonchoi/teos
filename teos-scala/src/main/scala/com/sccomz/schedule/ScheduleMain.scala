package com.sccomz.schedule

import java.sql.Statement

import com.sccomz.analysis._
import com.sccomz.analysis.AbstractJob
import com.sccomz.rdb.OracleCon
import com.sccomz.serialize.ByteUtil
import com.sccomz.wf.{HiveSchema, MyEnv}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession


class ScheduleMain extends AbstractJob with HiveSchema with Logging {

  def execute2D(scheduleId: String)(implicit stmt: Statement, spark: SparkSession): Unit = {
    // Analysis
    logInfo(s"Phase(${scheduleId}): LOS")

    d2.LOS.executeScenario(scheduleId)
    d2.PathLoss.executeSite(scheduleId)
    d2.PathLoss.executeScenario(scheduleId)
    d2.RSRPPilot.executeSite(scheduleId)
    d2.RSRP.executeSite(scheduleId)
    d2.RSRP.executeScenario(scheduleId)
    d2.BestServer.executeScenario(scheduleId)
    d2.RSSI.executeSite(scheduleId)
    d2.RSSI.executeScenario(scheduleId)
    d2.SINR.executeSite(scheduleId)
    d2.SINR.executeScenario(scheduleId)
    d2.Throughput.executeScenario(scheduleId)
  }

  def make2DBinFiles(scheduleId: String, resultRoot: String)(implicit stmt: Statement, spark: SparkSession): Unit = {

    // Make bin files
    logInfo(s"Phase(${scheduleId}): Making bin files")

    val schedule = MyEnv.getSchedule(scheduleId)
    val scenario = MyEnv.getScenario(schedule.scenarioId)

//    val userId = "SYS" // todo get it from oracle
    val ruInfo = MyEnv.makeResultPath(scenario.id)
//    val ruInfo = Map.empty[String, RU] // MakeFolder.makeResultPath(scheduleId)
//    val targetPath = s"$resultRoot/${DateUtil.getDate("yyyyMMdd")}/$userId/${scheduleId}"
    val targetPath = s"$resultRoot/${scheduleId}"

    val ruPathInfo = ruInfo.map { ru =>
      (ru._1, s"ENB_${ru._2.enbId}/PCI_${ru._2.pci}_PORT_${ru._2.pciPort}_${ru._2.id}")
    }

    // separate this function from the job
//    val ruInfo = MakeFolder.makeResultPath(scheduleId)
//    val sectorPath = ruInfo.getOrElse("SECTOR_PATH", "")

    // by schedule

    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "LOS", RESULT_NR_2D_LOS, "LOS", "Int")
    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "PATHLOSS", RESULT_NR_2D_PATHLOSS, "PATHLOSS", "Float")
    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "BESTSERVER", RESULT_NR_2D_BESTSERVER, "RU_SEQ", "Int")
    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "PILOT_EC", RESULT_NR_2D_RSRP, "RSRP", "Float")
    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "RSSI", RESULT_NR_2D_RSSI, "RSSI", "Float")
    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "C2I", RESULT_NR_2D_SINR, "SINR", "Float")
    d2.Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "DOWNLINK_THROUGHPUT", RESULT_NR_2D_THROUGHPUT, "THROUGHPUT", "Float")

    // by ru

    d2.Make2DBinFile.makeRuResult(scheduleId, targetPath, "LOS", RESULT_NR_2D_LOS_RU, "VALUE", "Int", ruInfo, ruPathInfo)
    d2.Make2DBinFile.makeRuResult(scheduleId, targetPath, "PATHLOSS", RESULT_NR_2D_PATHLOSS_RU, "PATHLOSS", "Float", ruInfo, ruPathInfo)
    d2.Make2DBinFile.makeRuResult(scheduleId, targetPath, "PILOT_EC", RESULT_NR_2D_RSRP_RU, "RSRP", "Float", ruInfo, ruPathInfo)
    d2.Make2DBinFile.makeRuResult(scheduleId, targetPath, "RSSI", RESULT_NR_2D_RSSI_RU, "RSSI", "Float", ruInfo, ruPathInfo)
    d2.Make2DBinFile.makeRuResult(scheduleId, targetPath, "C2I", RESULT_NR_2D_SINR_RU, "SINR", "Float", ruInfo, ruPathInfo)
  }

  def execute3D(scheduleId: String)(implicit stmt: Statement, spark: SparkSession): Unit = {
    // Analysis
    logInfo(s"Phase(${scheduleId}): LOS")

    bd.Header.executeScenario(scheduleId)
    bd.Header.executeSite(scheduleId)
    bd.LOS.executeScenario(scheduleId)
    bd.PathLoss.executeSite(scheduleId)
    bd.PathLoss.executeScenario(scheduleId)
    bd.RSRPPilot.executeSite(scheduleId)
    bd.RSRP.executeSite(scheduleId)
    bd.RSRP.executeScenario(scheduleId)
    bd.BestServer.executeScenario(scheduleId)
    bd.RSSI.executeSite(scheduleId)
    bd.RSSI.executeScenario(scheduleId)
    bd.SINR.executeSite(scheduleId)
    bd.SINR.executeScenario(scheduleId)
    bd.Throughput.executeScenario(scheduleId)
  }

  def make3DBinFiles(scheduleId: String, resultRoot: String)(implicit stmt: Statement, spark: SparkSession): Unit = {

    // Make bin files
    logInfo(s"Phase(${scheduleId}): Making bin files")

    val schedule = MyEnv.getSchedule(scheduleId)
    val scenario = MyEnv.getScenario(schedule.scenarioId)

    val ruInfo = MyEnv.makeResultPath(scenario.id)
    val targetPath = s"$resultRoot/${scheduleId}"
    val ruPathInfo = ruInfo.map { ru =>
      (ru._1, s"ENB_${ru._2.enbId}/PCI_${ru._2.pci}_PORT_${ru._2.pciPort}_${ru._2.id}")
    }

    // schedule       in_out!=0

    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "LOS", RESULT_NR_BF_LOS, "LOS", "Int", false, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PATHLOSS", RESULT_NR_BF_PATHLOSS, "PATHLOSS", "Float", false, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "BESTSERVER", RESULT_NR_BF_BESTSERVER, "RU_SEQ", "Int", false, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PILOT_EC", RESULT_NR_BF_RSRP, "RSRP", "Float", false, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "RSSI", RESULT_NR_BF_RSSI, "RSSI", "Float", false, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "C2I", RESULT_NR_BF_SINR, "SINR", "Float", false, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "DOWNLINK_THROUGHPUT", RESULT_NR_BF_THROUGHPUT, "THROUGHPUT", "Float", false, false)

    // schedule/noin

    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "LOS_NOIN", RESULT_NR_BF_LOS, "LOS", "Int", true, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PATHLOSS_NOIN", RESULT_NR_BF_PATHLOSS, "PATHLOSS", "Float", true, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "BESTSERVER_NOIN", RESULT_NR_BF_BESTSERVER, "RU_SEQ", "Int", true, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PILOT_EC_NOIN", RESULT_NR_BF_RSRP, "RSRP", "Float", true, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "RSSI_NOIN", RESULT_NR_BF_RSSI, "RSSI", "Float", true, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "C2I_NOIN", RESULT_NR_BF_SINR, "SINR", "Float", true, false)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "DOWNLINK_THROUGHPUT_NOIN", RESULT_NR_BF_THROUGHPUT, "THROUGHPUT", "Float", true, false)

    // schedule/noplb       in_out!=0           +nrsystem.penetrationloss

    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PATHLOSS_NOPLB", RESULT_NR_BF_PATHLOSS, "PATHLOSS", "Float", false, true)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PILOT_EC_NOPLB", RESULT_NR_BF_RSRP, "RSRP", "Float", false, true)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "RSSI_NOPLB", RESULT_NR_BF_RSSI, "RSSI", "Float", false, true)

    // schedule/noin_noplb                      +nrsystem.penetrationloss

    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PATHLOSS_NOIN_NOPLB", RESULT_NR_BF_PATHLOSS, "PATHLOSS", "Float", true, true)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "PILOT_EC_NOIN_NOPLB", RESULT_NR_BF_RSRP, "RSRP", "Float", true, true)
    bd.MakeBFBinFile.makeScheduleResult(scheduleId, targetPath, "RSSI_NOIN_NOPLB", RESULT_NR_BF_RSSI, "RSSI", "Float", true, true)

    // ru   bin_type!=0

    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "LOS", RESULT_NR_BF_LOS_RU, "VALUE", "Int", ruInfo, ruPathInfo, false)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "PATHLOSS", RESULT_NR_BF_PATHLOSS_RU, "PATHLOSS", "Float", ruInfo, ruPathInfo, false)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "PILOT_EC", RESULT_NR_BF_RSRP_RU, "RSRP", "Float", ruInfo, ruPathInfo, false)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "RSSI", RESULT_NR_BF_RSSI_RU, "RSSI", "Float", ruInfo, ruPathInfo, false)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "C2I", RESULT_NR_BF_SINR_RU, "SINR", "Float", ruInfo, ruPathInfo, false)

    // ru/noin

    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "LOS_NOIN", RESULT_NR_BF_LOS_RU, "VALUE", "Int", ruInfo, ruPathInfo, true)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "PATHLOSS_NOIN", RESULT_NR_BF_PATHLOSS_RU, "PATHLOSS", "Float", ruInfo, ruPathInfo, true)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "PILOT_EC_NOIN", RESULT_NR_BF_RSRP_RU, "RSRP", "Float", ruInfo, ruPathInfo, true)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "RSSI_NOIN", RESULT_NR_BF_RSSI_RU, "RSSI", "Float", ruInfo, ruPathInfo, true)
    bd.MakeBFBinFile.makeRuResult(scheduleId, targetPath, "C2I_NOIN", RESULT_NR_BF_SINR_RU, "SINR", "Float", ruInfo, ruPathInfo, true)
  }

  val initInt = ByteUtil.intZero()
  val initFloat = ByteUtil.floatMax()

  def makeScheduleBinRequest(alg: String): BinRequest = {
    // schedule 별
    alg match {
      case "LOS" => BinRequest(RESULT_NR_2D_LOS, "LOS", "LOS", initInt)
      case "PATHLOSS" => BinRequest(RESULT_NR_2D_PATHLOSS, "PATHLOSS", "PATHLOSS", initFloat)
      case "BESTSERVER" => BinRequest(RESULT_NR_2D_BESTSERVER, "RU_SEQ", "BESTSERVER", initInt)
      case "RSRP" => BinRequest(RESULT_NR_2D_RSRP, "RSRP", "PILOT_EC", initFloat)
      case "RSSI" => BinRequest(RESULT_NR_2D_RSSI, "RSSI", "RSSI", initFloat)
      case "SINR" => BinRequest(RESULT_NR_2D_SINR, "SINR", "C2I", initFloat)
      case "THROUGHPUT" => BinRequest(RESULT_NR_2D_THROUGHPUT, "THROUGHPUT", "DOWNLINK_THROUGHPUT", initFloat)
    }
  }

  def makeRUBinRequest(alg: String): Unit = {
    alg match {
      case "LOS" => BinRequest(RESULT_NR_2D_LOS_RU, "VALUE", "LOS", initInt)
      case "PATHLOSS" => BinRequest(RESULT_NR_2D_PATHLOSS_RU, "PATHLOSS", "PATHLOSS", initFloat)
      //      case "BESTSERVER" => BinRequest(RESULT_NR_2D_BESTSERVER_RU, "RU_SEQ", initInt)
      case "RSRP" => BinRequest(RESULT_NR_2D_RSRP_RU, "RSRP", "PILOT_EC", initFloat)
      case "RSSI" => BinRequest(RESULT_NR_2D_RSSI_RU, "RSSI", "RSSI", initFloat)
      case "SINR" => BinRequest(RESULT_NR_2D_SINR_RU, "SINR", "C2I", initFloat)
      //      case "THROUGHPUT" => BinRequest(RESULT_NR_2D_THROUGHPUT, "THROUGHPUT", initFloat)
    }

  }
}

case class BinRequest(tableName: String, colName: String, fileName: String, initValue: Array[Byte])

/**
 * 2D 및 3D 분석의 전과정을 하나의 프로세스로 진행
 */
object ScheduleMain extends ScheduleMain with Logging {

  val resultRoot = "/user/icpap/result"

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ScheduleMain <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val schedule = MyEnv.getSchedule(scheduleId)


    implicit val spark = SparkSession.builder()
      .appName(s"ScheduleMain $scheduleId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    setHiveExecEnv()

    try {
      logInfo(s"Phase($scheduleId): proceed")

      // Setting counts

      /*
            logInfo("Phase: Prepare")
            ScenarioSetter.execute(scenarioId)
      */

      if (schedule.scheduleType == "SC051") {
        execute3D(scheduleId)
        make3DBinFiles(scheduleId, resultRoot)
      } else {
        execute2D(scheduleId)
        make2DBinFiles(scheduleId, resultRoot)
      }

    } finally {
      try {
        stmt.close()
      }
      try {
        conn.close()
      }
      spark.close()
    }
  }
}

/**
 * 3D 분석에 있어서 RU별 SINR 의 데이터 처리가 불가능해서 그 처리를 기준으로 앞의 과정만을 하나의 프로세스로 진행
 */
object ScheduleMainToScenarioRSSI extends ScheduleMain with Logging {

  val resultRoot = "/user/icpap/result"

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ScheduleMainToScenarioRSSI <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val schedule = MyEnv.getSchedule(scheduleId)


    implicit val spark = SparkSession.builder()
      .appName(s"ScheduleMainToScenarioRSSI $scheduleId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    try {
      logInfo(s"Phase($scheduleId): proceed")

      // Setting counts

      /*
            logInfo("Phase: Prepare")
            ScenarioSetter.execute(scenarioId)
      */

      if (schedule.scheduleType == "SC051") {
        execute3D(scheduleId)
      } else {
        // todo throw a exception
        execute2D(scheduleId)
        make2DBinFiles(scheduleId, resultRoot)
      }

    } finally {
      try {
        stmt.close()
      }
      try {
        conn.close()
      }
      spark.close()
    }
  }


  override def execute3D(scheduleId: String)(implicit stmt: Statement, spark: SparkSession): Unit = {
    // Analysis
    logInfo(s"Phase(${scheduleId}): To site SINR")

    bd.LOS.setHiveExecEnv()
    bd.Header.executeScenario(scheduleId)
    bd.Header.executeSite(scheduleId)
    bd.LOS.executeScenario(scheduleId)
    bd.PathLoss.executeSite(scheduleId)
    bd.PathLoss.executeScenario(scheduleId)
    bd.RSRPPilot.executeSite(scheduleId)
    bd.RSRP.executeSite(scheduleId)
    bd.RSRP.executeScenario(scheduleId)
    bd.BestServer.executeScenario(scheduleId)
    bd.RSSI.executeSite(scheduleId)
    bd.RSSI.executeScenario(scheduleId)
  }

}


/**
 * 3D 분석에 있어서 RU별 SINR 의 데이터 처리가 불가능해서 그 처리를 기준으로 뒤의 과정만을 하나의 프로세스로 진행
 */
object ScheduleMainFromScenarioSINR extends ScheduleMain with Logging {

  val resultRoot = "/user/icpap/result"

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ScheduleMainFromScenarioSINR <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val schedule = MyEnv.getSchedule(scheduleId)


    implicit val spark = SparkSession.builder()
      .appName(s"ScheduleMainFromScenarioSINR $scheduleId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    setHiveExecEnv()

    try {
      logInfo(s"Phase($scheduleId): proceed")

      // Setting counts

      /*
            logInfo("Phase: Prepare")
            ScenarioSetter.execute(scenarioId)
      */

      if (schedule.scheduleType == "SC051") {
        execute3D(scheduleId)
        make3DBinFiles(scheduleId, resultRoot)
      } else {
        // todo throw a exception
        execute2D(scheduleId)
        make2DBinFiles(scheduleId, resultRoot)
      }

    } finally {
      try {
        stmt.close()
      }
      try {
        conn.close()
      }
      spark.close()
    }
  }

  override def execute3D(scheduleId: String)(implicit stmt: Statement, spark: SparkSession): Unit = {
    // Analysis
    logInfo(s"Phase(${scheduleId}): From scenario SINR")

    bd.SINR.executeScenario(scheduleId)
    bd.Throughput.executeScenario(scheduleId)
  }

}

/**
 * 테스트를 위해서 bin 파일 만드는 과정만을 하나의 프로세스로 진행
 */
object ScheduleMainFromBin extends ScheduleMain with Logging {

  val resultRoot = "/user/icpap/result"

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ScheduleMainFromBin <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val schedule = MyEnv.getSchedule(scheduleId)


    implicit val spark = SparkSession.builder()
      .appName(s"ScheduleMainFromBin $scheduleId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    setHiveExecEnv()

    try {
      logInfo(s"Phase($scheduleId): proceed")

      // Setting counts

      /*
            logInfo("Phase: Prepare")
            ScenarioSetter.execute(scenarioId)
      */

      if (schedule.scheduleType == "SC051") {
        make3DBinFiles(scheduleId, resultRoot)
      } else {
        make2DBinFiles(scheduleId, resultRoot)
      }

    } finally {
      try {
        stmt.close()
      }
      try {
        conn.close()
      }
      spark.close()
    }
  }
}
