package com.sccomz.schedule

import java.sql.Statement

import com.sccomz.rdb.OracleCon
import com.sccomz.schedule.real.ScheduleDaemonSql
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

/**
 * 스케쥴의 타입을 구하여 stdout 에 출력한다.
 *
 * java -cp ../../lib/scala-library-2.11.8.jar:../../lib/teos-scala-1.0-SNAPSHOT.jar:../../lib/config-1.3.3.jar:../../lib/ojdbc7.jar:../../lib/slf4j-api-1.7.16.jar:../../lib/slf4j-log4j12-1.7.16.jar:../../lib/log4j-1.2.17.jar:../../conf com.sccomz.schedule.ScheduleTypeSelector 1
 *
 */
object ScheduleTypeSelector {

  val logger = LoggerFactory.getLogger(getClass)

  val cf = ConfigFactory.load()

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ScheduleTypeSelector <schedule_id>")
      System.exit(1)
    }

    val schedule_id = args(0)

    logger.info(s"schedule_id=$schedule_id")

    val con = OracleCon.getCon()

    implicit val stmt = con.createStatement()

    try {
      execute(schedule_id)
    } catch {
      case e: Throwable => logger.error(e.getLocalizedMessage, e)
    } finally {
      stmt.close()
      con.close()
    }

    def execute(schedule_id: String)(implicit stmt: Statement): Unit = {
      val qry = s"SELECT TYPE_CD FROM SCHEDULE WHERE SCHEDULE_ID = ${schedule_id}"
//      println(qry)
      val rs = stmt.executeQuery(qry)
      val result =
        if (rs.next())
          rs.getString("TYPE_CD")
        else
          ""

      print(result)
    }
  }
}
