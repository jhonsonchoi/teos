package com.sccomz.schedule.job

import org.slf4j.LoggerFactory

object JobTest {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println("usage: JobTest <schedule_id> <scenario_id>")
      System.exit(1)
    }

    val wfId = args(0)
    val scenarioId = args(1)

    logger.info(s"schedule_id=$wfId, scenario_id=$scenarioId")
  }

}
