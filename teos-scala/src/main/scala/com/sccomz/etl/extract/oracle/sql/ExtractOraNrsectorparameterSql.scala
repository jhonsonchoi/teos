package com.sccomz.etl.extract.oracle.sql

object ExtractOraNrsectorparameterSql {

  def selectNrsectorparameter(scenarioId: String) = {
    s"""
        |SELECT
        |       NVL(ENB_ID,0) AS ENB_ID,
        |       NVL(PCI,0) AS PCI,
        |       NVL(PCI_PORT,0) AS PCI_PORT,
        |       RU_ID,
        |       NVL(TXPWRWATT,0) AS TXPWRWATT,
        |       NVL(TXPWRDBM,0) AS TXPWRDBM,
        |       NVL(TXEIRPWATT,0) AS TXEIRPWATT,
        |       NVL(TXEIRPDBM,0) AS TXEIRPDBM,
        |       NVL(RETXEIRPWATT,0) AS RETXEIRPWATT,
        |       NVL(RETXEIRPDBM,0) AS RETXEIRPDBM,
        |       NVL(TRXCOUNT,0) AS TRXCOUNT,
        |       NVL(DLMODULATION,0) AS DLMODULATION,
        |       NVL(DLMIMOTYPE,0) AS DLMIMOTYPE,
        |       NVL(DLMIMOGAIN,0) AS DLMIMOGAIN,
        |       NVL(ULMODULATION,0) AS ULMODULATION,
        |       NVL(ULMIMOTYPE,0) AS ULMIMOTYPE,
        |       NVL(ULMIMOGAIN,0) AS ULMIMOGAIN,
        |       NVL(LOSBEAMFORMINGLOSS,0) AS LOSBEAMFORMINGLOSS,
        |       NVL(NLOSBEAMFORMINGLOSS,0) AS NLOSBEAMFORMINGLOSS,
        |       NVL(HANDOVERCALLDROPTHRESHOLD,0) AS HANDOVERCALLDROPTHRESHOLD,
        |       NVL(POWERCOMBININGGAIN,0) AS POWERCOMBININGGAIN,
        |       NVL(BEAMMISMATCHMARGIN,0) AS BEAMMISMATCHMARGIN,
        |       NVL(ANTENNAGAIN,0) AS ANTENNAGAIN,
        |       NVL(FOLIAGELOSS,0) AS FOLIAGELOSS,
        |       NVL(TXLAYER,0) AS TXLAYER,
        |       NVL(RXLAYER,0) AS RXLAYER,
        |       TOTAL_ERLANG,
        |       DLINTERCELL,
        |       SCENARIO_ID
        |FROM   NRSECTORPARAMETER
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
  }

def selectNrsectorparameterCsv(scenarioId: String) = {
s"""
SELECT 
       NVL(ENB_ID,0)                    ||'|'||
       NVL(PCI,0)                       ||'|'||
       NVL(PCI_PORT,0)                  ||'|'||
       RU_ID                            ||'|'||
       NVL(TXPWRWATT,0)                 ||'|'||
       NVL(TXPWRDBM,0)                  ||'|'||
       NVL(TXEIRPWATT,0)                ||'|'||
       NVL(TXEIRPDBM,0)                 ||'|'||
       NVL(RETXEIRPWATT,0)              ||'|'||
       NVL(RETXEIRPDBM,0)               ||'|'||
       NVL(TRXCOUNT,0)                  ||'|'||
       NVL(DLMODULATION,0)              ||'|'||
       NVL(DLMIMOTYPE,0)                ||'|'||
       NVL(DLMIMOGAIN,0)                ||'|'||
       NVL(ULMODULATION,0)              ||'|'||
       NVL(ULMIMOTYPE,0)                ||'|'||
       NVL(ULMIMOGAIN,0)                ||'|'||
       NVL(LOSBEAMFORMINGLOSS,0)        ||'|'||
       NVL(NLOSBEAMFORMINGLOSS,0)       ||'|'||
       NVL(HANDOVERCALLDROPTHRESHOLD,0) ||'|'||
       NVL(POWERCOMBININGGAIN,0)        ||'|'||
       NVL(BEAMMISMATCHMARGIN,0)        ||'|'||
       NVL(ANTENNAGAIN,0)               ||'|'||
       NVL(FOLIAGELOSS,0)               ||'|'||
       NVL(TXLAYER,0)                   ||'|'||
       NVL(RXLAYER,0)                   ||'|'||
       NVL(SCENARIO_ID,0)               ||'|'
FROM   NRSECTORPARAMETER
WHERE  SCENARIO_ID = ${scenarioId}
"""
}

def selectNrsectorparameterIns(scheduleId:String) = {
s"""
"""
}



}