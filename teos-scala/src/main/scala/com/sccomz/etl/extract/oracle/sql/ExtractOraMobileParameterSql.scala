package com.sccomz.etl.extract.oracle.sql

object ExtractOraMobileParameterSql {
  
  def selectMobileParameter(scenarioId: String) = {
    s"""
        |SELECT
        |       NVL(MOBILE_ID,0) AS MOBILE_ID,
        |       NVL(TYPE,0) AS TYPE,
        |       MOBILENAME,
        |       MAKER,
        |       NVL(MINPOWER,0) AS MINPOWER,
        |       NVL(MAXPOWER,0) AS MAXPOWER,
        |       NVL(MOBILEGAIN,0) AS MOBILEGAIN,
        |       NVL(NOISEFLOOR,0) AS NOISEFLOOR,
        |       NVL(HEIGHT,0) AS HEIGHT,
        |       NVL(BODYLOSS,0) AS BODYLOSS,
        |       NVL(BUILDINGLOSS,0) AS BUILDINGLOSS,
        |       NVL(CARLOSS,0) AS CARLOSS,
        |       NVL(FEEDERLOSS,0) AS FEEDERLOSS,
        |       NVL(NOISEFIGURE,0) AS NOISEFIGURE,
        |       NVL(DIVERSITYGAIN,0) AS DIVERSITYGAIN,
        |       NVL(ANTENNAGAIN,0) AS ANTENNAGAIN,
        |       NVL(RX_LAYER,0) AS RX_LAYER,
        |       NVL(SCENARIO_ID,0) AS SCENARIO_ID
        |FROM   MOBILE_PARAMETER
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
}

  def selectMobileParameterCsv(scenarioId: String) = {
    s"""
SELECT
       NVL(MOBILE_ID,0)     ||'|'||
       NVL(TYPE,0)          ||'|'||
       MOBILENAME           ||'|'||
       MAKER                ||'|'||
       NVL(MINPOWER,0)      ||'|'||
       NVL(MAXPOWER,0)      ||'|'||
       NVL(MOBILEGAIN,0)    ||'|'||
       NVL(NOISEFLOOR,0)    ||'|'||
       NVL(HEIGHT,0)        ||'|'||
       NVL(BODYLOSS,0)      ||'|'||
       NVL(BUILDINGLOSS,0)  ||'|'||
       NVL(CARLOSS,0)       ||'|'||
       NVL(FEEDERLOSS,0)    ||'|'||
       NVL(NOISEFIGURE,0)   ||'|'||
       NVL(DIVERSITYGAIN,0) ||'|'||
       NVL(ANTENNAGAIN,0)   ||'|'||
       NVL(RX_LAYER,0)      ||'|'||
       NVL(SCENARIO_ID,0)   ||'|'
FROM   MOBILE_PARAMETER
WHERE  SCENARIO_ID = ${scenarioId}
"""
  }

  def selectMobileParameterForPostgres(scenarioId: String) = {
    s"""
        |SELECT
        |       SCENARIO_ID,
        |       NVL(MOBILE_ID,0) AS MOBILE_ID,
        |       NVL(TYPE,0) AS TYPE,
        |       MOBILENAME,
        |       MAKER,
        |       NVL(MINPOWER,0) AS MINPOWER,
        |       NVL(MAXPOWER,0) AS MAXPOWER,
        |       NVL(MOBILEGAIN,0) AS MOBILEGAIN,
        |       NVL(NOISEFLOOR,0) AS NOISEFLOOR,
        |       NVL(HEIGHT,0) AS HEIGHT,
        |       NVL(BODYLOSS,0) AS BODYLOSS,
        |       NVL(BUILDINGLOSS,0) AS BUILDINGLOSS,
        |       NVL(CARLOSS,0) AS CARLOSS,
        |       NVL(FEEDERLOSS,0) AS FEEDERLOSS,
        |       NVL(NOISEFIGURE,0) AS NOISEFIGURE,
        |       NVL(DIVERSITYGAIN,0) AS DIVERSITYGAIN,
        |       NVL(ANTENNAGAIN,0) AS ANTENNAGAIN,
        |       NVL(RX_LAYER,0) AS RX_LAYER
        |FROM   MOBILE_PARAMETER
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
  }

  def selectMobileParameterIns(scenarioId: String) = {
s"""
SELECT 
'INSERT INTO MOBILE_PARAMETER VALUES ('
||' '  ||NVL(SCENARIO_ID,0)   
||','  ||NVL(MOBILE_ID,0)     
||','  ||NVL(TYPE,0)          
||','''||MOBILENAME           ||''''
||','''||MAKER                ||''''
||','  ||NVL(MINPOWER,0)      
||','  ||NVL(MAXPOWER,0)      
||','  ||NVL(MOBILEGAIN,0)    
||','  ||NVL(NOISEFLOOR,0)    
||','  ||NVL(HEIGHT,0)        
||','  ||NVL(BODYLOSS,0)      
||','  ||NVL(BUILDINGLOSS,0)  
||','  ||NVL(CARLOSS,0)       
||','  ||NVL(FEEDERLOSS,0)    
||','  ||NVL(NOISEFIGURE,0)   
||','  ||NVL(DIVERSITYGAIN,0) 
||','  ||NVL(ANTENNAGAIN,0)   
||','  ||NVL(RX_LAYER,0)      
||');'
FROM   MOBILE_PARAMETER
WHERE  SCENARIO_ID = ${scenarioId}
"""
}



}