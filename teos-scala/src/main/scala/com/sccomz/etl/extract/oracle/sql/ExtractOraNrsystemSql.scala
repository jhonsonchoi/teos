package com.sccomz.etl.extract.oracle.sql

object ExtractOraNrsystemSql {


  def selectNrsystem(scenarioId: String) = {
    s"""
        |SELECT
        |       NVL(SYSTEM_ID,0) AS SYSTEM_ID,
        |       NVL(FA_SEQ,0) AS FA_SEQ,
        |       NVL(BANDWIDTH_PER_CC,0) AS BANDWIDTH_PER_CC,
        |       NVL(NUMBER_OF_CC,0) AS NUMBER_OF_CC,
        |       NVL(NUMBER_OF_SC_PER_RB,0) AS NUMBER_OF_SC_PER_RB,
        |       NVL(TOTALBANDWIDTH,0) AS TOTALBANDWIDTH,
        |       NVL(SUBCARRIERSPACING,0) AS SUBCARRIERSPACING,
        |       NVL(RB_PER_CC,0) AS RB_PER_CC,
        |       NVL(RADIOFRAMELENGTH,0) AS RADIOFRAMELENGTH,
        |       NVL(SUBFRAMELENGTH,0) AS SUBFRAMELENGTH,
        |       NVL(NO_SLOTPERRADIOFRAME,0) AS NO_SLOTPERRADIOFRAME,
        |       NVL(SLOTLENGTH,0) AS SLOTLENGTH,
        |       NVL(NO_OFDMSYMBOLPERSUBFRAME,0) AS NO_OFDMSYMBOLPERSUBFRAME,
        |       NVL(FRAMECONFIGURATION,0) AS FRAMECONFIGURATION,
        |       NVL(DLDATARATIO,0) AS DLDATARATIO,
        |       NVL(ULDATARATIO,0) AS ULDATARATIO,
        |       NVL(DLBLER,0) AS DLBLER,
        |       NVL(ULBLER,0) AS ULBLER,
        |       NVL(DIVERSITYGAINRATIO,0) AS DIVERSITYGAINRATIO,
        |       NVL(DLINTRACELL,0) AS DLINTRACELL,
        |       NVL(DLINTERCELL,0) AS DLINTERCELL,
        |       NVL(ULINTRACELL,0) AS ULINTRACELL,
        |       NVL(ULINTERCELL,0) AS ULINTERCELL,
        |       NVL(DLCOVERAGELIMITRSRP,0) AS DLCOVERAGELIMITRSRP,
        |       NVL(INTERFERENCEMARGIN,0) AS INTERFERENCEMARGIN,
        |       NVL(NRGAINOVERLTE,0) AS NRGAINOVERLTE,
        |       NVL(PENETRATIONLOSS,0) AS PENETRATIONLOSS,
        |       NVL(SLOT_CONFIGURATION,0) AS SLOT_CONFIGURATION,
        |       NVL(DLCOVERAGELIMITRSRPLOS,0) AS DLCOVERAGELIMITRSRPLOS,
        |       NVL(DLOH,0) AS DLOH,
        |       NVL(ULOH,0) AS ULOH,
        |       NVL(DLSINROFFSET,0) AS DLSINROFFSET,
        |       NVL(TECHTYPE,0) AS TECHTYPE,
        |       NVL(RAYTRACING_REFLECTION,0) AS RAYTRACING_REFLECTION,
        |       NVL(RAYTRACING_DIFFRACTION,0) AS RAYTRACING_DIFFRACTION,
        |       NVL(RAYTRACING_SCATTERING,0) AS RAYTRACING_SCATTERING,
        |       NVL(RELATED_ANALYSIS_COVLIMITRSRP,0) AS RELATED_ANALYSIS_COVLIMITRSRP,
        |       ENV_ROADSIDE_TREE_YN,
        |       DLCOVLIMITRSRP_YN,
        |       ANT_CATEGORY,
        |       ANT_NM,
        |       DUHNOISEFIGURE,
        |       ULCOVERAGELIMITRSRP,
        |       TRAFFIC_TYPE,
        |       MORPHOLOGY_WEIGHT_USE,
        |       SCALING_FACTOR,
        |       TOTAL_ERLANG,
        |       SCENARIO_ID
        |FROM   NRSYSTEM
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
  }

def selectNrsystemCsv(scenarioId: String) = {
s"""
SELECT 
       NVL(SYSTEM_ID,0)                     ||'|'||
       NVL(FA_SEQ,0)                        ||'|'||
       NVL(BANDWIDTH_PER_CC,0)              ||'|'||
       NVL(NUMBER_OF_CC,0)                  ||'|'||
       NVL(NUMBER_OF_SC_PER_RB,0)           ||'|'||
       NVL(TOTALBANDWIDTH,0)                ||'|'||
       NVL(SUBCARRIERSPACING,0)             ||'|'||
       NVL(RB_PER_CC,0)                     ||'|'||
       NVL(RADIOFRAMELENGTH,0)              ||'|'||
       NVL(SUBFRAMELENGTH,0)                ||'|'||
       NVL(NO_SLOTPERRADIOFRAME,0)          ||'|'||
       NVL(SLOTLENGTH,0)                    ||'|'||
       NVL(NO_OFDMSYMBOLPERSUBFRAME,0)      ||'|'||
       NVL(FRAMECONFIGURATION,0)            ||'|'||
       NVL(DLDATARATIO,0)                   ||'|'||
       NVL(ULDATARATIO,0)                   ||'|'||
       NVL(DLBLER,0)                        ||'|'||
       NVL(ULBLER,0)                        ||'|'||
       NVL(DIVERSITYGAINRATIO,0)            ||'|'||
       NVL(DLINTRACELL,0)                   ||'|'||
       NVL(DLINTERCELL,0)                   ||'|'||
       NVL(ULINTRACELL,0)                   ||'|'||
       NVL(ULINTERCELL,0)                   ||'|'||
       NVL(DLCOVERAGELIMITRSRP,0)           ||'|'||
       NVL(INTERFERENCEMARGIN,0)            ||'|'||
       NVL(NRGAINOVERLTE,0)                 ||'|'||
       NVL(PENETRATIONLOSS,0)               ||'|'||
       NVL(SLOT_CONFIGURATION,0)            ||'|'||
       NVL(DLCOVERAGELIMITRSRPLOS,0)        ||'|'||
       NVL(DLOH,0)                          ||'|'||
       NVL(ULOH,0)                          ||'|'||
       NVL(DLSINROFFSET,0)                  ||'|'||
       NVL(TECHTYPE,0)                      ||'|'||
       NVL(RAYTRACING_REFLECTION,0)         ||'|'||
       NVL(RAYTRACING_DIFFRACTION,0)        ||'|'||
       NVL(RAYTRACING_SCATTERING,0)         ||'|'||
       NVL(RELATED_ANALYSIS_COVLIMITRSRP,0) ||'|'||
       ENV_ROADSIDE_TREE_YN                 ||'|'||
       DLCOVLIMITRSRP_YN                    ||'|'||
       ANT_CATEGORY                         ||'|'||
       ANT_NM                               ||'|'||
       NVL(SCENARIO_ID,0)                   ||'|'
FROM   NRSYSTEM
WHERE  SCENARIO_ID = ${scenarioId}
"""
}

def selectNrsystemIns(scheduleId:String) = {
s"""
"""
}



}