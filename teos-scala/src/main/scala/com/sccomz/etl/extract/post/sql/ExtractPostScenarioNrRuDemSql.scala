package com.sccomz.etl.extract.post.sql

object ExtractPostScenarioNrRuDemSql {

def selectScenarioNrRuDemCsv(scenarioId: String) = {
s"""
SELECT
	     ENB_ID         ||'|'||
	     PCI            ||'|'||
	     PCI_PORT       ||'|'||
	     RU_ID          ||'|'||
	     ROUND(CAST(DEM_HEGHT AS NUMERIC),2)      ||'|'||
	     ROUND(CAST(BLD_AVG_HEIGHT AS NUMERIC),2) ||'|'||
	     SCENARIO_ID    ||'|'
FROM   SCENARIO_NR_RU_DEM
WHERE  SCENARIO_ID = '${scenarioId}'
"""
}

}