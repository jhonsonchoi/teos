package com.sccomz.etl.extract.oracle.sql

object ExtractOraScenarioSql {

  def selectScenario(scenarioId:String) = {
    s"""
        |SELECT
        |       SCENARIO_NM,
        |       USER_ID,
        |       NVL(SYSTEM_ID,0) AS SYSTEM_ID,
        |       NVL(NETWORK_TYPE,0) AS NETWORK_TYPE,
        |       SIDO_CD,
        |       SIGUGUN_CD,
        |       DONG_CD,
        |       SIDO,
        |       SIGUGUN,
        |       DONG,
        |       NVL(STARTX,0) AS STARTX,
        |       NVL(STARTY,0) AS STARTY,
        |       NVL(ENDX,0) AS ENDX,
        |       NVL(ENDY,0) AS ENDY,
        |       NVL(FA_MODEL_ID,0) AS FA_MODEL_ID,
        |       NVL(FA_SEQ,0) AS FA_SEQ,
        |       SCENARIO_DESC,
        |       NVL(USE_BUILDING,0) AS USE_BUILDING,
        |       NVL(USE_MULTIFA,0) AS USE_MULTIFA,
        |       NVL(PRECISION,0) AS PRECISION,
        |       NVL(PWRCTRLCHECKPOINT,0) AS PWRCTRLCHECKPOINT,
        |       NVL(MAXITERATIONPWRCTRL,0) AS MAXITERATIONPWRCTRL,
        |       NVL(RESOLUTION,0) AS RESOLUTION,
        |       NVL(MODEL_RADIUS,0) AS MODEL_RADIUS,
        |       TO_CHAR(REG_DT,'YYYY-MM-DD HH24:MI:SS') AS REG_DT,
        |       TO_CHAR(MODIFY_DT,'YYYY-MM-DD HH24:MI:SS') AS MODIFY_DT,
        |       NVL(UPPER_SCENARIO_ID,0) AS UPPER_SCENARIO_ID,
        |       NVL(FLOORBUILDING,0) AS FLOORBUILDING,
        |       NVL(FLOOR,0) AS FLOOR,
        |       NVL(FLOORLOSS,0) AS FLOORLOSS,
        |       NVL(SCENARIO_SUB_ID,0) AS SCENARIO_SUB_ID,
        |       NVL(SCENARIO_SOLUTION_NUM,0) AS SCENARIO_SOLUTION_NUM,
        |       NVL(LOSS_TYPE,0) AS LOSS_TYPE,
        |       NVL(RU_CNT,0) AS RU_CNT,
        |       MODIFY_YN,
        |       BATCH_YN,
        |       NVL(TM_STARTX,0) AS TM_STARTX,
        |       NVL(TM_STARTY,0) AS TM_STARTY,
        |       NVL(TM_ENDX,0) AS TM_ENDX,
        |       NVL(TM_ENDY,0) AS TM_ENDY,
        |       REAL_DATE,
        |       REAL_DRM_FILE,
        |       REAL_COMP,
        |       REAL_CATT,
        |       REAL_CATY,
        |       REPLACE(BLD_TYPE,'|','^') AS BLD_TYPE,
        |       NVL(RET_PERIOD,0) AS RET_PERIOD,
        |       RET_END_DATETIME,
        |       BUILDINGANALYSIS3D_YN,
        |       NVL(BUILDINGANALYSIS3D_RESOLUTION,0) AS BUILDINGANALYSIS3D_RESOLUTION,
        |       NVL(AREA_ID,0) AS AREA_ID,
        |       BUILDINGANALYSIS3D_RELATED_YN,
        |       NVL(RELATED_ANALYSIS_COVLIMITRSRP,0) AS RELATED_ANALYSIS_COVLIMITRSRP,
        |       SCENARIO_ID
        |FROM   SCENARIO
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
  }

  def selectScenarioCsv(scenarioId:String) = {
s"""
SELECT
       SCENARIO_NM                                 ||'|'||
       USER_ID                                     ||'|'||
       NVL(SYSTEM_ID,0)                            ||'|'||
       NVL(NETWORK_TYPE,0)                         ||'|'||
       SIDO_CD                                     ||'|'||
       SIGUGUN_CD                                  ||'|'||
       DONG_CD                                     ||'|'||
       SIDO                                        ||'|'||
       SIGUGUN                                     ||'|'||
       DONG                                        ||'|'||
       NVL(STARTX,0)                               ||'|'||
       NVL(STARTY,0)                               ||'|'||
       NVL(ENDX,0)                                 ||'|'||
       NVL(ENDY,0)                                 ||'|'||
       NVL(FA_MODEL_ID,0)                          ||'|'||
       NVL(FA_SEQ,0)                               ||'|'||
       SCENARIO_DESC                               ||'|'||
       NVL(USE_BUILDING,0)                         ||'|'||
       NVL(USE_MULTIFA,0)                          ||'|'||
       NVL(PRECISION,0)                            ||'|'||
       NVL(PWRCTRLCHECKPOINT,0)                    ||'|'||
       NVL(MAXITERATIONPWRCTRL,0)                  ||'|'||
       NVL(RESOLUTION,0)                           ||'|'||
       NVL(MODEL_RADIUS,0)                         ||'|'||
       TO_CHAR(REG_DT,'YYYY-MM-DD HH24:MI:SS')     ||'|'||
       TO_CHAR(MODIFY_DT,'YYYY-MM-DD HH24:MI:SS')  ||'|'||
       NVL(UPPER_SCENARIO_ID,0)                    ||'|'||
       NVL(FLOORBUILDING,0)                        ||'|'||
       NVL(FLOOR,0)                                ||'|'||
       NVL(FLOORLOSS,0)                            ||'|'||
       NVL(SCENARIO_SUB_ID,0)                      ||'|'||
       NVL(SCENARIO_SOLUTION_NUM,0)                ||'|'||
       NVL(LOSS_TYPE,0)                            ||'|'||
       NVL(RU_CNT,0)                               ||'|'||
       MODIFY_YN                                   ||'|'||
       BATCH_YN                                    ||'|'||
       NVL(TM_STARTX,0)                            ||'|'||
       NVL(TM_STARTY,0)                            ||'|'||
       NVL(TM_ENDX,0)                              ||'|'||
       NVL(TM_ENDY,0)                              ||'|'||
       REAL_DATE                                   ||'|'||
       REAL_DRM_FILE                               ||'|'||
       REAL_COMP                                   ||'|'||
       REAL_CATT                                   ||'|'||
       REAL_CATY                                   ||'|'||
       REPLACE(BLD_TYPE,'|','^')                   ||'|'||
       NVL(RET_PERIOD,0)                           ||'|'||
       RET_END_DATETIME                            ||'|'||
       BUILDINGANALYSIS3D_YN                       ||'|'||
       NVL(BUILDINGANALYSIS3D_RESOLUTION,0)        ||'|'||
       NVL(AREA_ID,0)                              ||'|'||
       BUILDINGANALYSIS3D_RELATED_YN               ||'|'||
       NVL(RELATED_ANALYSIS_COVLIMITRSRP,0)        ||'|'||
       SCENARIO_ID                                 ||'|'
FROM   SCENARIO
WHERE  SCENARIO_ID = ${scenarioId}
"""
}

  def selectScenarioForPostgres(scenarioId: String) = {
    s"""
        |SELECT
        |       SCENARIO_ID,
        |       SCENARIO_NM,
        |       USER_ID,
        |       NVL(SYSTEM_ID,0) AS SYSTEM_ID,
        |       NVL(NETWORK_TYPE,0) AS NETWORK_TYPE,
        |       SIDO_CD,
        |       SIGUGUN_CD,
        |       DONG_CD,
        |       SIDO,
        |       SIGUGUN,
        |       DONG,
        |       NVL(STARTX,0) AS STARTX,
        |       NVL(STARTY,0) AS STARTY,
        |       NVL(ENDX,0) AS ENDX,
        |       NVL(ENDY,0) AS ENDY,
        |       NVL(FA_MODEL_ID,0) AS FA_MODEL_ID,
        |       NVL(FA_SEQ,0) AS FA_SEQ,
        |       SCENARIO_DESC,
        |       NVL(USE_BUILDING,0) AS USE_BUILDING,
        |       NVL(USE_MULTIFA,0) AS USE_MULTIFA,
        |       NVL(PRECISION,0) AS PRECISION,
        |       NVL(PWRCTRLCHECKPOINT,0) AS PWRCTRLCHECKPOINT,
        |       NVL(MAXITERATIONPWRCTRL,0) AS MAXITERATIONPWRCTRL,
        |       NVL(RESOLUTION,0) AS RESOLUTION,
        |       NVL(MODEL_RADIUS,0) AS MODEL_RADIUS,
        |       REG_DT,
        |       MODIFY_DT,
        |       NVL(UPPER_SCENARIO_ID,0) AS UPPER_SCENARIO_ID,
        |       NVL(FLOORBUILDING,0) AS FLOORBUILDING,
        |       NVL(FLOOR,0) AS FLOOR,
        |       NVL(FLOORLOSS,0) AS FLOORLOSS,
        |       NVL(SCENARIO_SUB_ID,0) AS SCENARIO_SUB_ID,
        |       NVL(SCENARIO_SOLUTION_NUM,0) AS SCENARIO_SOLUTION_NUM,
        |       NVL(LOSS_TYPE,0) AS LOSS_TYPE,
        |       NVL(RU_CNT,0) AS RU_CNT,
        |       MODIFY_YN,
        |       BATCH_YN,
        |       NVL(TM_STARTX,0) AS TM_STARTX,
        |       NVL(TM_STARTY,0) AS TM_STARTY,
        |       NVL(TM_ENDX,0) AS TM_ENDX,
        |       NVL(TM_ENDY,0) AS TM_ENDY,
        |       REAL_DATE,
        |       REAL_DRM_FILE,
        |       REAL_COMP,
        |       REAL_CATT,
        |       REAL_CATY,
        |       BLD_TYPE,
        |       NVL(RET_PERIOD,0) AS RET_PERIOD,
        |       RET_END_DATETIME,
        |       BUILDINGANALYSIS3D_YN,
        |       NVL(BUILDINGANALYSIS3D_RESOLUTION,0) AS BUILDINGANALYSIS3D_RESOLUTION,
        |       NVL(AREA_ID,0) AS AREA_ID,
        |       BUILDINGANALYSIS3D_RELATED_YN,
        |       NVL(RELATED_ANALYSIS_COVLIMITRSRP,0) AS RELATED_ANALYSIS_COVLIMITRSRP
        |FROM   SCENARIO
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
  }

def selectScenarioIns(scenarioId: String) = {
s"""
SELECT 
'INSERT INTO SCENARIO VALUES ('
||' '  ||NVL(SCENARIO_ID,0)                         
||','''||SCENARIO_NM                                ||''''
||','''||USER_ID                                    ||''''
||','  ||NVL(SYSTEM_ID,0)                           
||','  ||NVL(NETWORK_TYPE,0)                        
||','''||SIDO_CD                                    ||''''
||','''||SIGUGUN_CD                                 ||''''
||','''||DONG_CD                                    ||''''
||','''||SIDO                                       ||''''
||','''||SIGUGUN                                    ||''''
||','''||DONG                                       ||''''
||','  ||NVL(STARTX,0)                              
||','  ||NVL(STARTY,0)                              
||','  ||NVL(ENDX,0)                                
||','  ||NVL(ENDY,0)                                
||','  ||NVL(FA_MODEL_ID,0)                         
||','  ||NVL(FA_SEQ,0)                              
||','''||SCENARIO_DESC                              ||''''
||','  ||NVL(USE_BUILDING,0)                        
||','  ||NVL(USE_MULTIFA,0)                         
||','  ||NVL(PRECISION,0)                           
||','  ||NVL(PWRCTRLCHECKPOINT,0)                   
||','  ||NVL(MAXITERATIONPWRCTRL,0)                 
||','  ||NVL(RESOLUTION,0)                          
||','  ||NVL(MODEL_RADIUS,0)                        
||','''||TO_CHAR(REG_DT,'YYYY-MM-DD HH24:MI:SS')    ||''''
||','''||TO_CHAR(MODIFY_DT,'YYYY-MM-DD HH24:MI:SS') ||''''
||','  ||NVL(UPPER_SCENARIO_ID,0)                   
||','  ||NVL(FLOORBUILDING,0)                       
||','  ||NVL(FLOOR,0)                               
||','  ||NVL(FLOORLOSS,0)                           
||','  ||NVL(SCENARIO_SUB_ID,0)                     
||','  ||NVL(SCENARIO_SOLUTION_NUM,0)               
||','  ||NVL(LOSS_TYPE,0)                           
||','  ||NVL(RU_CNT,0)                              
||','''||MODIFY_YN                                  ||''''
||','''||BATCH_YN                                   ||''''
||','  ||NVL(TM_STARTX,0)                           
||','  ||NVL(TM_STARTY,0)                           
||','  ||NVL(TM_ENDX,0)                             
||','  ||NVL(TM_ENDY,0)                             
||','''||REAL_DATE                                  ||''''
||','''||REAL_DRM_FILE                              ||''''
||','''||REAL_COMP                                  ||''''
||','''||REAL_CATT                                  ||''''
||','''||REAL_CATY                                  ||''''
||','''||BLD_TYPE                                   ||''''
||','  ||NVL(RET_PERIOD,0)                          
||','''||RET_END_DATETIME                           ||''''
||','''||BUILDINGANALYSIS3D_YN                      ||''''
||','  ||NVL(BUILDINGANALYSIS3D_RESOLUTION,0)       
||','  ||NVL(AREA_ID,0)                             
||','''||BUILDINGANALYSIS3D_RELATED_YN              ||''''
||','  ||NVL(RELATED_ANALYSIS_COVLIMITRSRP,0)       
||','  ||ROUND(PRECISION)       
||');'
FROM   SCENARIO
WHERE  SCENARIO_ID = ${scenarioId}
"""
}

}