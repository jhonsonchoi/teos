package com.sccomz.etl.extract.oracle

import java.io.File
import java.io.PrintWriter
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

import com.sccomz.comm.App
import com.sccomz.etl.extract.oracle.sql.ExtractOraDuSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraMobileParameterSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraRuSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraScenarioNrAntennaSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraScenarioNrRuSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraScenarioSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraScheduleSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraSiteSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraFabaseSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraNruetrafficSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraNrsectorparameterSql
import com.sccomz.etl.extract.oracle.sql.ExtractOraNrsystemSql

/*
import com.sccomz.etl.extract.oracle.ExtractOraManager
ExtractOraManager.extractOracleToPostgreIns("8463234")
ExtractOraManager.extractOracleToPostgreIns("8463235")


import com.sccomz.etl.extract.oracle.ExtractOraManager
ExtractOraManager.extractOracleToHadoopCsv("8463233")

8463233	5113566

ExtractOraManager.extractOracleToHadoopCsv("8460064")

ExtractOraManager.extractOracleToHadoopCsv("8463189")
ExtractOraManager.extractOracleToHadoopCsvBatch("20191107")
ExtractOraManager.extractOracleToHadoopCsv("8460178")
ExtractOraManager.extractOracleToHadoopCsv("8460179")
ExtractOraManager.extractOracleToHadoopCsv("8460062")
ExtractOraManager.extractOracleToHadoopCsv("8460063")


 * */
object ExtractOraManager {

  Class.forName(App.dbDriverOra);
  var con:Connection = DriverManager.getConnection(App.dbUrlOra,App.dbUserOra,App.dbPwOra);
  var stat:Statement=con.createStatement();
  var rs:ResultSet = null;
  var tabNm = "";

  def main(args: Array[String]): Unit = {
    //extractOracleToPostgreIns("8463234");
    //extractOracleToPostgreIns("8463235");
    //extractOracleToHadoopCsv("8460064");
    //extractOracleToHadoopCsv("8460178");

    //extractOracleToPostgreIns("8460178");
    //extractOracleToPostgreIns("8460179");
    //extractOracleToPostgreIns("8460062");
    //extractOracleToPostgreIns("8460063");
    
    //8463233	5113566
    
  }

  def extractOracleToPostgreInsByScenario(scenarioId: String): Unit = {
    var tabNm = ""; var qry = ""; var pw: PrintWriter = null

    //--------------------------------------
    tabNm = "SCENARIO";
    //--------------------------------------
    qry = ExtractOraScenarioSql.selectScenarioIns(scenarioId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".sql" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "MOBILE_PARAMETER";
    //--------------------------------------
    qry = ExtractOraMobileParameterSql.selectMobileParameterIns(scenarioId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".sql" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "SCENARIO_NR_RU";
    //--------------------------------------
    qry = ExtractOraScenarioNrRuSql.selectScenarioNrRuIns(scenarioId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".sql" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
  }

  def extractOracleToPostgreInsBySchedule(scheduleId: String): Unit = {
    var tabNm = ""; var qry = "";

    //--------------------------------------
        tabNm = "SCHEDULE"
    //--------------------------------------
    qry = ExtractOraScheduleSql.selectScheduleIns(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    var pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".sql" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
  }

  def extractOracleToHadoopCsvByScenario(scenarioId: String): Unit = {

    var tabNm = ""; var qry = ""; var pw: PrintWriter = null;

/*
    //--------------------------------------
        tabNm = "SCHEDULE"
    //--------------------------------------
    qry = ExtractOraScheduleSql.selectScheduleCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    var pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
*/

    //--------------------------------------
        tabNm = "SCENARIO"
    //--------------------------------------
    qry = ExtractOraScenarioSql.selectScenarioCsv(scenarioId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

/*
     //--------------------------------------
         tabNm = "DU"
     //--------------------------------------
     qry = ExtractOraDuSql.selectDuCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "RU"
     //--------------------------------------
     qry = ExtractOraRuSql.selectRuCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "SITE"
     //--------------------------------------
     qry = ExtractOraSiteSql.selectSiteCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
*/

     //--------------------------------------
         tabNm = "SCENARIO_NR_RU"
     //--------------------------------------
     qry = ExtractOraScenarioNrRuSql.selectScenarioNrRuCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "SCENARIO_NR_ANTENNA"
     //--------------------------------------
     qry = ExtractOraScenarioNrAntennaSql.selectScenarioNrAntennaCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "MOBILE_PARAMETER"
     //--------------------------------------
     qry = ExtractOraMobileParameterSql.selectMobileParameterCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "NRUETRAFFIC"
     //--------------------------------------
     qry = ExtractOraNruetrafficSql.selectNruetrafficCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "NRSECTORPARAMETER" 
     //--------------------------------------
     qry = ExtractOraNrsectorparameterSql.selectNrsectorparameterCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

     //--------------------------------------
         tabNm = "NRSYSTEM" 
     //--------------------------------------
     qry = ExtractOraNrsystemSql.selectNrsystemCsv(scenarioId); println(qry);
     rs = stat.executeQuery(qry);
     pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat" ),"UTF-8");
     while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
     
  }

  def extractOracleToHadoopCsvBySchedule(scheduleId: String): Unit = {

    var tabNm = ""; var qry = "";

    //--------------------------------------
    tabNm = "SCHEDULE"
    //--------------------------------------
    qry = ExtractOraScheduleSql.selectScheduleCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    var pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

/*
    //--------------------------------------
    tabNm = "SCENARIO"
    //--------------------------------------
    qry = ExtractOraScenarioSql.selectScenarioCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "DU"
    //--------------------------------------
    qry = ExtractOraDuSql.selectDuCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "RU"
    //--------------------------------------
    qry = ExtractOraRuSql.selectRuCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "SITE"
    //--------------------------------------
    qry = ExtractOraSiteSql.selectSiteCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "SCENARIO_NR_RU"
    //--------------------------------------
    qry = ExtractOraScenarioNrRuSql.selectScenarioNrRuCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "SCENARIO_NR_ANTENNA"
    //--------------------------------------
    qry = ExtractOraScenarioNrAntennaSql.selectScenarioNrAntennaCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "MOBILE_PARAMETER"
    //--------------------------------------
    qry = ExtractOraMobileParameterSql.selectMobileParameterCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "NRUETRAFFIC"
    //--------------------------------------
    qry = ExtractOraNruetrafficSql.selectNruetrafficCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "NRSECTORPARAMETER"
    //--------------------------------------
    qry = ExtractOraNrsectorparameterSql.selectNrsectorparameterCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    //--------------------------------------
    tabNm = "NRSYSTEM"
    //--------------------------------------
    qry = ExtractOraNrsystemSql.selectNrsystemCsv(scheduleId); println(qry);
    rs = stat.executeQuery(qry);
    pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+scheduleId+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
*/

  }

  def extractOracleToHadoopCsvBatch(workDt:String): Unit = {

    var tabNm = ""; var qry = "";

    //--------------------------------------
        tabNm = "MOBILE_PARAMETER";
    //--------------------------------------
    qry = ExtractOraFabaseSql.selectFabaseCsv(); println(qry);
    rs = stat.executeQuery(qry);
    var pw = new PrintWriter(new File(App.extJavaPath+"/"+tabNm+"_"+workDt+".dat" ),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
    
  }  
  
}