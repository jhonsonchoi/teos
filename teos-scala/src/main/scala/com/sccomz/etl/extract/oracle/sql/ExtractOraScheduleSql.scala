package com.sccomz.etl.extract.oracle.sql

object ExtractOraScheduleSql {

  def selectSchedule(scheduleId:String) = {
    s"""
        |SELECT
        |       TYPE_CD,
        |       NVL(SCENARIO_ID,0) AS SCENARIO_ID,
        |       USER_ID,
        |       PRIORITIZE,
        |       PROCESS_CD,
        |       PROCESS_MSG,
        |       SCENARIO_PATH,
        |       TO_CHAR(REG_DT,'YYYY-MM-DD HH24:MI:SS') AS REG_DT,
        |       TO_CHAR(MODIFY_DT,'YYYY-MM-DD HH24:MI:SS') AS MODIFY_DT,
        |       NVL(RETRY_CNT,0) AS RETRY_CNT,
        |       SERVER_ID,
        |       NVL(BIN_X_CNT,0) AS BIN_X_CNT,
        |       NVL(BIN_Y_CNT,0) AS BIN_Y_CNT,
        |       NVL(RU_CNT,0) AS RU_CNT,
        |       NVL(ANALYSIS_WEIGHT,0) AS ANALYSIS_WEIGHT,
        |       PHONE_NO,
        |       NVL(RESULT_TIME,0) AS RESULT_TIME,
        |       NVL(TILT_PROCESS_TYPE,0) AS TILT_PROCESS_TYPE,
        |       NVL(GEOMETRYQUERY_SCHEDULE_ID,0) AS GEOMETRYQUERY_SCHEDULE_ID,
        |       RESULT_BIT,
        |       INTERWORKING_INFO,
        |       SCHEDULE_ID
        |FROM   SCHEDULE
        |WHERE  SCHEDULE_ID = ${scheduleId}
        |""".stripMargin
  }

  def selectScheduleCsv(scheduleId:String) = {
s"""
SELECT
       TYPE_CD                                    ||'|'||
       NVL(SCENARIO_ID,0)                         ||'|'||
       USER_ID                                    ||'|'||
       PRIORITIZE                                 ||'|'||
       PROCESS_CD                                 ||'|'||
       PROCESS_MSG                                ||'|'||
       SCENARIO_PATH                              ||'|'||
       TO_CHAR(REG_DT,'YYYY-MM-DD HH24:MI:SS')    ||'|'||
       TO_CHAR(MODIFY_DT,'YYYY-MM-DD HH24:MI:SS') ||'|'||
       NVL(RETRY_CNT,0)                           ||'|'||
       SERVER_ID                                  ||'|'||
       NVL(BIN_X_CNT,0)                           ||'|'||
       NVL(BIN_Y_CNT,0)                           ||'|'||
       NVL(RU_CNT,0)                              ||'|'||
       NVL(ANALYSIS_WEIGHT,0)                     ||'|'||
       PHONE_NO                                   ||'|'||
       NVL(RESULT_TIME,0)                         ||'|'||
       NVL(TILT_PROCESS_TYPE,0)                   ||'|'||
       NVL(GEOMETRYQUERY_SCHEDULE_ID,0)           ||'|'||
       RESULT_BIT                                 ||'|'||
       INTERWORKING_INFO                          ||'|'||
       SCHEDULE_ID                                ||'|'
FROM   SCHEDULE
WHERE  SCHEDULE_ID = ${scheduleId}
"""
}

  def selectScheduleForPostgres(scheduleId:String) = {
    s"""
       |SELECT
       |       SCHEDULE_ID,
       |       TYPE_CD,
       |       NVL(SCENARIO_ID,0) AS SCENARIO_ID,
       |       USER_ID,
       |       PRIORITIZE,
       |       PROCESS_CD,
       |       PROCESS_MSG,
       |       SCENARIO_PATH,
       |       REG_DT,
       |       MODIFY_DT,
       |       NVL(RETRY_CNT,0) AS RETRY_CNT,
       |       SERVER_ID,
       |       NVL(BIN_X_CNT,0) AS BIN_X_CNT,
       |       NVL(BIN_Y_CNT,0) AS BIN_Y_CNT,
       |       NVL(RU_CNT,0) AS RU_CNT,
       |       NVL(ANALYSIS_WEIGHT,0) AS ANALYSIS_WEIGHT,
       |       PHONE_NO,
       |       NVL(RESULT_TIME,0) AS RESULT_TIME,
       |       NVL(TILT_PROCESS_TYPE,0) AS TILT_PROCESS_TYPE,
       |       NVL(GEOMETRYQUERY_SCHEDULE_ID,0) AS GEOMETRYQUERY_SCHEDULE_ID,
       |       RESULT_BIT,
       |       INTERWORKING_INFO
       |FROM   SCHEDULE
       |WHERE  SCHEDULE_ID = ${scheduleId}
       |""".stripMargin
  }

def selectScheduleIns(scheduleId:String) = {
s"""
SELECT 
'INSERT INTO SCHEDULE VALUES ('
||' '  ||NVL(SCHEDULE_ID,0)                         
||','''||TYPE_CD                                    ||''''
||','  ||NVL(SCENARIO_ID,0)                         
||','''||USER_ID                                    ||''''
||','''||PRIORITIZE                                 ||''''
||','''||PROCESS_CD                                 ||''''
||','''||PROCESS_MSG                                ||''''
||','''||SCENARIO_PATH                              ||''''
||','''||TO_CHAR(REG_DT,'YYYY-MM-DD HH24:MI:SS')    ||''''
||','''||TO_CHAR(MODIFY_DT,'YYYY-MM-DD HH24:MI:SS') ||''''
||','  ||NVL(RETRY_CNT,0)                           
||','''||SERVER_ID                                  ||''''
||','  ||NVL(BIN_X_CNT,0)                           
||','  ||NVL(BIN_Y_CNT,0)                           
||','  ||NVL(RU_CNT,0)                              
||','  ||NVL(ANALYSIS_WEIGHT,0)                     
||','''||PHONE_NO                                   ||''''
||','  ||NVL(RESULT_TIME,0)                         
||','  ||NVL(TILT_PROCESS_TYPE,0)                   
||','  ||NVL(GEOMETRYQUERY_SCHEDULE_ID,0)           
||','''||RESULT_BIT                                 ||''''
||','''||INTERWORKING_INFO                          ||''''
||');'
FROM   SCHEDULE
WHERE  SCHEDULE_ID = ${scheduleId}
"""
}



}