package com.sccomz.etl.extract.oracle.sql

object ExtractOraNruetrafficSql {

  def selectNruetraffic(scenarioId: String) = {
    s"""
        |SELECT
        |       NVL(SYSTEM_ID,0) AS SYSTEM_ID,
        |       NVL(FA_SEQ,0) AS FA_SEQ,
        |       NVL(DLUL_TYPE,0) AS DLUL_TYPE,
        |       NVL(RX_MODULATION,0) AS RX_MODULATION,
        |       NVL(RX_LAYER,0) AS RX_LAYER,
        |       NVL(MODULATION1,0) AS MODULATION1,
        |       NVL(MODULATION2,0) AS MODULATION2,
        |       NVL(MODULATION3,0) AS MODULATION3,
        |       NVL(MODULATION4,0) AS MODULATION4,
        |       NVL(MODULATION5,0) AS MODULATION5,
        |       NVL(MODULATION6,0) AS MODULATION6,
        |       NVL(MODULATION7,0) AS MODULATION7,
        |       NVL(MODULATION8,0) AS MODULATION8,
        |       NVL(MODULATION9,0) AS MODULATION9,
        |       NVL(MODULATION10,0) AS MODULATION10,
        |       NVL(MODULATION11,0) AS MODULATION11,
        |       NVL(MODULATION12,0) AS MODULATION12,
        |       NVL(MODULATION13,0) AS MODULATION13,
        |       NVL(MODULATION14,0) AS MODULATION14,
        |       NVL(MODULATION15,0) AS MODULATION15,
        |       NVL(LAYER1,0) AS LAYER1,
        |       NVL(LAYER2,0) AS LAYER2,
        |       NVL(LAYER3,0) AS LAYER3,
        |       NVL(LAYER4,0) AS LAYER4,
        |       NVL(LAYER5,0) AS LAYER5,
        |       NVL(LAYER6,0) AS LAYER6,
        |       NVL(LAYER7,0) AS LAYER7,
        |       NVL(LAYER8,0) AS LAYER8,
        |       NVL(LAYER9,0) AS LAYER9,
        |       NVL(LAYER10,0) AS LAYER10,
        |       NVL(LAYER11,0) AS LAYER11,
        |       NVL(LAYER12,0) AS LAYER12,
        |       NVL(LAYER13,0) AS LAYER13,
        |       NVL(LAYER14,0) AS LAYER14,
        |       NVL(LAYER15,0) AS LAYER15,
        |       NVL(CODERATE1,0) AS CODERATE1,
        |       NVL(CODERATE2,0) AS CODERATE2,
        |       NVL(CODERATE3,0) AS CODERATE3,
        |       NVL(CODERATE4,0) AS CODERATE4,
        |       NVL(CODERATE5,0) AS CODERATE5,
        |       NVL(CODERATE6,0) AS CODERATE6,
        |       NVL(CODERATE7,0) AS CODERATE7,
        |       NVL(CODERATE8,0) AS CODERATE8,
        |       NVL(CODERATE9,0) AS CODERATE9,
        |       NVL(CODERATE10,0) AS CODERATE10,
        |       NVL(CODERATE11,0) AS CODERATE11,
        |       NVL(CODERATE12,0) AS CODERATE12,
        |       NVL(CODERATE13,0) AS CODERATE13,
        |       NVL(CODERATE14,0) AS CODERATE14,
        |       NVL(CODERATE15,0) AS CODERATE15,
        |       NVL(SNR1,0) AS SNR1,
        |       NVL(SNR2,0) AS SNR2,
        |       NVL(SNR3,0) AS SNR3,
        |       NVL(SNR4,0) AS SNR4,
        |       NVL(SNR5,0) AS SNR5,
        |       NVL(SNR6,0) AS SNR6,
        |       NVL(SNR7,0) AS SNR7,
        |       NVL(SNR8,0) AS SNR8,
        |       NVL(SNR9,0) AS SNR9,
        |       NVL(SNR10,0) AS SNR10,
        |       NVL(SNR11,0) AS SNR11,
        |       NVL(SNR12,0) AS SNR12,
        |       NVL(SNR13,0) AS SNR13,
        |       NVL(SNR14,0) AS SNR14,
        |       NVL(SNR15,0) AS SNR15,
        |       NVL(SCENARIO_ID,0) AS SCENARIO_ID
        |FROM   NRUETRAFFIC
        |WHERE  SCENARIO_ID = ${scenarioId}
        |""".stripMargin
  }

def selectNruetrafficCsv(scenarioId: String) = {
s"""
SELECT 
       NVL(SYSTEM_ID,0)     ||'|'||
       NVL(FA_SEQ,0)        ||'|'||
       NVL(DLUL_TYPE,0)     ||'|'||
       NVL(RX_MODULATION,0) ||'|'||
       NVL(RX_LAYER,0)      ||'|'||
       NVL(MODULATION1,0)   ||'|'||
       NVL(MODULATION2,0)   ||'|'||
       NVL(MODULATION3,0)   ||'|'||
       NVL(MODULATION4,0)   ||'|'||
       NVL(MODULATION5,0)   ||'|'||
       NVL(MODULATION6,0)   ||'|'||
       NVL(MODULATION7,0)   ||'|'||
       NVL(MODULATION8,0)   ||'|'||
       NVL(MODULATION9,0)   ||'|'||
       NVL(MODULATION10,0)  ||'|'||
       NVL(MODULATION11,0)  ||'|'||
       NVL(MODULATION12,0)  ||'|'||
       NVL(MODULATION13,0)  ||'|'||
       NVL(MODULATION14,0)  ||'|'||
       NVL(MODULATION15,0)  ||'|'||
       NVL(LAYER1,0)        ||'|'||
       NVL(LAYER2,0)        ||'|'||
       NVL(LAYER3,0)        ||'|'||
       NVL(LAYER4,0)        ||'|'||
       NVL(LAYER5,0)        ||'|'||
       NVL(LAYER6,0)        ||'|'||
       NVL(LAYER7,0)        ||'|'||
       NVL(LAYER8,0)        ||'|'||
       NVL(LAYER9,0)        ||'|'||
       NVL(LAYER10,0)       ||'|'||
       NVL(LAYER11,0)       ||'|'||
       NVL(LAYER12,0)       ||'|'||
       NVL(LAYER13,0)       ||'|'||
       NVL(LAYER14,0)       ||'|'||
       NVL(LAYER15,0)       ||'|'||
       NVL(CODERATE1,0)     ||'|'||
       NVL(CODERATE2,0)     ||'|'||
       NVL(CODERATE3,0)     ||'|'||
       NVL(CODERATE4,0)     ||'|'||
       NVL(CODERATE5,0)     ||'|'||
       NVL(CODERATE6,0)     ||'|'||
       NVL(CODERATE7,0)     ||'|'||
       NVL(CODERATE8,0)     ||'|'||
       NVL(CODERATE9,0)     ||'|'||
       NVL(CODERATE10,0)    ||'|'||
       NVL(CODERATE11,0)    ||'|'||
       NVL(CODERATE12,0)    ||'|'||
       NVL(CODERATE13,0)    ||'|'||
       NVL(CODERATE14,0)    ||'|'||
       NVL(CODERATE15,0)    ||'|'||
       NVL(SNR1,0)          ||'|'||
       NVL(SNR2,0)          ||'|'||
       NVL(SNR3,0)          ||'|'||
       NVL(SNR4,0)          ||'|'||
       NVL(SNR5,0)          ||'|'||
       NVL(SNR6,0)          ||'|'||
       NVL(SNR7,0)          ||'|'||
       NVL(SNR8,0)          ||'|'||
       NVL(SNR9,0)          ||'|'||
       NVL(SNR10,0)         ||'|'||
       NVL(SNR11,0)         ||'|'||
       NVL(SNR12,0)         ||'|'||
       NVL(SNR13,0)         ||'|'||
       NVL(SNR14,0)         ||'|'||
       NVL(SNR15,0)         ||'|'||
       NVL(SCENARIO_ID,0)   ||'|'
FROM   NRUETRAFFIC 
WHERE  SCENARIO_ID = ${scenarioId}
"""
}

def selectNruetrafficIns(scheduleId:String) = {
s"""
"""
}



}