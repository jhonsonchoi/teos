package com.sccomz.etl.extract.post

import java.io._
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.Statement

import scala.collection._
import scala.collection.mutable.Map
import com.sccomz.comm.App
import com.sccomz.etl.extract.post.sql.ExtractJobDisSql
import com.sccomz.etl.extract.post.sql.ExtractPostLosEngResultSql
import com.sccomz.etl.extract.post.sql.ExtractPostLosBldResultSql
import com.sccomz.etl.extract.post.sql.ExtractPostScenarioNrRuDemSql
import com.sccomz.etl.load.LoadHdfsLosManager
import com.sccomz.etl.load.LoadHdfsManager
import com.sccomz.schedule.real.ExecuteJob
import java.util.logging.Logger

import com.sccomz.rdb.PostgresCon

/*
import com.sccomz.etl.extract.post.ExtractLoadPostManager
ExtractLoadPostManager.deleteJobDisExt("8463246");



import com.sccomz.etl.extract.post.ExtractLoadPostManager
ExtractLoadPostManager.deleteJobDisExt("8460178");


import com.sccomz.etl.extract.post.ExtractLoadPostManager
ExtractLoadPostManager.deleteJobDisExt("8463290");

ExtractLoadPostManager.monitorJobDis("8460178");


ExtractLoadPostManager.executeExtractLoadAvg("8463233","5113566");

ExtractLoadPostManager.monitorJobDis("8463233","5113566");

ExtractJobDisSql.selectRuCnt("8463233");
ExtractLoadPostManager.extractPostToHadoopCsv("8460062","1012242284","gis01");

8463233	5113566

spark.sql("SELECT COUNT(*) FROM SCENARIO_NR_RU_AVG_HEIGHT WHERE SCENARIO_ID = 5113566;)").take(100).foreach(println);


spark.sql("SELECT COUNT(*) FROM (SELECT DISTINCT SCHEDULE_ID,RU_ID FROM RESULT_NR_BF_LOS_RU)").take(100).foreach(println);


spark.sql("SELECT COUNT(*) FROM SCENARIO_NR_RU_AVG_HEIGHT WHERE SCENARIO_ID = 5113566;)").take(100).foreach(println);

spark.sql("SELECT COUNT(*) FROM (SELECT DISTINCT SCHEDULE_ID,RU_ID FROM RESULT_NR_2D_LOS_RU)").take(100).foreach(println);

spark.sql("SELECT DISTINCT 'INSERT INTO TEMP001 VALUE ('''||RU_ID||''')' FROM RESULT_NR_2D_LOS_RU WHERE SCHEDULE_ID = 8460062").take(100).foreach(println);

SELECT DISTINCT SCHEDULE_ID,RU_ID
FROM RESULT_NR_2D_LOS_RU

 * */
object ExtractLoadPostManager {

//  var scenarioId = "";
  var typeCd = "";
  
  def main(args: Array[String]): Unit = {
    //monitorJobDis("");
    println("ExtractLoadPostManager start");

    //extractPostToHadoopCsv("8460062","1012242284","gis01");

    val scheduleId = "8463235"
    val schedule = ExecuteJob.selectSchedule(scheduleId)

    monitorJobDis(scheduleId, schedule.scenarioId,true);

    //5113766
    println("ExtractLoadPostManager end");
  }

  def deleteJobDisExt(scheduleId:String): Unit = {
    val con = PostgresCon.getCon()
    val stmt = con.createStatement()
    val qry = ExtractJobDisSql.deleteJobDisExt(scheduleId)
    println(qry)
    stmt.execute(qry)
    stmt.close()
    con.close()
  }


  /**
   * Postgresql => HDFS
   *
   * @param scheduleId
   * @param scenarioId
   * @param isRetry
   */
  def monitorJobDis(scheduleId:String, scenarioId:String,isRetry:Boolean): Unit = {

    println("monitorJobDis start scheduleId="+scheduleId)
    
    var qry = "";

    if(isRetry) deleteJobDisExt(scheduleId)
 
//    scenarioId = ExecuteJob.selectScenarioId(scheduleId)
    typeCd = ExecuteJob.selectTypeCd(scheduleId)
 
    var ruCnt = 0; var post_done_cnt = 0; var extCnt = 0; var extDoneCnt = -1;
    var avgCnt = 1;

    var loofCnt = 0;
    var isLoof = true;
    var exeLoofCnt = 0;
    // 빌딩분석여부
    var bdYn = "N";

    val con = PostgresCon.getCon()
    val stmt = con.createStatement()

    qry = ExtractJobDisSql.selectBdYn(scenarioId); println(qry);
    val rs = stmt.executeQuery(qry)
    rs.next()
    bdYn = rs.getString("BD_YN");
    rs.close()
    stmt.close()

    try {
//      while(isLoof) {
        loofCnt += 1;
        // JOB_DIS 와 JOB_DIS_ETL 에서 추출
        qry = ExtractJobDisSql.selectRuCnt(scheduleId,typeCd,bdYn); println(qry)

        val stmt = con.createStatement()
        val rs = stmt.executeQuery(qry)

        rs.next()
        // 18 0 18
        ruCnt  = rs.getInt("RU_CNT");
        extDoneCnt = rs.getInt("EXT_DONE_CNT");
        extCnt = rs.getInt("EXT_CNT");

        rs.close()
        stmt.close()


        // 루프 종료
        if(ruCnt > 0 && ruCnt==extDoneCnt) isLoof = false
        
        println("extCnt="+extCnt)

        if(extCnt>0) {
          exeLoofCnt += 1;
          // 처음 한 번만 실행
          if(exeLoofCnt==1) {
            executeExtractLoadOneTime(scenarioId)
          }
          // 반복 실행
          executeExtractLoad(scheduleId,typeCd,bdYn)
        }
//        println("loofCnt="+loofCnt);
//        Thread.sleep(1000*3);

//      }

      println("monitorJobDis end scheduleId="+scheduleId)

    } catch {
      case ex: Throwable => {println("ex="+ex);throw ex}
    } finally {
      con.close()
    }
  }

  /**
   * RU 당 하나씩 생성.
   * <p>RESULT_NR_BF_LOS_RU 또는 RESULT_NR_2D_LOS_RU 복제.
   * <p>진행 상태는 JOB_DIS 로 관리.
   * <p><tabNm>_<scheduleId>_<ruId>.dat
   *
   * @param scheduleId
   * @param typeCd
   * @param bdYn
   */
  def executeExtractLoad(scheduleId:String,typeCd:String,bdYn:String): Unit = {

    println("executeExtractLoad start scheduleId="+scheduleId)
    
    
    var qry = "";
    var ruId = ""; var clusterName = "";

    val con = PostgresCon.getCon()
    val stmt = con.createStatement()

    qry = ExtractJobDisSql.selectExtRu(scheduleId,typeCd,bdYn); println(qry);
    val rs = stmt.executeQuery(qry);

    // 추출 목록
    val extList = mutable.Map[String,String]();

    while(rs.next()) {
    //if(rs.next()) {
        extList += (rs.getString("RU_ID") -> rs.getString("CLUSTER_NAME"));
        ruId  = rs.getString("RU_ID");
        clusterName = rs.getString("CLUSTER_NAME");
        //
    }

    rs.close()
    stmt.close()
    con.close()

    // 추출 목록에 대한 처리
    for(ext <- extList) {
        ruId  = ext._1; clusterName = ext._2;
        println("ruId="+ruId);

      // RU 별 실행 초기 상태 생성
//        insertJobDisExt(scheduleId,ruId,"4")
//        println("ruId1="+ruId);
        extractPostToHadoopCsv(scheduleId,ruId,clusterName,typeCd,bdYn);
//        println("ruId2="+ruId);
      // spark 실행
//        LoadHdfsLosManager.executeRealPostToHdfs(scheduleId,ruId,typeCd,bdYn);
//        println("ruId3="+ruId);
      // RU 별 실행 상태 업데이트
//        updateJobDisExt(scheduleId,ruId,"5")
//        println("ruId4="+ruId);
    }

    println("executeExtractLoad end scheduleId="+scheduleId)
    
    //LoadHdfsLosManager.sparkClose();
  }

  /**
   * if typeCd=="SC051" && bdYn=="Y"
   *   then RESULT_NR_BF_LOS_RU
   *   else RESULT_NR_2D_LOS_RU
   *
   * @param scheduleId
   * @param ruId
   * @param clusterName
   * @param typeCd
   * @param bdYn
   */
  def extractPostToHadoopCsv(scheduleId: String,ruId:String,clusterName:String,typeCd:String,bdYn:String) : Unit = {

    println("extractPostToHadoopCsv start scheduleId="+scheduleId)
    
    
    //var dbUrl = if(clusterName=="gis01") App.dbUrlPost else if(clusterName=="gis02") App.dbUrlPost2 else if(clusterName=="gis03") App.dbUrlPost3 else if(clusterName=="gis04") App.dbUrlPost4 else App.dbUrlPost;
    //var con = DriverManager.getConnection(dbUrl,App.dbUserPost,App.dbPwPost);

    val cfRoot =
      clusterName match {
        case "gis01" => "postgresql-1"
        case "gis02" => "postgresql-2"
        case "gis03" => "postgresql-3"
        case "gis04" => "postgresql-4"
      }

    val con = PostgresCon.getCon(cfRoot)


    var tabNm = ""; var qry = "";
    var filePathNm = "";

    if(typeCd=="SC051" && bdYn=="Y") {
      //---------------------------------------
           tabNm = "RESULT_NR_BF_LOS_RU"
      //---------------------------------------
      qry = ExtractPostLosBldResultSql.selectLosBldResultCsv(scheduleId, ruId); println(qry);

      val stmt = con.createStatement()
      val rs = stmt.executeQuery(qry)

      filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
      var pw = new PrintWriter(new File(filePathNm),"UTF-8");
      while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

      rs.close()
      stmt.close()

      ////---------------------------------------
      //     tabNm = "TREE_BLD_RESULT";
      ////---------------------------------------
      //qry = ExtractPostTreeBldResultSql.selectTreeBldResultCsv(scheduleId,ruId); println(qry);
      //rs = stat.executeQuery(qry);
      //var filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
      //var pw = new PrintWriter(new File(filePathNm),"UTF-8");
      //while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;
    } else {

      //---------------------------------------
           tabNm = "RESULT_NR_2D_LOS_RU";
      //---------------------------------------
      qry = ExtractPostLosEngResultSql.selectLosEngResultCsv(scheduleId, ruId); println(qry);

      val stmt = con.createStatement()
      val rs = stmt.executeQuery(qry)

      filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
      var pw = new PrintWriter(new File(filePathNm),"UTF-8");
      while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

      rs.close()
      stmt.close()

    //
    // //---------------------------------------
    //      tabNm = "TREE_ENG_RESULT";
    // //---------------------------------------
    // qry = ExtractPostTreeEngResultSql.selectTreeEngResultCsv(scheduleId,ruId); println(qry);
    // rs = stat.executeQuery(qry);
    // filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
    // pw = new PrintWriter(new File(filePathNm),"UTF-8");
    // while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    }

    con.close()

    println("extractPostToHadoopCsv end scheduleId="+scheduleId)
  }

  def extractPostToHadoopCsv2D(scheduleId: String,ruId:String,clusterName:String) : Unit = {

    println("extractPostToHadoopCsv2D start scheduleId="+scheduleId)


    //var dbUrl = if(clusterName=="gis01") App.dbUrlPost else if(clusterName=="gis02") App.dbUrlPost2 else if(clusterName=="gis03") App.dbUrlPost3 else if(clusterName=="gis04") App.dbUrlPost4 else App.dbUrlPost;
    //var con = DriverManager.getConnection(dbUrl,App.dbUserPost,App.dbPwPost);

    val cfRoot =
      clusterName match {
        case "gis01" => "postgresql-1"
        case "gis02" => "postgresql-2"
        case "gis03" => "postgresql-3"
        case "gis04" => "postgresql-4"
      }

    val con = PostgresCon.getCon(cfRoot)


    var tabNm = ""; var qry = "";
    var filePathNm = "";


    //---------------------------------------
    tabNm = "RESULT_NR_2D_LOS_RU";
    //---------------------------------------
    qry = ExtractPostLosEngResultSql.selectLosEngResultCsv(scheduleId, ruId); println(qry);

    val stmt = con.createStatement()
    val rs = stmt.executeQuery(qry)

    filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
    var pw = new PrintWriter(new File(filePathNm),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    rs.close()
    stmt.close()

    //
    // //---------------------------------------
    //      tabNm = "TREE_ENG_RESULT";
    // //---------------------------------------
    // qry = ExtractPostTreeEngResultSql.selectTreeEngResultCsv(scheduleId,ruId); println(qry);
    // rs = stat.executeQuery(qry);
    // filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
    // pw = new PrintWriter(new File(filePathNm),"UTF-8");
    // while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;


    con.close()

    println("extractPostToHadoopCsv2D end scheduleId="+scheduleId)
  }

  def extractPostToHadoopCsvBF(scheduleId: String,ruId:String,clusterName:String) : Unit = {

    println("extractPostToHadoopCsvBF start scheduleId="+scheduleId)


    //var dbUrl = if(clusterName=="gis01") App.dbUrlPost else if(clusterName=="gis02") App.dbUrlPost2 else if(clusterName=="gis03") App.dbUrlPost3 else if(clusterName=="gis04") App.dbUrlPost4 else App.dbUrlPost;
    //var con = DriverManager.getConnection(dbUrl,App.dbUserPost,App.dbPwPost);

    val cfRoot =
      clusterName match {
        case "gis01" => "postgresql-1"
        case "gis02" => "postgresql-2"
        case "gis03" => "postgresql-3"
        case "gis04" => "postgresql-4"
      }

    val con = PostgresCon.getCon(cfRoot)


    var tabNm = ""; var qry = "";
    var filePathNm = "";

    //---------------------------------------
    tabNm = "RESULT_NR_BF_LOS_RU"
    //---------------------------------------
    qry = ExtractPostLosBldResultSql.selectLosBldResultCsv(scheduleId, ruId); println(qry);

    val stmt = con.createStatement()
    val rs = stmt.executeQuery(qry)

    filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
    var pw = new PrintWriter(new File(filePathNm),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    rs.close()
    stmt.close()

    ////---------------------------------------
    //     tabNm = "TREE_BLD_RESULT";
    ////---------------------------------------
    //qry = ExtractPostTreeBldResultSql.selectTreeBldResultCsv(scheduleId,ruId); println(qry);
    //rs = stat.executeQuery(qry);
    //var filePathNm = App.extJavaPath+"/"+tabNm+"_"+scheduleId+"_"+ruId+".dat"; println(filePathNm);
    //var pw = new PrintWriter(new File(filePathNm),"UTF-8");
    //while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    con.close()

    println("extractPostToHadoopCsvBF end scheduleId="+scheduleId)
  }

  /**
   * SCENARIO_NR_RU_AVG_HEIGHT 복제. 시나리오 당 하나
   * <p><tabNm>_<scheduleId>.dat
   *
   * @param scenarioId
   */
  def executeExtractLoadOneTime(scenarioId: String): Unit = {
    println("executeExtractLoadOneTime start scenarioId="+scenarioId)
    
    var tabNm = ""; var qry = "";

    //---------------------------------------
         tabNm = "SCENARIO_NR_RU_AVG_HEIGHT";
    //---------------------------------------
    val con = PostgresCon.getCon()
    val stmt = con.createStatement()

    qry = ExtractPostScenarioNrRuDemSql.selectScenarioNrRuDemCsv(scenarioId); println(qry);
    val rs = stmt.executeQuery(qry);
    val filePathNm = App.extJavaPath+"/"+tabNm+"_"+scenarioId+".dat"; println(filePathNm);
    val pw = new PrintWriter(new File(filePathNm),"UTF-8");
    while(rs.next()) { pw.write(rs.getString(1)+"\n") }; pw.close;

    rs.close()
    stmt.close()
    con.close()

//    LoadHdfsManager.postAvgToHdfs(scheduleId, scenarioId);

    //---------------------------------------
         tabNm = "BLD_LIST";
    //---------------------------------------

    println("executeExtractLoadOneTime end scenarioId="+scenarioId)

  }

  def insertJobDisExt(scheduleId:String,ruId:String,stat2:String) : Unit = {
    val con = PostgresCon.getCon()
    val stmt = con.createStatement()

    val qry = ExtractJobDisSql.insertJobDisExt(scheduleId, ruId, stat2); println(qry); stmt.execute(qry)

    stmt.close()
    con.close()
  }

  def updateJobDisExt(scheduleId:String,ruId:String,stat2:String) : Unit = {
    val con = PostgresCon.getCon()
    val stmt = con.createStatement()

    val qry = ExtractJobDisSql.updateJobDisExt(scheduleId, ruId, stat2); println(qry); stmt.execute(qry)

    stmt.close()
    con.close()
  }


}