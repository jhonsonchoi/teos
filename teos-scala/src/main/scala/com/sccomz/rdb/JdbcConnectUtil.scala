package com.sccomz.rdb

import com.typesafe.config.{Config, ConfigFactory}

object JdbcConnectUtil {

  val cf = ConfigFactory.load()

  def getJdbcConnect(root: String): JdbcConnect = {
    JdbcConnect(cf.getString(s"${root}.driver"), cf.getString(s"${root}.url"), cf.getString(s"${root}.user"), cf.getString(s"${root}.password"))
  }

}

case class JdbcConnect(driver: String, url: String, user: String, password: String)

