package com.sccomz.rdb

import java.sql.{Connection, DriverManager, ResultSet, Statement}

import com.typesafe.config.ConfigFactory

/**
  * todo to use connection pool
  */
object PostgresCon {
  val cf = ConfigFactory.load()

  def getCon(cfRoot: String = "postgres"): Connection = {
    val host = cf.getString(s"${cfRoot}.host")
    val port = cf.getString(s"${cfRoot}.port")
    val service = cf.getString(s"${cfRoot}.service")
    val user = cf.getString(s"${cfRoot}.user")
    val password = cf.getString(s"${cfRoot}.password")
    //    Class.forName(App.dbDriverOra);

    val url = s"jdbc:postgresql://${host}:${port}/${service}"

    println(s"cf-root=$cfRoot url=$url")

    DriverManager.getConnection(url, user, password)
  }

  def getCon(jdbcConnect: JdbcConnect): Connection = {
    val url = jdbcConnect.url
    val user = jdbcConnect.user
    val password = jdbcConnect.password

    println(s"url=$url")

    DriverManager.getConnection(url, user, password)
  }

  def executeQuery(con: Connection, qry: String): ResultSet = {
    println(qry)
    val stmt = con.createStatement()
    stmt.executeQuery(qry)
  }

  def executeQuery(stmt: Statement, qry: String): ResultSet = {
    println(qry)
    stmt.executeQuery(qry)
  }

  def executeUpdate(con: Connection, qry: String): Int = {
    println(qry)
    val stmt = con.createStatement()
    stmt.executeUpdate(qry)
  }

  def executeUpdate(stmt: Statement, qry: String): Unit = {
    println(qry)
    stmt.execute(qry)
  }

}
