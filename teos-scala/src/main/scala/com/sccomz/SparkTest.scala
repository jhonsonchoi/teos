package com.sccomz

import org.apache.spark.sql.SparkSession

object SparkTest {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Spark Example")
      .getOrCreate()

    import spark.implicits._
    val df = Seq(1, 2, 3).toDS()


    df.show()

//    Thread.sleep(60000)

    spark.stop();
  }

}

