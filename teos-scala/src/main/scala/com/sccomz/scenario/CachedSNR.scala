package com.sccomz.scenario

import java.sql.{PreparedStatement, Statement}

import com.sccomz.etl.extract.oracle.sql.{ExtractOraMobileParameterSql, ExtractOraScenarioNrRuSql}
import com.sccomz.rdb.{JdbcConnect, OracleCon, PostgresCon}
import org.slf4j.LoggerFactory


/**
 * SCENARION_NR_RU
 *
 * Postgres GIS 에 요청할 때, 이미 분석된 것은 분석 요청에서 제외한다.
 *
 */
trait CachedSNR {

  /**
   * 캐쉬 기능에 대해서 정리하기 위해 임시로 만든 메서드임
   * todo 삭제
   */
  def cache(scenarioId: String): Unit = {
    // scenario_nr_ru, mobile_parameter 에서 기준 데이터 취득
    //    val mpHeight = mobileParameter(scenarioId)
    //    val snrList = scenarioNrRu(scenarioId)



    /*
       이미 계산돼 있는가 체크.
       3D 시나리오는 반드시 3D 시나리오로 체크해야 함.
       시나리오가 완료된 것만 사용. (물론 자신은 제외?)

        for postgres

        select snr.scenario_id, snr.ru_id, snr.xposition, snr.yposition, snr.height, mp.height as mp_height
        from scenario_nr_ru as snr, mobile_parameter as mp
        where snr.scenario_id = cast(mp.scenario_id as text)
        and snr.ru_id = '1011760619'
        and snr.xposition='201716.73146117'
        and snr.yposition='446167.875123306'
        and snr.height=18
        and mp.height=1.5
        ;

        for hive

        select snr.scenario_id, snr.ru_id, snr.xposition, snr.yposition, snr.height, mp.height as mp_height
        from scenario_nr_ru as snr, mobile_parameter as mp
        where snr.scenario_id = mp.scenario_id
        and snr.ru_id = '1011760619'
        and snr.xposition='201716.73146117'
        and snr.yposition='446167.875123306'
        and snr.height=18
        and mp.height=1.5
        ;

        // 3D 는 3D 시나리오만 출력돼야 함.
        5113566	1011760619	201716.73146117	446167.875123306	18	1.5
        5117967	1011760619	201716.73146117	446167.875123306	18	1.5
        5113766	1011760619	201716.73146117	446167.875123306	18	1.5
    */

    // postgres 에 필요한 정보만 요청
    // scenario_nr_ru 에 전체목록을 전달해야 함
    // scenario_nr_ru 에 is_cached 칼럼에 캐쉬 히트 여부, cache_scenario_id 칼럼에 사용할 기존 데이터 위치를 추가

    // 이미 Hive 에 있는 데이터는 기존 데이터를 복사
    // 2D 시나리오의 경우, RESULT_NR_2D_LOS_RU 에서 스케쥴아이디와 RU_ID 를 키로 새로운 스케쥴아이디를 사용해 데이터 복사
    // 3D 시나리오의 경우, RESULT_NR_2D_LOS_RU 에서 2D 스케쥴아이디와 RU_ID 를 키로 데이터 복사, RESULT_NR_BF_LOS_RU 에서 3D 스케쥴아이디와 RU_ID 를 키로 새로운 스케쥴아이디를 사용해 데이터 복사

    // postgres 의 계산 완료 체크
    // JOB_DIS 테이블의 stat 칼럼 체크
  }

  /**
   * copy scenario_nr_ru to postgres
   * 캐쉬를 참고해서 SCENARIO_NR_RU 를 작성
   *
   * @param targetJdbcConnects
   * @param scenarioId
   * @param stmt
   */
  def copySNR(targetJdbcConnects: Array[JdbcConnect], scenarioId: String)(implicit stmt: Statement): Unit = {
    println(s"SCENARIO_NR_RU")

    val delQry = s"DELETE FROM SCENARIO_NR_RU WHERE scenario_id = '${scenarioId}'"

    val insQry =
      s"""INSERT INTO SCENARIO_NR_RU (
         | scenario_id,
         | enb_id,
         | pci,
         | pci_port,
         | ru_id,
         | maker,
         | sector_ord,
         | repeaterattenuation,
         | repeaterpwrratio,
         | ru_seq,
         | radius,
         | feeder_loss,
         | noisefloor,
         | correction_value,
         | fade_margin,
         | xposition,
         | yposition,
         | height,
         | site_addr,
         | type,
         | status,
         | sisul_cd,
         | msc,
         | bsc,
         | networkid,
         | usabletrafficch,
         | systemid,
         | ru_type,
         | fa_model_id,
         | network_type,
         | resolution,
         | fa_seq,
         | site_startx,
         | site_starty,
         | site_endx,
         | site_endy,
         | x_bin_cnt,
         | y_bin_cnt,
         | is_cached,
         | cache_scenario_id
         | ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
         |""".stripMargin

    // 기존 데이터 삭제 단계

    val conn1 = PostgresCon.getCon(targetJdbcConnects(0))
    val stmt1 = conn1.createStatement()
    stmt1.executeUpdate(delQry)
    stmt1.close()
    val pstmt1 = conn1.prepareStatement(insQry)

    val conn2 = PostgresCon.getCon(targetJdbcConnects(1))
    val stmt2 = conn2.createStatement()
    stmt2.executeUpdate(delQry)
    stmt2.close()
    val pstmt2 = conn2.prepareStatement(insQry)

    val conn3 = PostgresCon.getCon(targetJdbcConnects(2))
    val stmt3 = conn3.createStatement()
    stmt3.executeUpdate(delQry)
    stmt3.close()
    val pstmt3 = conn3.prepareStatement(insQry)

    val conn4 = PostgresCon.getCon(targetJdbcConnects(3))
    val stmt4 = conn4.createStatement()
    stmt4.executeUpdate(delQry)
    stmt4.close()
    val pstmt4 = conn4.prepareStatement(insQry)

    // 복사할 데이터 구성

    val dim = dimension(scenarioId)

    println(s"dimension=$dim")

    val mpHeight = mobileParameter(scenarioId)
    //    val snrList = scenarioNrRu(scenarioId)

    println(s"mp_height=$mpHeight")

    val qry = ExtractOraScenarioNrRuSql.selectScenarioNrRuForPostgres(scenarioId)
    val rs = stmt.executeQuery(qry)

    var snrList = List.empty[SNR]

    while (rs.next()) {
      snrList ::= SNR(
        rs.getString("scenario_id"),
        rs.getString("enb_id"),
        rs.getString("pci"),
        rs.getString("pci_port"),
        rs.getString("ru_id"),
        rs.getString("maker"),
        rs.getString("sector_ord"),
        rs.getString("repeaterattenuation"),
        rs.getString("repeaterpwrratio"),
        rs.getString("ru_seq"),
        rs.getInt("radius"),
        rs.getString("feeder_loss"),
        rs.getString("noisefloor"),
        rs.getString("correction_value"),
        rs.getString("fade_margin"),
        rs.getString("xposition"),
        rs.getString("yposition"),
        rs.getInt("height"),
        rs.getString("site_addr"),
        rs.getString("type"),
        rs.getString("status"),
        rs.getString("sisul_cd"),
        rs.getString("msc"),
        rs.getString("bsc"),
        rs.getString("networkid"),
        rs.getString("usabletrafficch"),
        rs.getString("systemid"),
        rs.getString("ru_type"),
        rs.getString("fa_model_id"),
        rs.getString("network_type"),
        rs.getString("resolution"),
        rs.getString("fa_seq"),
        rs.getDouble("site_startx"),
        rs.getDouble("site_starty"),
        rs.getDouble("site_endx"),
        rs.getDouble("site_endy"),
        rs.getInt("x_bin_cnt"),
        rs.getInt("y_bin_cnt")
      )
    }

    rs.close()

    // 캐쉬를 참고해 복사할 데이터에 캐쉬 사용 여부 필드와 캐쉬 시나리오 아이디 필드 추가해서 데이터 저장
    snrList.
      map { snr =>
        // check cache
        val cacheScenarioId = checkSNRCache(snr.scenario_id, snr.ru_id, snr.xposition, snr.yposition, snr.height, mpHeight, dim)

        ESNR(
          snr.scenario_id,
          snr.enb_id,
          snr.pci,
          snr.pci_port,
          snr.ru_id,
          snr.maker,
          snr.sector_ord,
          snr.repeaterattenuation,
          snr.repeaterpwrratio,
          snr.ru_seq,
          snr.radius,
          snr.feeder_loss,
          snr.noisefloor,
          snr.correction_value,
          snr.fade_margin,
          snr.xposition,
          snr.yposition,
          snr.height,
          snr.site_addr,
          snr.`type`,
          snr.status,
          snr.sisul_cd,
          snr.msc,
          snr.bsc,
          snr.networkid,
          snr.usabletrafficch,
          snr.systemid,
          snr.ru_type,
          snr.fa_model_id,
          snr.network_type,
          snr.resolution,
          snr.fa_seq,
          snr.site_startx,
          snr.site_starty,
          snr.site_endx,
          snr.site_endy,
          snr.x_bin_cnt,
          snr.y_bin_cnt,
          cacheScenarioId.isDefined, // 자신은 캐쉬로 사용하지 않는다.
          cacheScenarioId.getOrElse("") // 자신은 캐쉬로 사용하지 않는다.
        )
      }.
      foreach { esnr =>
        insertESNR(pstmt1, esnr)
        insertESNR(pstmt2, esnr)
        insertESNR(pstmt3, esnr)
        insertESNR(pstmt4, esnr)
      }

    pstmt1.close()
    pstmt2.close()
    pstmt3.close()
    pstmt4.close()

    conn1.close()
    conn2.close()
    conn3.close()
    conn4.close()
  }

  def insertESNR(pstmt: PreparedStatement, esnr: ESNR): Unit = {
    pstmt.clearParameters()
    pstmt.setString(1, esnr.scenario_id)
    pstmt.setString(2, esnr.enb_id)
    pstmt.setString(3, esnr.pci)
    pstmt.setString(4, esnr.pci_port)
    pstmt.setString(5, esnr.ru_id)
    pstmt.setString(6, esnr.maker)
    pstmt.setString(7, esnr.sector_ord)
    pstmt.setString(8, esnr.repeaterattenuation)
    pstmt.setString(9, esnr.repeaterpwrratio)
    pstmt.setString(10, esnr.ru_seq)
    pstmt.setInt(11, esnr.radius)
    pstmt.setString(12, esnr.feeder_loss)
    pstmt.setString(13, esnr.noisefloor)
    pstmt.setString(14, esnr.correction_value)
    pstmt.setString(15, esnr.fade_margin)
    pstmt.setString(16, esnr.xposition)
    pstmt.setString(17, esnr.yposition)
    pstmt.setInt(18, esnr.height)
    pstmt.setString(19, esnr.site_addr)
    pstmt.setString(20, esnr.`type`)
    pstmt.setString(21, esnr.status)
    pstmt.setString(22, esnr.sisul_cd)
    pstmt.setString(23, esnr.msc)
    pstmt.setString(24, esnr.bsc)
    pstmt.setString(25, esnr.networkid)
    pstmt.setString(26, esnr.usabletrafficch)
    pstmt.setString(27, esnr.systemid)
    pstmt.setString(28, esnr.ru_type)
    pstmt.setString(29, esnr.fa_model_id)
    pstmt.setString(30, esnr.network_type)
    pstmt.setString(31, esnr.resolution)
    pstmt.setString(32, esnr.fa_seq)
    pstmt.setDouble(33, esnr.site_startx)
    pstmt.setDouble(34, esnr.site_starty)
    pstmt.setDouble(35, esnr.site_endx)
    pstmt.setDouble(36, esnr.site_endy)
    pstmt.setInt(37, esnr.x_bin_cnt)
    pstmt.setInt(38, esnr.y_bin_cnt)
    pstmt.setBoolean(39, esnr.is_cached)
    pstmt.setString(40, esnr.cache_scenario_id)
    pstmt.executeUpdate()
  }

  def dimension(scenarioId: String)(implicit stmt: Statement): Int = {
    val qry = s"SELECT COUNT(*) FROM SCHEDULE WHERE SCENARIO_ID=${scenarioId} AND TYPE_CD = 'SC051'"
    val rs = stmt.executeQuery(qry)

    try {
      if (rs.next()) {
        rs.getInt(1) + 2
      } else {
        throw new Exception(s"schedules not found: scenario_id=$scenarioId")
      }
    } catch {
      case e: Exception =>
        throw e
    } finally {
      rs.close()
    }
  }

  def mobileParameter(scenarioId: String)(implicit stmt: Statement): Float = {
    val qry = ExtractOraMobileParameterSql.selectMobileParameter(scenarioId)
    val rs = stmt.executeQuery(qry)

    try {
      if (rs.next()) {
        rs.getFloat("height")
      } else {
        throw new Exception(s"mobile parameter not found: scenario_id=$scenarioId")
      }
    } catch {
      case e: Exception =>
        throw e
    } finally {
      rs.close()
    }
  }

  def checkSNRCache(scenarioId: String, ru_id: String, xposition: String, yposition: String, height: Int, mpHeight: Float, dim: Int)(implicit stmt: Statement): Option[String] = {
    val qry = s"""SELECT SCENARIO_ID
    | FROM SCENARIO_CACHE
    | WHERE RU_ID='${ru_id}' AND XPOSITION='${xposition}' AND YPOSITION='${yposition}' AND HEIGHT=${height} AND MP_HEIGHT=${mpHeight} AND DIM=${dim} AND SCENARIO_ID!=${scenarioId}
    | """.stripMargin
    println(qry)
    val rs = stmt.executeQuery(qry)

    try {
      if (rs.next()) {
        println(rs.getString("SCENARIO_ID"))
        Some(rs.getString("SCENARIO_ID"))
      } else {
        None
      }
    } catch {
      case e: Exception =>
        throw e
    }
    finally {
      rs.close()
    }
  }

  case class SNR(
                  scenario_id: String,
                  enb_id: String,
                  pci: String,
                  pci_port: String,
                  ru_id: String,
                  maker: String,
                  sector_ord: String,
                  repeaterattenuation: String,
                  repeaterpwrratio: String,
                  ru_seq: String,
                  radius: Int,
                  feeder_loss: String,
                  noisefloor: String,
                  correction_value: String,
                  fade_margin: String,
                  xposition: String,
                  yposition: String,
                  height: Int,
                  site_addr: String,
                  `type`: String,
                  status: String,
                  sisul_cd: String,
                  msc: String,
                  bsc: String,
                  networkid: String,
                  usabletrafficch: String,
                  systemid: String,
                  ru_type: String,
                  fa_model_id: String,
                  network_type: String,
                  resolution: String,
                  fa_seq: String,
                  site_startx: Double,
                  site_starty: Double,
                  site_endx: Double,
                  site_endy: Double,
                  x_bin_cnt: Int,
                  y_bin_cnt: Int
                )


  case class ESNR(
                   scenario_id: String,
                   enb_id: String,
                   pci: String,
                   pci_port: String,
                   ru_id: String,
                   maker: String,
                   sector_ord: String,
                   repeaterattenuation: String,
                   repeaterpwrratio: String,
                   ru_seq: String,
                   radius: Int,
                   feeder_loss: String,
                   noisefloor: String,
                   correction_value: String,
                   fade_margin: String,
                   xposition: String,
                   yposition: String,
                   height: Int,
                   site_addr: String,
                   `type`: String,
                   status: String,
                   sisul_cd: String,
                   msc: String,
                   bsc: String,
                   networkid: String,
                   usabletrafficch: String,
                   systemid: String,
                   ru_type: String,
                   fa_model_id: String,
                   network_type: String,
                   resolution: String,
                   fa_seq: String,
                   site_startx: Double,
                   site_starty: Double,
                   site_endx: Double,
                   site_endy: Double,
                   x_bin_cnt: Int,
                   y_bin_cnt: Int,
                   is_cached: Boolean,
                   cache_scenario_id: String
                 )
}

object TestCachedSNR extends CachedSNR {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      println("usage: TestCachedSNR <scenario_id> <reset>")
      System.exit(1)
    }

    val scenarioId = args(0)

    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val targetJdbcConnects = Array(
      JdbcConnect("org.postgresql.Driver", "jdbc:postgresql://185.15.16.156:5432/postgres", "postgres", "postgres"),
      JdbcConnect("org.postgresql.Driver", "jdbc:postgresql://185.15.16.157:5432/postgres", "postgres", "postgres"),
      JdbcConnect("org.postgresql.Driver", "jdbc:postgresql://185.15.16.158:5432/postgres", "postgres", "postgres"),
      JdbcConnect("org.postgresql.Driver", "jdbc:postgresql://185.15.16.159:5432/postgres", "postgres", "postgres")
    )

    copySNR(targetJdbcConnects, scenarioId)
  }
}