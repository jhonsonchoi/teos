package com.sccomz.scenario

import com.sccomz.etl.extract.oracle.ExtractOraManager
import com.sccomz.etl.load.LoadPostManager
import com.sccomz.rdb.OracleCon
import com.sccomz.wf.MyEnv
import org.slf4j.LoggerFactory

object ETLOracleToPostgresql {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ETLOracleToPostgresql <scenario_id>")
      System.exit(1)
    }

    val scenarioId = args(0)

    logger.info(s"scenario_id=$scenarioId")

    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    try {
      ExtractOraManager.extractOracleToPostgreInsByScenario(scenarioId)
      LoadPostManager.oracleToPostgreAllBySchedule(scenarioId)

      val schedules = MyEnv.selectSchedules(scenarioId)

      schedules.foreach { s =>
        ExtractOraManager.extractOracleToPostgreInsBySchedule(s.id)
        LoadPostManager.oracleToPostgreAllByScenario(s.id)
      }

    } finally {
      try { stmt.close() }
      try { conn.close() }
    }
  }

}
