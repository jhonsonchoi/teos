package com.sccomz.scenario

import com.sccomz.etl.load.LoadHdfsManager
import com.sccomz.rdb.OracleCon
import com.sccomz.wf.MyEnv
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object SparkPostgresqlToHDFS {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: SparkPostgresqlToHDFS <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logger.info(s"scenario_id=$scheduleId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    try {

      implicit val spark = SparkSession.builder()
        .master("local[*]")
        .appName(s"SparkPostgresqlToHDFS $scheduleId")
        .config("spark.sql.warehouse.dir","/teos/warehouse")
        .enableHiveSupport()
        .getOrCreate()

      val schedule = MyEnv.selectSchedule(scheduleId)
      execute(schedule.scenarioId)

      spark.close()
    } finally {
      try { stmt.close() }
      try { conn.close() }
    }
  }

  def execute(scenarioId: String)(implicit spark: SparkSession): Unit = {
    LoadHdfsManager.toParquetPartitionByScenario("local","SCENARIO_NR_RU_AVG_HEIGHT", scenarioId)
  }

}
