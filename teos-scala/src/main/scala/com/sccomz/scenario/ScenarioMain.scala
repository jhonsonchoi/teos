package com.sccomz.scenario

import java.sql.{PreparedStatement, Statement}

import com.sccomz.analysis.AbstractJob
import com.sccomz.etl.extract.oracle.sql._
import com.sccomz.rdb.{JdbcConnect, JdbcConnectUtil, OracleCon, PostgresCon}
import com.sccomz.wf.{GISJob, HiveSchema, MyEnv, MySchedule}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}
import org.slf4j.LoggerFactory

import scala.sys.process._

object ScenarioMain extends AbstractJob with HiveSchema with CachedSNR with Logging {
//  val cf = ConfigFactory.load()


  val ORACLE_CONNECT = JdbcConnectUtil.getJdbcConnect("oracle")
  val POSTGRES_CONNECT = JdbcConnectUtil.getJdbcConnect("postgres")

  val GIS01_CONNECT = JdbcConnectUtil.getJdbcConnect("gis01")
  val GIS02_CONNECT = JdbcConnectUtil.getJdbcConnect("gis02")
  val GIS03_CONNECT = JdbcConnectUtil.getJdbcConnect("gis03")
  val GIS04_CONNECT = JdbcConnectUtil.getJdbcConnect("gis04")

  val gisCheckInterval = 180000
  val gisScriptKill = s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 pkill -9 -ef 'anal_los_job_dis.sh'"
  val gisScriptRun = s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 /gis01/bin/anal_los_job_dis.sh"

  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      println("usage: ScenarioMain <scenario_id> <reset>")
      System.exit(1)
    }

    val scenarioId = args(0)

    val reset = if (args.length == 2) Some(args(1).toBoolean) else None

    logInfo(s"scenario_id=$scenarioId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()


    implicit val spark = SparkSession.builder()
      .appName(s"ScenarioMain $scenarioId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    setHiveExecEnv()

    try {
      val schedules = MyEnv.selectSchedules(scenarioId)

      reset match {
        case Some(true) =>
          // prepare master data for GIS and proceed after GIS processes

          prepareData(scenarioId, schedules)

          logInfo(s"Phase($scenarioId): Request analysis from GIS")

          // PostgresGIS 분석 요청
          runGISScript(scenarioId)

          processData(scenarioId, schedules, true)

        case Some(false) =>
          // just prepare master data and don't request GIS to process and don't proceed

          prepareData(scenarioId, schedules)
//          processData(scenarioId, schedules, true)

        case _ =>
          // proceed without waiting for GIS to process

          processData(scenarioId, schedules, false)
      }

    } finally {
      try {
        stmt.close()
      }
      try {
        conn.close()
      }
      spark.close()
    }
  }

  /**
   * Oracle => Hive
   * Oracle => GIS
   *
   * @param scenarioId
   * @param schedules
   * @param stmt
   * @param spark
   */
  def prepareData(scenarioId: String, schedules: List[MySchedule])(implicit stmt: Statement, spark: SparkSession): Unit = {
    // Setting counts

//    logInfo(s"Phase($scenarioId): Prepare")
//    ScenarioSetter.execute(scenarioId)

    // Oracle => Hive

    logInfo(s"Phase($scenarioId): Oracle => Hive")

    var qry: String = null

    // 시나리오 단위 데이터

    /*
          val query = s"(SELECT * FROM SCENARIO WHERE  SCENARIO_ID = ${scenarioId})"
          val jdbcDF = spark.read
            .format("jdbc")
            .option("driver", "oracle.jdbc.driver.OracleDriver")
            .option("url", "jdbc:oracle:thin:@150.23.13.165:11521/IAMLTE")
            .option("dbtable", query)
            .option("user", "cellplan")
            .option("password", "cell_2012")
            .load()

          jdbcDF.write
            .format("jdbc")
            .option("driver", "org.postgresql.Driver")
            .option("url", "jdbc:postgresql://185.15.16.156:5432/postgres")
            .option("dbtable", "SCENARIO_MY")
            .option("user", "postgres")
            .option("password", "postgres")
            .mode(SaveMode.Overwrite)
            .save()
    */

    // 캐쉬 기능을 위해서
    // select * from ru WHERE  SCENARIO_ID = 5116767
    // RU 가 다른 경우에만 요청
    // SCENARIO_NR_RU.CACHED 칼럼을 1로 설정


    //    copyScenarioDataForHive(scenarioId)
    // SCENARIO
    qry = ExtractOraScenarioSql.selectScenario(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, SCENARIO, SaveMode.Overwrite)

    // SCENARIO_NR_RU
    qry = ExtractOraScenarioNrRuSql.selectScenarioNrRu(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, SCENARIO_NR_RU, SaveMode.Overwrite)

    // SCENARIO_NR_ANTENNA
    qry = ExtractOraScenarioNrAntennaSql.selectScenarioNrAntenna(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, SCENARIO_NR_ANTENNA, SaveMode.Overwrite)

    // MOBILE_PARAMETER
    qry = ExtractOraMobileParameterSql.selectMobileParameter(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, MOBILE_PARAMETER, SaveMode.Overwrite)

    // NRUETRAFFIC
    qry = ExtractOraNruetrafficSql.selectNruetraffic(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, NRUETRAFFIC, SaveMode.Overwrite)

    // NRSECTORPARAMETER
    qry = ExtractOraNrsectorparameterSql.selectNrsectorparameter(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, NRSECTORPARAMETER, SaveMode.Overwrite)

    // NRSYSTEM
    qry = ExtractOraNrsystemSql.selectNrsystem(scenarioId)
//    println(qry)
    copyObjectToHive(ORACLE_CONNECT, qry, NRSYSTEM, SaveMode.Overwrite)
/*
    ExtractOraManager.extractOracleToHadoopCsvByScenario(scenarioId)

    toParquetPartitionByScenario("local", "SCENARIO", scenarioId)
    //toParquetPartition(spark,"local","DU",scheduleId);
    //toParquetPartition(spark,"local","RU",scheduleId);
    //toParquetPartition(spark,"local","SITE",scheduleId);
    toParquetPartitionByScenario("local", "SCENARIO_NR_RU", scenarioId)
    toParquetPartitionByScenario("local", "SCENARIO_NR_ANTENNA", scenarioId)
    toParquetPartitionByScenario("local", "MOBILE_PARAMETER", scenarioId)
    toParquetPartitionByScenario("local", "NRUETRAFFIC", scenarioId)
    toParquetPartitionByScenario("local", "NRSECTORPARAMETER", scenarioId)
    toParquetPartitionByScenario("local", "NRSYSTEM", scenarioId)
*/

    // 스케쥴 단위 데이터

    schedules.foreach { s =>
//      copyScheduleDataForHive(s.id)
      // SCHEDULE
      qry = ExtractOraScheduleSql.selectSchedule(s.id)
//      println(qry)
      copyObjectToHive(ORACLE_CONNECT, qry, SCHEDULE, SaveMode.Overwrite)
/*
      ExtractOraManager.extractOracleToHadoopCsvBySchedule(s.id)
      toParquetPartitionBySchedule("local", "SCHEDULE", s.id)
*/
    }


    // Oracle => Postgres

    logInfo(s"Phase($scenarioId): Oracle => Postgres")

    // 시나리오 단위 데이터를 복수의 PostGIS 에 복사

    val gisConnects = Array(GIS01_CONNECT, GIS02_CONNECT, GIS03_CONNECT, GIS04_CONNECT)

//    copyScenarioDataForPostgres(scenarioId)
    // SCENARIO
    qry = ExtractOraScenarioSql.selectScenarioForPostgres(scenarioId)
//    println(qry)
    copyObject(ORACLE_CONNECT, qry, gisConnects, "SCENARIO", scenarioId)

    // MOBILE_PARAMETER
    qry = ExtractOraMobileParameterSql.selectMobileParameterForPostgres(scenarioId)
//    println(qry)
    copyObject(ORACLE_CONNECT, qry, gisConnects, "MOBILE_PARAMETER", scenarioId)

    // SCENARIO_NR_RU
//    qry = ExtractOraScenarioNrRuSql.selectScenarioNrRuForPostgres(scenarioId)
//    println(qry)
    copySNR(Array(GIS01_CONNECT, GIS02_CONNECT, GIS03_CONNECT, GIS04_CONNECT), scenarioId)
/*
    ExtractOraManager.extractOracleToPostgreInsByScenario(scenarioId)
    LoadPostManager.oracleToPostgreAllByScenario(scenarioId)
*/

    // 스케쥴 단위 데이터

    schedules.foreach { s =>
//      copyScheduleDataForPostgres(s.id)
      // SCHEDULE
      qry = ExtractOraScheduleSql.selectScheduleForPostgres(s.id)
//      println(qry)
      copyScheduleObject(ORACLE_CONNECT, qry, gisConnects, "SCHEDULE", s.id)
/*
      ExtractOraManager.extractOracleToPostgreInsBySchedule(s.id)
      LoadPostManager.oracleToPostgreAllBySchedule(s.id)
*/
    }
  }

  /**
    * JDBC => Hive
    *
    * @param jdbcConnect
    * @param query
    * @param targetObjectName
    * @param spark
    */
  def copyObjectToHive(jdbcConnect: JdbcConnect, query: String, targetObjectName: String, saveMode: SaveMode)(implicit spark: SparkSession): Unit = {
    println(s"JDBC => Hive")
    println(query)
    println(s"source=${jdbcConnect.url} => target=${targetObjectName}")

    val df = spark.read
      .format("jdbc")
      .option("driver", jdbcConnect.driver)
      .option("url", jdbcConnect.url)
      .option("user", jdbcConnect.user)
      .option("password", jdbcConnect.password)
      .option("dbtable", s"($query) a")
      .load()

    df.write
      .format("hive")
      .mode(saveMode)
      .insertInto(targetObjectName)
  }

  def deleteObject(jdbcConnect: JdbcConnect, targetObjectName: String, scenarioId: String) {
    val conn = PostgresCon.getCon(jdbcConnect)
    val stmt = conn.createStatement()

    val qry = targetObjectName match {
      case "SCENARIO" =>
        s"DELETE FROM $targetObjectName WHERE scenario_id = $scenarioId"
      case "SCENARIO_NR_RU" =>
        s"DELETE FROM $targetObjectName WHERE scenario_id = '$scenarioId'"
      case "MOBILE_PARAMETER" =>
        s"DELETE FROM $targetObjectName WHERE scenario_id = $scenarioId"
    }

    println(qry)
    stmt.executeUpdate(qry)
    stmt.close()
    conn.close()
  }

  /**
    * Oracle => Postgres
    *
    * @param jdbcConnect
    * @param query
    * @param targetJdbcConnects
    * @param targetObjectName
    * @param spark
    */
  def copyObject(jdbcConnect: JdbcConnect, query: String, targetJdbcConnects: Array[JdbcConnect], targetObjectName: String, scenarioId: String)(implicit spark: SparkSession): Unit = {
    println(s"JDBC => JDBC")
    println(query)
    println(targetObjectName)


    targetJdbcConnects.foreach { connect =>
      deleteObject(connect, targetObjectName, scenarioId)

      val df = spark.read
        .format("jdbc")
        .option("driver", jdbcConnect.driver)
        .option("url", jdbcConnect.url)
        .option("user", jdbcConnect.user)
        .option("password", jdbcConnect.password)
        .option("dbtable", s"($query) a")
        .load()

      df.write
        .format("jdbc")
        .option("driver", connect.driver)
        .option("url", connect.url)
        .option("user", connect.user)
        .option("password", connect.password)
        .option("dbtable", targetObjectName)
        .mode(SaveMode.Append)
        .save()
    }

/*
      .mode(SaveMode.Overwrite)
      .insertInto(targetObjectName)
*/
  }

  def deleteScheduleObject(jdbcConnect: JdbcConnect, targetObjectName: String, scheduleId: String) {
    val conn = PostgresCon.getCon(jdbcConnect)
    val stmt = conn.createStatement()

    val qry = targetObjectName match {
      case "SCHEDULE" =>
        s"DELETE FROM $targetObjectName WHERE schedule_id = $scheduleId"
    }

    println(qry)
    stmt.executeUpdate(qry)
    stmt.close()
    conn.close()
  }

  /**
    * Oracle => Postgres
    *
    * @param jdbcConnect
    * @param query
    * @param targetJdbcConnects
    * @param targetObjectName
    * @param spark
    */
  def copyScheduleObject(jdbcConnect: JdbcConnect, query: String, targetJdbcConnects: Array[JdbcConnect], targetObjectName: String, scheduleId: String)(implicit spark: SparkSession): Unit = {
    println(s"JDBC => JDBC")
    println(query)
    println(targetObjectName)


    targetJdbcConnects.foreach { connect =>
      deleteScheduleObject(connect, targetObjectName, scheduleId)

      val df = spark.read
        .format("jdbc")
        .option("driver", jdbcConnect.driver)
        .option("url", jdbcConnect.url)
        .option("user", jdbcConnect.user)
        .option("password", jdbcConnect.password)
        .option("dbtable", s"($query) a")
        .load()

      df.write
        .format("jdbc")
        .option("driver", connect.driver)
        .option("url", connect.url)
        .option("user", connect.user)
        .option("password", connect.password)
        .option("dbtable", targetObjectName)
        .mode(SaveMode.Append)
        .save()
    }

    /*
          .mode(SaveMode.Overwrite)
          .insertInto(targetObjectName)
    */
  }

  /**
   * GIS 에서 계산돼 Hive 에 저장된 SCENARIO_NR_RU 를 앞으로 기계산 데이터로 사용하도록 등록해 놓음
   *
   * insert into SCENARIO_CACHE values ('1012427968', '209297.82089615',	'449883.279046674',	17, 1.5, 2, '0000000');
   *
   * select * from SCENARIO_CACHE;
   *
   * @param source
   * @param scenarioId
   * @param scenarioType
   */
  def cacheSNR(source: JdbcConnect, target: JdbcConnect, scenarioId: String, scenarioType: String): Unit = {

    // insert into SCENARIO_CACHE values ('1012427968', '209297.82089615',	'449883.279046674',	17, 1.5, 2, '0000000');
    val insQry = s"INSERT INTO SCENARIO_CACHE(RU_ID, XPOSITION, YPOSITION, HEIGHT, MP_HEIGHT, DIM, SCENARIO_ID) VALUES (?, ?,	?, ?, ?, ?, ?)"

    val tconn = OracleCon.getCon(target)
    val pstmt = tconn.prepareStatement(insQry)

    val dim = scenarioType match {
      case MyEnv.SCENARIO_TYPE_SIMPLE => 2
      case MyEnv.SCENARIO_TYPE_COMPLEX => 3
    }

    // ru_id, xposition, yposition, height, mp_height, dim, scenario_id
    val qry = s"""SELECT SNR.RU_ID, SNR.XPOSITION, SNR.YPOSITION, SNR.HEIGHT, MP.HEIGHT AS MP_HEIGHT, SNR.SCENARIO_ID
                 | FROM SCENARIO_NR_RU AS SNR, MOBILE_PARAMETER AS MP
                 | WHERE CAST(SNR.SCENARIO_ID AS INT) = MP.SCENARIO_ID AND SNR.SCENARIO_ID = '${scenarioId}'
                 | """.stripMargin

    val sconn = PostgresCon.getCon(source)
    val sstmt = sconn.createStatement()

    val rs = sstmt.executeQuery(qry)

    while (rs.next()) {
      pstmt.clearParameters()
      pstmt.setString(1, rs.getString("RU_ID"))
      pstmt.setString(2, rs.getString("XPOSITION"))
      pstmt.setString(3, rs.getString("YPOSITION"))
      pstmt.setInt(4, rs.getInt("HEIGHT"))
      pstmt.setFloat(5, rs.getFloat("MP_HEIGHT"))
      pstmt.setInt(6, dim)
      pstmt.setString(7, rs.getString("SCENARIO_ID"))
      pstmt.executeUpdate()
    }

    rs.close()
    sstmt.close()
    sconn.close()

    pstmt.close()
    tconn.close()

  }

  /**
   * GIS => Hive
   *
   * @param scenarioId
   * @param schedules
   * @param wait
   * @param stmt
   * @param spark
   */
  def processData(scenarioId: String, schedules: List[MySchedule], wait: Boolean)(implicit stmt: Statement, spark: SparkSession): Unit = {
    val scenarioType = MyEnv.selectScenarioType(scenarioId)

    val completeState = scenarioType match {
      case MyEnv.SCENARIO_TYPE_SIMPLE => 3
      case MyEnv.SCENARIO_TYPE_COMPLEX => 4
    }

    // 결과를 기다리다 가져옴
    val gisJobs =
      if (wait)
        waitAndGetGIS(scenarioId, completeState)
      else
        getGIS(scenarioId, completeState)

//    gisJobs.foreach(println)

    logInfo(s"Phase($scenarioId): Moving data from GIS to Hive")

    // GIS 에서 계산돼 Hive 에 저장된 SCENARIO_NR_RU 를 앞으로 기계산 데이터로 사용하도록 등록해 놓음
    cacheSNR(POSTGRES_CONNECT, ORACLE_CONNECT, scenarioId, scenarioType)

    /*
        ExtractLoadPostManager.executeExtractLoadOneTime(scenarioId)
        LoadHdfsManager.toParquetPartitionByScenario("local", "SCENARIO_NR_RU_AVG_HEIGHT", scenarioId)
    */

//    ALTER TABLE SCENARIO_NR_RU_AVG_HEIGHT_JHONSON DROP IF EXISTS PARTITION (SCENARIO_ID=5116966);

    val query =
      s"""
          |SELECT
          |	     ENB_ID,
          |	     PCI,
          |	     PCI_PORT,
          |	     RU_ID,
          |	     ROUND(CAST(DEM_HEGHT AS NUMERIC),2) as txtotalheight,
          |	     ROUND(CAST(BLD_AVG_HEIGHT AS NUMERIC),2) as avgbuildingheight,
          |	     SCENARIO_ID
          |FROM   SCENARIO_NR_RU_DEM
          |WHERE  SCENARIO_ID = '${scenarioId}'
          |""".stripMargin

    copyObjectToHive(POSTGRES_CONNECT, query, SCENARIO_NR_RU_AVG_HEIGHT, SaveMode.Overwrite)


    schedules.foreach { schedule =>

/*
      schedule.scheduleType match {
        case "SC051" =>
          dropPartition(RESULT_NR_BF_LOS_RU, schedule.id)
        case _ =>
          dropPartition(RESULT_NR_2D_LOS_RU, schedule.id)
      }
*/
      // todo 스케쥴 단위로 데이터를 옮기려고 했는데, 클러스터에서 에러가 발생. 어쩔 수 없이 RU 단위로 데이터 복사.
      // GIS 에서 RU 단위로 파티션 돼 있어서, full scan 의 염려. RU 단위로 완성된 것부터 하나씩 복사. 등의 이유로 RU 단위로 복사.

//      println(s"gis ru data count: ${gisJobs.size}")

//      gisJobs.foreach { job =>

//        println(job)

        // todo configuration
//        val gisConnect =
//          job.place match {
//            case "gis01" => GIS01_CONNECT
//            case "gis02" => GIS02_CONNECT
//            case "gis03" => GIS03_CONNECT
//            case "gis04" => GIS04_CONNECT
//          }

        if (schedule.scheduleType == "SC051") { // 스케쥴이 3D 인 경우
/*
          ExtractLoadPostManager.extractPostToHadoopCsvBF(schedule.id, job.ruId, job.place)
          LoadHdfsLosManager.samToParquetPartition("RESULT_NR_BF_LOS_RU", schedule.id, job.ruId)
*/

          // ALTER TABLE RESULT_NR_2D_LOS_RU_JHONSON DROP IF EXISTS PARTITION (SCHEDULE_ID=8463314, RU_ID=);
//          ALTER TABLE RESULT_NR_2D_LOS_RU_JHONSON DROP IF EXISTS PARTITION (SCHEDULE_ID=8463314);

          val query =
            s"""
                |SELECT
                |       TBD as tbd_key,
                |       FLOOR_X as rx_tm_xpos,
                |       FLOOR_Y as rx_tm_ypos,
                |       FLOOR_Z as rx_floorz,
                |       FLOOR_DEM as rx_gbh,
                |       THETA_DEG as theta,
                |       PHI_DEG as phi,
                |       CASE WHEN LOS THEN 1 ELSE 0 END as value,
                |       IN_OUT as bin_type,
                |       SCHEDULE_ID,
                |       RU_ID
                |FROM   LOS_BLD_RESULT
                |WHERE  SCHEDULE_ID = ${schedule.id} -- AND RU_ID = '{job.ruId}'
                |""".stripMargin

//          copyObject(GIS01_CONNECT, query, RESULT_NR_BF_LOS_RU, SaveMode.Overwrite)
//          copyObject(GIS02_CONNECT, query, RESULT_NR_BF_LOS_RU, SaveMode.Overwrite)
//          copyObject(GIS03_CONNECT, query, RESULT_NR_BF_LOS_RU, SaveMode.Overwrite)
//          copyObject(GIS04_CONNECT, query, RESULT_NR_BF_LOS_RU, SaveMode.Overwrite)

          // tree_bld_result
//          copyObject(GIS01_CONNECT, query, RESULT_NR_BF_TREE_RU, SaveMode.Overwrite)

          // sc_bld_list
          val bldQuery =
            s"""
               |SELECT
               |       TBD as tbd_key,
               |       SCHEDULE_ID
               |FROM SC_BLD_LIST
               |WHERE  SCHEDULE_ID = ${schedule.id}
             """.stripMargin

          copyObjectToHive(GIS01_CONNECT, bldQuery, RESULT_NR_BF_TBDKEY, SaveMode.Overwrite)

          // ru_bld_list
          val ruBldQuery =
            s"""
               |SELECT
               |       RU_ID,
               |       TBD as tbd_key,
               |       SCHEDULE_ID
               |FROM RU_BLD_LIST
               |WHERE  SCHEDULE_ID = ${schedule.id}
             """.stripMargin

          copyObjectToHive(GIS01_CONNECT, ruBldQuery, RESULT_NR_BF_RU_TBDKEY, SaveMode.Overwrite)
          copyObjectToHive(GIS02_CONNECT, ruBldQuery, RESULT_NR_BF_RU_TBDKEY, SaveMode.Append)
          copyObjectToHive(GIS03_CONNECT, ruBldQuery, RESULT_NR_BF_RU_TBDKEY, SaveMode.Append)
          copyObjectToHive(GIS04_CONNECT, ruBldQuery, RESULT_NR_BF_RU_TBDKEY, SaveMode.Append)

          // 캐쉬에서 복사
          val cachedRUs = findCached3DRUs(POSTGRES_CONNECT, scenarioId)
          cachedRUs.foreach { cachedRU =>
            copyHive3D(cachedRU.scheduleId, cachedRU.ruId, schedule.id)
          }
        } else { // 스케쥴이 2D 인 경우
/*
          ExtractLoadPostManager.extractPostToHadoopCsv2D(schedule.id, job.ruId, job.place)
          LoadHdfsLosManager.samToParquetPartition("RESULT_NR_2D_LOS_RU", schedule.id, job.ruId)
*/
          // ALTER TABLE RESULT_NR_2D_LOS_RU_JHONSON DROP IF EXISTS PARTITION (SCHEDULE_ID=8463314, RU_ID=);
          //          ALTER TABLE RESULT_NR_2D_LOS_RU_JHONSON DROP IF EXISTS PARTITION (SCHEDULE_ID=8463314);


          val query =
            s"""
               |SELECT
               |       BIN_X as rx_tm_xpos,
               |       BIN_Y as rx_tm_ypos,
               |       BIN_Z as rz,
               |       CASE WHEN LOS THEN 1 ELSE 0 END as value,
               |       THETA_DEG as theta,
               |       PHI_DEG as phi,
               |       CASE WHEN IN_BLD THEN 'T' ELSE 'F' END as is_bld,
               |       SCHEDULE_ID,
               |       RU_ID
               |FROM   LOS_ENG_RESULT
               |WHERE  SCHEDULE_ID = ${schedule.id} -- AND RU_ID = '{job.ruId}'
               |""".stripMargin

//          copyObject(GIS01_CONNECT, query, RESULT_NR_2D_LOS_RU, SaveMode.Overwrite)
//          copyObject(GIS02_CONNECT, query, RESULT_NR_2D_LOS_RU, SaveMode.Overwrite)
//          copyObject(GIS03_CONNECT, query, RESULT_NR_2D_LOS_RU, SaveMode.Overwrite)
//          copyObject(GIS04_CONNECT, query, RESULT_NR_2D_LOS_RU, SaveMode.Overwrite)

          // tree_bld_result
//          copyObject(GIS01_CONNECT, query, RESULT_NR_BF_TREE_RU, SaveMode.Overwrite)

          // 캐쉬에서 복사
          val cachedRUs = findCached2DRUs(POSTGRES_CONNECT, scenarioId)
          cachedRUs.foreach { cachedRU =>
            copyHive2D(cachedRU.scheduleId, cachedRU.ruId, schedule.id)
          }
        }
//      }

    }

  }

  case class CachedRU(scheduleId: String, ruId: String)

  /**
   * postgres
   *
   * @param scenarioId
   * @return
   */
  def findCached2DRUs(source: JdbcConnect, scenarioId: String): List[CachedRU] = {
    val conn = PostgresCon.getCon(source)
    val stmt = conn.createStatement()

    val qry = s"""SELECT S.SCHEDULE_ID, RU_ID
                 | FROM SCENARIO_NR_RU SNR, SCHEDULE S
                 | WHERE S.SCENARIO_ID = cast(SNR.CACHE_SCENARIO_ID as int) and S.TYPE_CD='SC001' and SNR.SCENARIO_ID = '${scenarioId}' AND IS_CACHED
                 | """.stripMargin
    val rs = stmt.executeQuery(qry)
    var cruList = List.empty[CachedRU]
    while (rs.next()) {
      cruList ::= CachedRU(rs.getString("SCHEDULE_ID"), rs.getString("RU_ID"))
    }

    rs.close()
    stmt.close()
    conn.close()

    cruList
  }

  /**
   * postgres
   *
   * @param scenarioId
   * @return
   */
  def findCached3DRUs(source: JdbcConnect, scenarioId: String): List[CachedRU] = {
    val conn = PostgresCon.getCon(source)
    val stmt = conn.createStatement()

    val qry = s"""SELECT S.SCHEDULE_ID, RU_ID
                  | FROM SCENARIO_NR_RU SNR, SCHEDULE S
                  | WHERE S.SCENARIO_ID = cast(SNR.CACHE_SCENARIO_ID as int) and S.TYPE_CD='SC051' and SNR.SCENARIO_ID = '${scenarioId}' AND IS_CACHED
                  | """.stripMargin
    val rs = stmt.executeQuery(qry)
    var cruList = List.empty[CachedRU]
    while (rs.next()) {
      cruList ::= CachedRU(rs.getString("SCHEDULE_ID"), rs.getString("RU_ID"))
    }

    rs.close()
    stmt.close()
    conn.close()

    cruList
  }

  /**
   * hive> describe RESULT_NR_2D_LOS_RU;
   * OK
   * rx_tm_xpos          	int
   * rx_tm_ypos          	int
   * rz                  	float
   * value               	int
   * theta               	float
   * phi                 	float
   * is_bld              	string
   * schedule_id         	int
   * ru_id               	string
   *
   * # Partition Information
   * # col_name            	data_type           	comment
   *
   * schedule_id         	int
   * ru_id               	string
   *
   * @param cacheScheduleId
   * @param ruId
   * @param scheduleId
   */
  def copyHive2D(cacheScheduleId: String, ruId: String, scheduleId: String)(implicit spark: SparkSession): Unit = {
    if (cacheScheduleId == scheduleId) {
      return
    }

    val qry = s"""SELECT rx_tm_xpos, rx_tm_ypos, rz, value, theta, phi, is_bld, ${scheduleId} as schedule_id, ru_id
              | FROM ${RESULT_NR_2D_LOS_RU}
              | WHERE schedule_id = ${cacheScheduleId} AND ru_id = ${ruId}
              | """.stripMargin

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_2D_LOS_RU)
  }

  /**
   * hive> describe RESULT_NR_BF_LOS_RU_POSTGIS;
   * OK
   * tbd_key             	string
   * rx_tm_xpos          	float
   * rx_tm_ypos          	float
   * rx_floorz           	int
   * rx_gbh              	float
   * theta               	float
   * phi                 	float
   * value               	int
   * bin_type            	int
   * schedule_id         	int
   * ru_id               	string
   *
   * # Partition Information
   * # col_name            	data_type           	comment
   *
   * schedule_id         	int
   * ru_id               	string
   *
   * @param cacheScheduleId
   * @param ruId
   * @param scheduleId
   */
  def copyHive3D(cacheScheduleId: String, ruId: String, scheduleId: String)(implicit spark: SparkSession): Unit = {
    if (cacheScheduleId == scheduleId) {
      return
    }

    val qry = s"""SELECT tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz, rx_gbh, theta, phi, value, bin_type, ${scheduleId} as schedule_id, ru_id
              | FROM ${RESULT_NR_BF_LOS_RU}
              | WHERE schedule_id=${cacheScheduleId} AND ru_id=${ruId}
              | """.stripMargin

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_LOS_RU)
  }

  def dropPartition(table: String, scheduleId: String)(implicit spark: SparkSession): Unit = {
    val qry = s"ALTER TABLE ${table} DROP IF EXISTS PARTITION (SCHEDULE_ID=${scheduleId})"
    println(qry)
    spark.sql(qry)
  }

  def runGISScript(scenarioId: String): Unit = {

    val killExitCode = s"sshpass -pteos ssh -o StrictHostKeyChecking=no postgres@teos-cluster-dn1 pkill -9 -ef 'anal_los_job_dis.sh ${scenarioId}'" !

    println(s"kill exit code: $killExitCode")

    val runExitCode = s"${gisScriptRun} ${scenarioId}" !

    println(s"run exit code: $runExitCode")
  }

  /**
   * GIS 분석 결과를 가져옴
   *
   * @param scenarioId
   * @param completeState
   */
  def getGIS(scenarioId: String, completeState: Int): List[GISJob] = {
    val conn = PostgresCon.getCon()
    implicit val stmt = conn.createStatement()

    val ruNumber = MyEnv.countRU(scenarioId)

    // GIS 잡이 일정 상태에 이를 때까지 기다림.

    MyEnv.checkGISJobState(scenarioId, completeState)
  }

  /**
   * GIS 분석 결과를 기다리다 가져옴
   * JOB_DIS 테이블의 모든 시나리오 레코드의 stat 이 원하는 상태에 도달할 때까지 기다림
   *
   * @param scenarioId
   * @param completeState
   */
  def waitAndGetGIS(scenarioId: String, completeState: Int): List[GISJob] = {
    val conn = PostgresCon.getCon()
    implicit val stmt = conn.createStatement()

    val ruNumber = MyEnv.countRU(scenarioId)

    // GIS 잡이 일정 상태에 이를 때까지 기다림.
    // JOB_DIS 테이블의 stat 이 원하는 상태에 도달할 때까지 기다림

    var jobs: List[GISJob] = null

    try {
      var ready = false

      while (!ready) {
        logInfo(s"is ready?: scenario_id=$scenarioId")

        jobs = MyEnv.checkGISJobState(scenarioId, completeState)

        ready = ruNumber == jobs.size

        if (!ready) {
          Thread.sleep(gisCheckInterval)
        }
      }
    } finally {
      try {
        stmt.close()
      }
      try {
        conn.close()
      }
    }

    jobs
  }

}

object TestCache {
  val POSTGRES_CONNECT = JdbcConnectUtil.getJdbcConnect("postgres")

  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      println("usage: TestCache <scenario_id>")
      System.exit(1)
    }

    val scenarioId = args(0)
    val warehouseDir = ""

    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    implicit val spark = SparkSession.builder()
      .appName(s"TestCache $scenarioId")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    val schedules = MyEnv.selectSchedules(scenarioId)

    // 캐쉬에서 복사
    val cachedRUs = ScenarioMain.findCached2DRUs(POSTGRES_CONNECT, scenarioId)

    schedules.foreach { schedule =>
      cachedRUs.foreach { cachedRU =>
        ScenarioMain.copyHive2D(cachedRU.scheduleId, cachedRU.ruId, schedule.id)
      }
    }

  }
}