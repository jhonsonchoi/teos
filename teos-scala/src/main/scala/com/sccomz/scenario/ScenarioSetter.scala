package com.sccomz.scenario

import java.sql.Statement

import com.sccomz.rdb.OracleCon
import com.sccomz.wf.MyEnv
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

object ScenarioSetter {

  val logger = LoggerFactory.getLogger(getClass)

  val cf = ConfigFactory.load()

  val oracle = "oracle"
  var resultRoot = cf.getString("bin-path") // "/user/icpap/result"

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ScenarioSetter <scenario_id>")
      System.exit(1)
    }

    val scenarioId = args(0)

    logger.info(s"scenario_id=$scenarioId")

    val con = OracleCon.getCon(oracle)

    implicit val stmt = con.createStatement()

    try {
      execute(scenarioId)
    } catch {
      case e: Throwable => logger.error(e.getLocalizedMessage, e)
    } finally {
      stmt.close()
      con.close()
    }
  }

  def execute(scenarioId: String)(implicit stmt: Statement): Unit = {
    MyEnv.selectSchedules(scenarioId)
      .map(s => MyEnv.selectRichReq(s.id))
      .foreach { rrq =>
        MyEnv.prepareScheduleRun(rrq.scheduleId, s"${resultRoot}/${rrq.scheduleId}", rrq.binXCnt, rrq.binYCnt, rrq.ruCnt)
      }
  }

}
