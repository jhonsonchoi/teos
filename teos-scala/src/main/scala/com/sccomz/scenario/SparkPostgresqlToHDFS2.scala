package com.sccomz.scenario

import com.sccomz.etl.load.LoadHdfsLosManager
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object SparkPostgresqlToHDFS2 {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: SparkPostgresqlToHDFS2 <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logger.info(s"schedule_id=$scheduleId")

//    LoadHdfsManager.postAvgToHdfs(wfId, scenarioId)
    // todo
//    val scenario = ExecuteJob.selectScenario(scenarioId)

//    val bdYn = scenario.schedules.exists(s => s.scheduleType == "SC501")

    implicit val spark = SparkSession.builder()
      .master("local[*]")
      .appName(s"SparkPostgresqlToHDFS2 $scheduleId")
      .config("spark.sql.warehouse.dir","/teos/warehouse")
      .enableHiveSupport()
      .getOrCreate()

    // todo
//    val rus = ExecuteJob.selectRUByScenario(scenarioId)

    // todo
//    LoadHdfsLosManager.samToParquetPartition(spark, "RESULT_NR_2D_LOS_RU", scheduleId, rus)

    spark.close()
  }

  def my(host: String, objNm: String, wfId: String)(implicit spark: SparkSession): Unit = {

    val sql =
s"""SELECT
    TBD                                       ,
      FLOOR_X                                   ,
      FLOOR_Y                                   ,
      FLOOR_Z                                   ,
      FLOOR_DEM                                 ,
      THETA_DEG                                 ,
      PHI_DEG                                   ,
      CASE WHEN LOS IS TRUE THEN 1 ELSE 0 END   ,
      SCHEDULE_ID                           ,
      RU_ID
    FROM   LOS_BLD_RESULT
      WHERE  SCHEDULE_ID = ${wfId}
"""
//    AND    RU_ID  = '${ruId}'

//    val host = "185.15.16.156"
    val port = "5432"
    val service = "postgres"
    val user = "postgres"
    val password = "postgres"

    spark.sql(s"""ALTER TABLE ${objNm} DROP IF EXISTS PARTITION (SCHEDULE_ID=${wfId})""")

    val df = spark.read
      .format("jdbc")
      .option("driver", "org.postgresql.Driver")
      .option("url", s"jdbc:postgresql://${host}:${port}/${service}")
      .option("user", user)
      .option("password", password)
      .option("query", sql)
      .load()

      df.write
          .mode("append")
      .parquet(s"/TEOS/warehouse/${objNm}")
  }


  def executeRealPostToHdfs(scheduleId:String,ruId:String,typeCd:String)(implicit spark: SparkSession) = {
    //samToParquetPartition("RESULT_NR_2D_TREE_RU",scheduleId,ruId);


    if(typeCd=="SC051") {
      LoadHdfsLosManager.samToParquetPartition("RESULT_NR_BF_LOS_RU",scheduleId,ruId);
      //samToParquetPartition("RESULT_NR_BF_TREE_RU",scheduleId,ruId);
    } else {
      LoadHdfsLosManager.samToParquetPartition("RESULT_NR_2D_LOS_RU",scheduleId,ruId);
    }
  }

}
