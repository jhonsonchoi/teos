package com.sccomz.scenario

import java.sql.Statement

import com.sccomz.etl.extract.post.ExtractLoadPostManager.{executeExtractLoadOneTime, extractPostToHadoopCsv}
import com.sccomz.rdb.PostgresCon
import org.slf4j.LoggerFactory

import scala.collection.mutable.ListBuffer

/**
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object ETLPostgresqlToHDFS {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: ETLPostgresqlToHDFS <scenario_id>")
      System.exit(1)
    }

    val scenarioId = args(0)

    logger.info(s"scenario_id=$scenarioId")

    val con = PostgresCon.getCon()
    implicit val stmt = con.createStatement()

    // todo 시나리오의 워크플로타입
    val ruState = 3 // 또는 4

    var ready = false

    while (!ready) {
      logger.info("is ready?")
      val rus = checkGISJobState(scenarioId)

      ready = rus.forall(ru => ru.state == ruState)

      if (!ready) {
        Thread.sleep(3000)
      }
    }

    // 시나리오 당
    executeExtractLoadOneTime(scenarioId)

    // 반복 실행

    val rus = checkGISJobState(scenarioId)

    rus.foreach { ru =>
      val splitId = ru.id.split("-")
      // todo 스케쥴과 ru 단위로 변경
      extractPostToHadoopCsv(scenarioId, ru.ruId, ru.place, "SC001", "N");
    }
//    executeExtractLoad(wfId, typeCd, "N")
//    ExtractLoadPostManager.monitorJobDis(wfId, scenarioId,true);
  }

  def checkGISJobState(scenarioId: String)(implicit stmt: Statement): List[RU] = {
    val rus = ListBuffer.empty[RU]

    val qry = s"""
    SELECT *
      FROM   JOB_DIS A
    WHERE  A.SCENARIO_ID = $scenarioId
"""
    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val state =
      rus += RU(rs.getString("job_id"), rs.getString("ru_id"), rs.getInt("stat"), rs.getString("job_from"))
    }

    rus.toList
  }
}

case class RU(id: String, ruId: String, state: Int, place: String)