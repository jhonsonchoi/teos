package com.sccomz.scenario

import com.sccomz.etl.load.LoadHdfsManager.{toParquetPartitionByScenario, toParquetPartitionBySchedule}
import com.sccomz.rdb.OracleCon
import com.sccomz.wf.MyEnv
import org.apache.spark.sql.SparkSession
import org.slf4j.LoggerFactory

object SparkOracleToHDFS {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: SparkOracleToHDFS <scenario_id>")
      System.exit(1)
    }

    val scenarioId = args(0)

    logger.info(s"scenario_id=$scenarioId")


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()

    val schedules = MyEnv.selectSchedules(scenarioId)

    try {
      implicit val spark = SparkSession.builder()
        .master("local[*]")
        .appName(s"SparkOracleToHDFS $scenarioId")
        .config("spark.sql.warehouse.dir","/teos/warehouse")
        .enableHiveSupport()
        .getOrCreate()

      schedules.foreach { s =>
        toParquetPartitionBySchedule("local","SCHEDULE", s.id)
      }
      toParquetPartitionByScenario("local","SCENARIO", scenarioId)
      //toParquetPartition(spark,"local","DU",scheduleId);
      //toParquetPartition(spark,"local","RU",scheduleId);
      //toParquetPartition(spark,"local","SITE",scheduleId);
      toParquetPartitionByScenario("local","SCENARIO_NR_RU", scenarioId)
      toParquetPartitionByScenario("local","SCENARIO_NR_ANTENNA", scenarioId)
      toParquetPartitionByScenario("local","MOBILE_PARAMETER", scenarioId)
      toParquetPartitionByScenario("local","NRUETRAFFIC", scenarioId)
      toParquetPartitionByScenario("local","NRSECTORPARAMETER", scenarioId)
      toParquetPartitionByScenario("local","NRSYSTEM", scenarioId)

      spark.close();
    } finally {
      try { stmt.close() }
      try { conn.close() }
    }
  }

}
