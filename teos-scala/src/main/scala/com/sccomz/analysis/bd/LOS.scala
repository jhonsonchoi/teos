package com.sccomz.analysis.bd

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 3D LOS
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object LOS extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: LOS <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"LOS $scheduleId").
      //      .config("queue", queueNm)
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeScenario(scheduleId)

    spark.close()
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_BF_LOS, scheduleId)

    val qry =
      s"""
        |
        |-- insert into ${RESULT_NR_BF_LOS} partition (schedule_id)
        |select tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz,
        |     case when sum(case when value = 1 then 1 else 0 end) > 0 then 1 else 0 end as los,
        |     case when sum(bin_type) > 0 then 1 else 0 end as in_out,
        |     max(schedule_id) as schedule_id
        |from ${RESULT_NR_BF_LOS_RU}
        |where schedule_id = ${scheduleId}
        |group by tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_LOS)
  }

}