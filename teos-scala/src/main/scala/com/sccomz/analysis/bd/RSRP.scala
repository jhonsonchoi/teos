package com.sccomz.analysis.bd

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 3D RSRP
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object RSRP extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: RSRP <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"RSRP $scheduleId").
      //      config("queue", queueNm).
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeSite(scheduleId)
    executeScenario(scheduleId)

    spark.close()
  }

  def executeSite(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_BF_RSRP_RU, scheduleId)

    val qry =
      s"""
        |WITH OVERLAB AS
        |(
        |select enb_id, cell_id, tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz, rz,
        |       case when sum(power(10., rsrppilot / 10.)) = 0. then -9999
        |            else 10. * log10 (sum(power(10., rsrppilot / 10.)))
        |        end as rsrppilot
        |  from ${RESULT_NR_BF_RSRPPILOT_RU}
        | where schedule_id = $scheduleId
        | group by enb_id, cell_id, tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz, rz
        | having count(*) > 1
        |)
        |-- insert into ${RESULT_NR_BF_RSRP_RU} partition (schedule_id)
        |select a.ru_id, a.enb_id, a.cell_id, a.tbd_key, a.rx_tm_xpos, a.rx_tm_ypos, a.rx_floorz, a.rz,
        |       a.los, a.bin_type, a.pathloss, a.antenna_gain, a.pathlossprime, a.rsrppilot,
        |       case when b.rsrppilot is not null then b.rsrppilot else a.rsrppilot end rsrp,
        |       a.schedule_id
        |  from (select * from ${RESULT_NR_BF_RSRPPILOT_RU} where schedule_id = $scheduleId) a left outer join OVERLAB b
        |    on (a.enb_id = b.enb_id and a.cell_id = b.cell_id and a.tbd_key = b.tbd_key and a.rx_tm_xpos = b.rx_tm_xpos and a.rx_tm_ypos = b.rx_tm_ypos and a.rx_floorz = b.rx_floorz and a.rz = b.rz)
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_RSRP_RU)
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_BF_RSRP, scheduleId)

    val qry =
      s"""
        |-- insert into ${RESULT_NR_BF_RSRP} partition (schedule_id)
        |select tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz,
        |       case when sum(bin_type) > 0 then 1 else 0 end as in_out,
        |       max(rsrp) as rsrp,
        |       max(schedule_id) as schedule_id
        |  from ${RESULT_NR_BF_RSRP_RU}
        | where schedule_id = $scheduleId
        | group by tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_RSRP)
  }

}