package com.sccomz.analysis.bd

import java.sql.Statement

import com.sccomz.analysis.{BldHeader, MakeBinFile}
import com.sccomz.serialize.{Byte4, MakeBfBinFileSql, MakeBinFileSql, MakeFolder}
import com.sccomz.wf.{HiveSchema, MyEnv, MyRU}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession

import scala.collection.mutable.ListBuffer
import scala.collection.Map

/*

import com.sccomz.serialize.MakeBfBinFile
MakeBfBinFile.executeResult("8463233");
MakeBfBinFile.makeSectorResult("8463233", "LOS","SYS/5113566");

 * */
object MakeBFBinFile extends HiveSchema with MakeBinFile with Logging {

  var resultRoot = "/user/icpap/result/bd"

  def main(args: Array[String]): Unit = {
    var scheduleId = if (args.length < 1) "" else args(0);

    //val spark: SparkSession = SparkSession.builder().master("yarn").appName(this.getClass.getName).getOrCreate();
    implicit val spark: SparkSession = SparkSession.builder().appName(this.getClass.getName).getOrCreate();

//    executeResult(scheduleId);
    //executeResult("8460062");

    spark.stop()
  }

  def executeResult(scheduleId: String)(implicit stmt: Statement, spark: SparkSession) = {

    // separate this function from the job
    val ruInfo = MakeFolder.makeResultPath(scheduleId)
    makeResult(ruInfo, scheduleId, "LOS")
    //makeResult(scheduleId, "PATHLOSS");
    //makeResult(scheduleId, "BEST_SERVER");
    //makeResult(scheduleId, "PILOT_EC");     // RSRP
    //makeResult(scheduleId, "RSSI");
    //makeResult(scheduleId, "C2I");      // SINR
  }

  // Bin 생성
  def makeResult(ruInfo: Map[String, String], scheduleId: String, cdNm: String)(implicit stmt: Statement, spark: SparkSession) = {
//    makeScheduleResult(scheduleId, cdNm, ruInfo.getOrElse("SECTOR_PATH", ""))
    //makeRuResult(scheduleId, cdNm, ruInfo);

    if (cdNm == "LOS" || cdNm == "PATHLOSS") {
//      makeRuResult(scheduleId, cdNm, ruInfo);
    }
  }

  // 섹터 Bin
  def makeScheduleResult(scheduleId: String, targetPath: String, targetFileName: String, tableName: String, colName: String, colType: String, noin: Boolean, noplb: Boolean)(implicit stmt: Statement, spark: SparkSession) = {

    var qry = ""
    // int    buildingIndex; // Building Index
    // char   cTBDKey[20];	 // TBD Key
    // UCha   xyz[4];			   // 0 : X Bin Count
    // 				               // 1 : Y Bin count
    //                       // 2 : Z floor
    //                       // 3 : Padding
    // float  startX, startY;	// Building Border letf bottom
    // ULLong	startPointBin;	// start point of BIN data

	  //int    bldCount;
	  //int    resolution;
	  //char*  bldHeader;	// BuildingHeader
	  //ULLong binCount;	// binData Count(do not save in file)
	  //char*  binData;


    // header

    //---------------------------------------------------
    println(s"""[SEL] Header ${scheduleId}""")
    //---------------------------------------------------
    // get schedule data from RESULT_NR_BF_SCEN_HEADER
    qry = MakeBfBinFileSql.selectResultNrBfScenHeader(scheduleId)
    println(qry)
    val headerDF = spark.sql(qry)
    headerDF.cache.createOrReplaceTempView("M_RESULT_NR_BF_SCEN_HEADER");

    //---------------------------------------------------
    println(s"""[SEL] bldCount ${scheduleId}""")
    //---------------------------------------------------
    // count TBD_KEY from M_RESULT_NR_BF_SCEN_HEADER
    qry = MakeBfBinFileSql.selectBldCount()
    println(qry)
    var sqlDf = spark.sql(qry)
    var row = sqlDf.collect.last
    val bldCount : Int = row(0).asInstanceOf[Int]

    //---------------------------------------------------
    println(s"""[SEL] resolution ${scheduleId}""")
    //---------------------------------------------------
    // get resolution from SCENARIO
    val resolution = MyEnv.getResolution(scheduleId)

    val penetrationLoss = if (noplb) MyEnv.getPenetrationLoss(scheduleId) else 0

    //---------------------------------------------------
    println(s"""[SEL] sumBinCnt ${scheduleId}""")
    //---------------------------------------------------
    // get sum of BIN_CNT from M_RESULT_NR_BF_SCEN_HEADER
    qry = MakeBfBinFileSql.selectSumBinCnt(scheduleId)
    println(qry)
    sqlDf = spark.sql(qry)
    row = sqlDf.collect.last
    val sumBinCnt : Long = row(0).asInstanceOf[Long]
    val sumBinCnt2 : Int = row(1).asInstanceOf[Int]

    val bldHeaders = ListBuffer.empty[BldHeader]
    headerDF.collect().foreach { row =>
      bldHeaders += BldHeader(row(0).asInstanceOf[Int], row(1).asInstanceOf[String], row(2).asInstanceOf[Int], row(3).asInstanceOf[Int], row(4).asInstanceOf[Int], row(5).asInstanceOf[Float], row(6).asInstanceOf[Float], row(7).asInstanceOf[Long])
    }

    // data
    qry = MakeBfBinFileSql.selectSectorResult(scheduleId, tableName, colName, noin, penetrationLoss)
    println(qry)

    val valueDF = spark.sql(qry).collect()

    val bin = initialArray(sumBinCnt2, colType)

    valueDF.foreach { row =>
      val pos = row(7).asInstanceOf[Int]
      val value = colType match {
        case "Int" => row(5)
        case "Float" => row(6)
      }
      setBinPoint(bin, pos, value, colType)
    }

    writeBin(bldCount, resolution, bldHeaders.toList, sumBinCnt, bin, s"${targetPath}/${targetFileName}.bin")

    println(s"""makeSectorResult end ${scheduleId}""");
  }


  def getRUBinCounts(scheduleId: String, ruId: (String, String))(implicit spark: SparkSession): (Int, Int) = {
    logInfo("makeRuResult 04 " + ruId._1)

    val qry = MakeBinFileSql.select2dRuBinCnt(scheduleId, ruId._1)
    logInfo(qry)
    val sqlDf = spark.sql(qry)

    val row = sqlDf.collect.last
    val x_bin_cnt = row(0).asInstanceOf[Int]
    val y_bin_cnt = row(1).asInstanceOf[Int]

    (x_bin_cnt, y_bin_cnt)
  }

  // RU별 Bin
  def makeRuResult(scheduleId: String, targetPath: String, targetFileName: String, tableName: String, colName: String, colType: String, ruInfo: Map[String, MyRU], ruPathInfo: Map[String, String], noin: Boolean)(implicit stmt: Statement, spark: SparkSession) = {

    var qry = ""

    //---------------------------------------------------
    logInfo(s"""[SEL] RU_VALUE ${scheduleId}""");
    //---------------------------------------------------

    // header

    //---------------------------------------------------
    println(s"""[SEL] Header ${scheduleId}""")
    //---------------------------------------------------
    // get ru data from RESULT_NR_BF_RU_HEADER
    qry = MakeBfBinFileSql.selectResultNrBfRuHeader(scheduleId)
    println(qry)
    val headerDF = spark.sql(qry)
    headerDF.cache.createOrReplaceTempView("M_RESULT_NR_BF_RU_HEADER");

    //---------------------------------------------------
    println(s"""[SEL] bldCount ${scheduleId}""")
    //---------------------------------------------------
    // count TBD_KEY from M_RESULT_NR_BF_RU_HEADER
    qry = MakeBfBinFileSql.selectRUBldCount()
    println(qry)
    var sqlDf = spark.sql(qry)

    var ruBldCnt = Map.empty[String, Int]

    sqlDf.collect().foreach { row =>
      ruBldCnt = ruBldCnt + (row(0).asInstanceOf[String] -> row(1).asInstanceOf[Int])
    }

    //---------------------------------------------------
    println(s"""[SEL] resolution ${scheduleId}""")
    //---------------------------------------------------
    // get resolution from SCENARIO
    val resolution = MyEnv.getResolution(scheduleId)

    //---------------------------------------------------
    println(s"""[SEL] sumBinCnt ${scheduleId}""")
    //---------------------------------------------------
    // get sum of BIN_CNT from M_RESULT_NR_BF_SCEN_HEADER
    qry = MakeBfBinFileSql.selectRUSumBinCnt()
    println(qry)
    sqlDf = spark.sql(qry)

    var ruSumBinCnt = Map.empty[String, (Long, Int)]

    sqlDf.collect().map { row =>
      ruSumBinCnt = ruSumBinCnt + (row(0).asInstanceOf[String] -> (row(1).asInstanceOf[Long], row(2).asInstanceOf[Int]))
    }

/*
    val imRUSumBinCnt = ruSumBinCnt.map { m =>
      (m._1, m._2)
    }
*/


    /*
        SELECT A.RU_ID                -- 0
        , A.BUILDING_INDEX       -- 1
        , A.TBD_KEY              -- 2
        , A.NX                   -- 3
        , A.NY                   -- 4
        , A.FLOORZ               -- 5
        , A.EXT_SX               -- 6
        , A.EXT_SY               -- 7
        , A.BIN_CNT              -- 8
        , A.START_POINT_BIN      -- 9
        , A.START_POINT_4BIN     -- 10
        , B.RU_PATH              -- 11
    */
    // todo
    var ruBldHeaders = Map.empty[String, ListBuffer[BldHeader]]
    headerDF.collect().foreach { row =>
      val ruId = row(0).asInstanceOf[String]
        // ruId 에 해당하는 헤더가 없으면 맵에 추가
      if (!ruBldHeaders.contains(ruId)) {
        ruBldHeaders = ruBldHeaders + (ruId -> ListBuffer.empty[BldHeader])
      }
//      ruBldHeaders += ruId -> Array.empty[BldHeader]
      // bldHeaders 배열에 추가
      ruBldHeaders(ruId) += BldHeader(row(1).asInstanceOf[Int], row(2).asInstanceOf[String], row(3).asInstanceOf[Int], row(4).asInstanceOf[Int], row(5).asInstanceOf[Int], row(6).asInstanceOf[Float], row(7).asInstanceOf[Float], row(8).asInstanceOf[Long])
    }
    val imRUBldHeaders = ruBldHeaders.map { m =>
      (m._1, m._2.toList)
    }

    // data

    import spark.implicits._

    qry = MakeBinFileSql.selectRuResultAll(scheduleId, tableName, colName)
    println(qry)
    val valueDF = spark.sql(qry).repartition($"RU_ID")
    //qry = MakeBinFileSql.selectRuResultAll(scheduleId, tabNm, colNm); logInfo(qry); val vDf = spark.sql(qry).repartition(1);
    //vDF.cache.createOrReplaceTempView("RU_VALUE");

    ////---------------------------------------------------
    //   logInfo(s"""[SEL] RU_BIN_CNT ${scheduleId}""");
    ////---------------------------------------------------
    //def getRuBinCounts(scheduleId: String, ruId: (String, String)): (Int, Int) = {
    //  val qry = MakeBinFileSql.select2dRuBinCnt(scheduleId, ruId._1); logInfo(qry); val cntDf = spark.sql(qry)
    //  val row = cntDf.collect.last
    //  val x_bin_cnt = row(0).asInstanceOf[Int]
    //  val y_bin_cnt = row(1).asInstanceOf[Int]
    //  (x_bin_cnt, y_bin_cnt)
    //}

    //---------------------------------------------------------------------------------------------------------
    logInfo(s"""파일  write start ${scheduleId}""");
    //---------------------------------------------------------------------------------------------------------
    //qry = MakeBinFileSql.selectAllRuResult2(); logInfo(qry);
    //import spark.implicits._
    //val sqlDf2 = spark.sql(qry).repartition($"RU_ID")

    println("ruBldCnt start ==============================================================================>")
    ruBldCnt.foreach( m => println(m._1))
    println("ruSumBinCnt start ==============================================================================>")
    ruSumBinCnt.foreach( m => println(m._1))
    println("imRUBldHeaders start ==============================================================================>")
    imRUBldHeaders.foreach( m => println(m._1))

    valueDF.foreachPartition { p =>
      println("partition start ==============================================================================>")

      // bin 초기화
      var ruId: Option[String] = None
      var xSize: Option[Int] = None
      var ySize: Option[Int] = None
      var bin: Option[Array[Byte4]] = None


      p.foreach { row =>
        // RU_ID, X_BIN_CNT, Y_BIN_CNT, X_POINT, Y_POINT, VALUE, RU_PATH
        val ru_id = row(0).asInstanceOf[String] // RU_ID
        val xPos = row(3).asInstanceOf[Int]    // X_POINT
        val yPos = row(4).asInstanceOf[Int]    // Y_POINT
        val value = row(5)                     // VALUE
        /*
                val ru_path = row(6).asInstanceOf[String] // RU_PATH
        */

        if (ruId.isEmpty) {
          ruId = Some(ru_id)
          xSize = Some(ruInfo(ru_id).xBinCount)
          ySize = Some(ruInfo(ru_id).yBinCount)
          bin = Some(initialArray(xSize.get, ySize.get, colType))
        }

        setBinPoint(bin.get, xPos, yPos, xSize.get, value, colType)
      }

      // ruId.get
      if (ruId.isDefined) {
        writeBin(ruBldCnt(ruId.get), resolution, imRUBldHeaders(ruId.get), ruSumBinCnt(ruId.get)._1, bin.get, s"${targetPath}/${ruPathInfo(ruId.get)}/${targetFileName}.bin")
      }

      println("partition end")
    }
  }

}
