package com.sccomz.analysis.bd

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 3D LOS
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object Header extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: LOS <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"LOS $scheduleId").
      //      .config("queue", queueNm)
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeScenario(scheduleId)

    spark.close()
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_BF_SCEN_HEADER, scheduleId)
// SELECT row_number()(value_expr) OVER (PARTITION BY window_partition ORDER BY window_ordering) from table
    val qry =
      s"""
        |with AREA as
        |(
        |select a.scenario_id, b.schedule_id,
        |       a.buildinganalysis3d_resolution as resolution
        |  from SCENARIO a, SCHEDULE b
        | where b.schedule_id = ${scheduleId}
        |   and a.scenario_id = b.scenario_id
        |),
        |HEADERtemp as
        |(
        |select a.schedule_id, a.tbd_key,
        |       b.nx as nx2, b.ny as ny2, b.floorz as floorz2, b.ext_sx as ext_sx2, b.ext_sy as ext_sy2,
        |       c.nx as nx5, c.ny as ny5, c.floorz as floorz5, c.ext_sx as ext_sx5, c.ext_sy as ext_sy5
        |  from RESULT_NR_BF_TBDKEY a, BUILDING_3DS_HEADER b, BUILDING_3DS_HEADER_5BY5 c
        | where a.schedule_id = ${scheduleId}
        |   and a.tbd_key = b.tbd_key
        |   and a.tbd_key = c.tbd_key
        |)
        |-- insert into RESULT_NR_BF_SCEN_HEADER partition(schedule_id)
        |SELECT a.tbd_key,
        |       row_number() over (order by a.tbd_key) - 1 as building_index,
        |       if (b.resolution = 2, nx2, nx5) as nx,
        |       if (b.resolution = 2, ny2, ny5) as ny,
        |       if (b.resolution = 2, floorz2, floorz5) as floorz,
        |       if (b.resolution = 2, ext_sx2, ext_sx5) as ext_sx,
        |       if (b.resolution = 2, ext_sy2, ext_sy5) as ext_sy,
        |       a.schedule_id
        |  from HEADERtemp a, AREA b
        | where a.schedule_id = b.schedule_id
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_SCEN_HEADER)
  }

  def executeSite(scheduleId: String)(implicit spark: SparkSession) = {
    //    dropTablePartition(RESULT_NR_BF_RU_HEADER, scheduleId)

    val qry =
      s"""
         |with AREA as
         |(
         |select a.scenario_id, b.schedule_id,
         |       a.buildinganalysis3d_resolution as resolution
         |  from SCENARIO a, SCHEDULE b
         | where b.schedule_id = ${scheduleId}
         |   and a.scenario_id = b.scenario_id
         |),
         |HEADERtemp as
         |(
         |select a.schedule_id, a.ru_id, a.tbd_key,
         |       b.nx as nx2, b.ny as ny2, b.floorz as floorz2, b.ext_sx as ext_sx2, b.ext_sy as ext_sy2,
         |       c.nx as nx5, c.ny as ny5, c.floorz as floorz5, c.ext_sx as ext_sx5, c.ext_sy as ext_sy5
         |  from RESULT_NR_BF_RU_TBDKEY a, BUILDING_3DS_HEADER b, BUILDING_3DS_HEADER_5BY5 c
         | where a.schedule_id = ${scheduleId}
         |   and a.tbd_key = b.tbd_key
         |   and a.tbd_key = c.tbd_key
         |)
         |-- insert into RESULT_NR_BF_RU_HEADER partition(schedule_id)
         |SELECT a.ru_id, a.tbd_key,
         |       row_number() over (order by a.ru_id, a.tbd_key) - 1 as building_index,
         |       if (b.resolution = 2, nx2, nx5) as nx,
         |       if (b.resolution = 2, ny2, ny5) as ny,
         |       if (b.resolution = 2, floorz2, floorz5) as floorz,
         |       if (b.resolution = 2, ext_sx2, ext_sx5) as ext_sx,
         |       if (b.resolution = 2, ext_sy2, ext_sy5) as ext_sy,
         |       a.schedule_id
         |  from HEADERtemp a, AREA b
         | where a.schedule_id = b.schedule_id
         |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_RU_HEADER)
  }

}