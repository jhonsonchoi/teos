package com.sccomz.analysis.bd

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 3D BestServer
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object BestServer extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 2) {
      println("usage: BestServer <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"BestServer $scheduleId").
      //      .config("queue", queueNm)
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeScenario(scheduleId)

    spark.close()
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_BF_BESTSERVER, scheduleId)

    val qry =
      s"""
        |with RSLT as
        |(
        |SELECT a.schedule_id, a.ru_id, a.tbd_key, a.rx_tm_xpos, a.rx_tm_ypos, a.rx_floorz, a.bin_type, b.ru_seq
        |  FROM
        |    (
        |    select schedule_id, ru_id, tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz, bin_type
        |      from
        |        (
        |        select schedule_id, ru_id, tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz, bin_type,
        |               rsrppilot,
        |               row_number() over(partition by rx_tm_xpos, rx_tm_ypos, rx_floorz order by rsrppilot desc) rsrppilot_ord
        |          from ${RESULT_NR_BF_RSRPPILOT_RU}
        |         where schedule_id = $scheduleId
        |        ) a
        |     where a.rsrppilot_ord = 1
        |    ) a,
        |    (
        |    SELECT a.schedule_id, b.ru_id, b.ru_seq
        |      from SCHEDULE a, SCENARIO_NR_RU b
        |     WHERE a.schedule_id = $scheduleId
        |       AND a.scenario_id = b.scenario_id
        |    ) b
        |where a.schedule_id = b.schedule_id
        |  and a.ru_id = b.ru_id
        |)
        |-- insert into ${RESULT_NR_BF_BESTSERVER} partition (schedule_id)
        |select tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz,
        |       case when sum(bin_type) > 0 then 1 else 0 end as in_out,
        |       max(RSLT.ru_seq) as ru_seq,
        |       max(schedule_id) as schedule_id
        |  from RSLT
        | group by tbd_key, rx_tm_xpos, rx_tm_ypos, rx_floorz
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_BF_BESTSERVER)
  }

}
