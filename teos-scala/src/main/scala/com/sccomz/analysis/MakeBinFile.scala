package com.sccomz.analysis

import com.sccomz.serialize.{Byte4, ByteUtil}
import org.apache.hadoop.conf.Configuration

case class BldHeader(index: Int, key: String, binXCnt: Int, binYCnt: Int, floorZ: Int, startX: Float, startY: Float, startPoint: Long)

trait MakeBinFile {

  def initialArray(xSize: Int, ySize: Int, colType: String): Array[Byte4] = {
    initialArray(xSize * ySize, colType)
  }

  def initialArray(size: Int, colType: String): Array[Byte4] = {

    val initialValue = colType match {
      case "Int" => ByteUtil.intZero()
      case "Float" => ByteUtil.floatMax()
    }

    val iv = new Byte4(initialValue)

    Array.fill(size)(iv)
  }

  def setBinPoint(bin: Array[Byte4], xPos: Int, yPos: Int, xWidth: Int, value: Any, colType: String): Unit = {
    val pos = yPos * xWidth + xPos

    setBinPoint(bin, pos, value, colType)
  }

  def setBinPoint(bin: Array[Byte4], pos: Int, value: Any, colType: String): Unit = {
    bin(pos).value = colType match {
      case "Int" => ByteUtil.intToByteArray(value.asInstanceOf[Int])
      case "Float" => ByteUtil.floatToByteArray(value.asInstanceOf[Float])
    }
  }

  /**
   * HDFS 에 파일 출력
   *
   * @param bin
   * @param path
   */
  def writeBinToHDFS(bin: Array[Byte4], path: String) {
    import org.apache.hadoop.fs.{FileSystem, Path}
    val fs = FileSystem.get(new Configuration())
    val out = fs.create(new Path(path))

    bin.foreach { e =>
      out.write(e.value)
    }

    out.close()
  }

  def writeBin(bldCount: Int, resolution: Int, bldHeader: List[BldHeader], binCount: Long, bin: Array[Byte4], path: String): Unit = {
    //---------------------------------------------------------------------------------------------------------
    println(s"파일 Write start")
    //---------------------------------------------------------------------------------------------------------
    import org.apache.hadoop.fs.{FileSystem, Path}
    val fs = FileSystem.get(new Configuration())
    val out = fs.create(new Path(path))

    //val dos = new DataOutputStream(new FileOutputStream(sectorPathFileNm ));
    out.writeInt(ByteUtil.swap(bldCount));                                   // bldCount       int        4
    out.writeInt(ByteUtil.swap(resolution));                                 // resolution     int        4

    //---------------------------------------------------------------------------------------------------------
    println(s"파일 Write Header")
    //---------------------------------------------------------------------------------------------------------

    bldHeader.foreach { bldHeader =>
      out.writeInt(ByteUtil.swap(bldHeader.index))                // BUILDING_INDEX int        4
      out.write(ByteUtil.toByte20(bldHeader.key))            // TBD_KEY        char[20]  20
      out.write(bldHeader.binXCnt)                                  // BinXCnt        uchar      1
      out.write(bldHeader.binYCnt)                                  // BinYCnt        uchar      1
      out.write(bldHeader.floorZ)                                  // FloorZ         uchar      1
      out.write(bldHeader.floorZ)                                  // Padding        uchar      1
      out.writeFloat(ByteUtil.swap(bldHeader.startX))            // startX         float      4
      out.writeFloat(ByteUtil.swap(bldHeader.startY))            // startY         float      4
      out.writeLong(bldHeader.startPoint)                             // start point    ulong      8
    }

    //---------------------------------------------------------------------------------------------------------
    println(s"파일 Write binCount")
    //---------------------------------------------------------------------------------------------------------
    out.writeLong(binCount);                                                 // binCount        ulong     8

    //---------------------------------------------------------------------------------------------------------
    println(s"파일 Write binData")
    //---------------------------------------------------------------------------------------------------------

    bin.foreach { e =>
      out.write(e.value)
    }

    out.close()
  }

}
