package com.sccomz.analysis

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.SparkSession

/**
 * Common methods
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
class AbstractJob {
  val warehouseDir = "/TEOS/warehouse"

  def setHiveExecEnv()(implicit spark: SparkSession): Unit = {
    val qry1 = s"set hive.exec.dynamic.partition.mode=nonstrict"
    println(s"\n$qry1\n")
    spark.sql(qry1)
    val qry2 = s"set hive.exec.max.dynamic.partitions=30000" // default: 1000
    println(s"\n$qry2\n")
    spark.sql(qry2)
  }

  // schedule_id int
  def dropTablePartition(objNm: String, scheduleId: String)(implicit spark: SparkSession): Unit = {
    val qry = s"ALTER TABLE ${objNm} DROP IF EXISTS PARTITION (schedule_id=${scheduleId})"
    println(s"\n$qry\n")
    spark.sql(qry)

    val conf = new Configuration()
    val fs = FileSystem.get(conf)
    fs.delete(new Path(s"/TEOS/warehouse/${objNm}/schedule_id=${scheduleId}"), true)
  }

  def session(appName: String, instanceId: String): SparkSession = {
    val spark = SparkSession.builder()
      .appName(s"${appName} ${instanceId}")
      .config("spark.sql.warehouse.dir", warehouseDir)
      .enableHiveSupport()
      .getOrCreate()

    spark
  }
}

object AbstractJobTest extends AbstractJob {

  def main(args: Array[String]): Unit = {
    if (args.length < 1) {
      println("usage: AbstractJobTest <object_name> <schedule_id>")
      System.exit(1)
    }

    val objectName = args(0)
    val scheduleId = args(1)

    implicit val spark = session("AbstractJobTest", scheduleId)

    setHiveExecEnv()
    dropTablePartition(objectName, scheduleId)
  }

}
