package com.sccomz.analysis.d2

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 2D RSSI
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object RSSI extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: RSSI <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"RSSI $scheduleId").
      //      config("queue", queueNm).
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeSite(scheduleId)
    executeScenario(scheduleId)

    spark.close()
  }

  def executeSite(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_2D_RSSI_RU, scheduleId)

    val qry =
      s"""
        |with NR_PARAMETER as -- 파라미터 정보
        |(
        |select a.scenario_id, b.schedule_id, c.ant_category,
        |       c.number_of_cc, c.number_of_sc_per_rb, c.rb_per_cc, c.bandwidth_per_cc, c.subcarrierspacing,
        |       d.noisefigure
        |  from SCENARIO a, SCHEDULE b, NRSYSTEM c, MOBILE_PARAMETER d
        | where b.schedule_id = ${scheduleId}
        |   and a.scenario_id = b.scenario_id
        |   and a.scenario_id = c.scenario_id
        |   and a.scenario_id = d.scenario_id
        |)
        |-- insert into ${RESULT_NR_2D_RSSI_RU} partition (schedule_id)
        |select a.scenario_id, a.ru_id, a.enb_id, a.cell_id, a.rx_tm_xpos, a.rx_tm_ypos,
        |       a.los, a.pathloss, a.antenna_gain, a.pathlossprime, a.rsrppilot,
        |       if (RSSINoNoise = 0. , -9999, 10. * log10(RSSINoNoise)) as RSSINoNoise,
        |       if ((RSSINoNoise + MobileNoiseFloor) = 0. , -9999, 10. * log10((RSSINoNoise + MobileNoiseFloor))) as RSSI,
        |       a.schedule_id
        |  from
        |	(
        |	select a.scenario_id, a.ru_id, a.enb_id, a.cell_id, a.rx_tm_xpos, a.rx_tm_ypos,
        |	       a.los, a.pathloss, a.antenna_gain, a.pathlossprime, a.rsrppilot,
        |	       power(10 ,
        |	       case when upper(b.ant_category) = 'COMMON' then
        |	                    a.rsrppilot
        |	            else    a.rsrppilot + 10. * log10(b.number_of_cc * b.number_of_sc_per_rb * b.rb_per_cc)
        |	        end / 10.) as RSSINoNoise, -- dRssidBm
        |	       power(10,
        |	       case when upper(b.ant_category) = 'COMMON' then
        |	                    -174. + b.noisefigure + 10. * log10 ((b.subcarrierspacing / 1000.) * 1000000.)
        |	            else    -174. + b.noisefigure + 10. * log10 ((b.bandwidth_per_cc * b.number_of_cc) * 1000000.)
        |	        end / 10.)  as MobileNoiseFloor, -- m_dNowMilliWatt
        |	       a.schedule_id
        |	  from ${RESULT_NR_2D_RSRPPILOT_RU} a, NR_PARAMETER b
        |	 where a.schedule_id = ${scheduleId}
        |	   and a.schedule_id = b.schedule_id
        |	) a
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_2D_RSSI_RU)
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_2D_RSSI, scheduleId)

    val qry =
      s"""
        |with AREA as
        |(
        |select a.scenario_id, b.schedule_id,
        |       a.tm_startx div a.resolution * a.resolution as tm_startx,
        |       a.tm_starty div a.resolution * a.resolution as tm_starty,
        |       a.tm_endx div a.resolution * a.resolution as tm_endx,
        |       a.tm_endy div a.resolution * a.resolution as tm_endy,
        |       a.resolution
        |  from SCENARIO a, SCHEDULE b
        | where b.schedule_id = ${scheduleId}
        |   and a.scenario_id = b.scenario_id
        |)
        |-- insert into ${RESULT_NR_2D_RSSI} partition (schedule_id)
        |select max(AREA.scenario_id) as scenario_id,
        |       RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution as rx_tm_xpos,
        |       RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution as rx_tm_ypos,
        |       (RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution - AREA.tm_startx) / AREA.resolution as x_point,
        |       (RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution - AREA.tm_starty) / AREA.resolution as y_point,
        |       if (sum((power(10, (RSSINoNoise)/10.0))) = 0. , -9999, 10. * log10(sum((power(10, (RSSINoNoise)/10.0))))) as RSSI,
        |       max(AREA.schedule_id) as schedule_id
        |  from AREA, ${RESULT_NR_2D_RSSI_RU} RSLT
        | where RSLT.schedule_id = AREA.schedule_id
        |   and AREA.tm_startx <= RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution and RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution < AREA.tm_endx
        |   and AREA.tm_starty <= RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution and RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution < AREA.tm_endy
        |  group by RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution, RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution,
        |           (RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution - AREA.tm_startx) / AREA.resolution, (RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution - AREA.tm_starty) / AREA.resolution
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_2D_RSSI)
  }

}
