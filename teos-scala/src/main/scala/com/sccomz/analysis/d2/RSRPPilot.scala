package com.sccomz.analysis.d2

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 2D RSRPPilot
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object RSRPPilot extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: RSRPPilot <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"RSRPPilot $scheduleId").
      //      config("queue", queueNm).
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeSite(scheduleId)

    spark.close()
  }

  def executeSite(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_2D_RSRPPILOT_RU, scheduleId)

    val qry =
      s"""
        |    WITH PARAM AS
        |    (
        |    SELECT b.scenario_id, a.schedule_id, b.ru_id, b.enb_id, b.sector_ord as cell_id,
        |           nvl(d.mobilegain, 0) - nvl(d.feederloss,0) - nvl(d.carloss,0) - nvl(d.buildingloss,0) - nvl(d.bodyloss,0) as all_loss,
        |           b.fade_margin as ru_fade_margin, b.feeder_loss as ru_feeder_loss,
        |           c.beammismatchmargin, c.losbeamformingloss, c.nlosbeamformingloss,
        |           e.number_of_sc_per_rb,
        |           (c.txpwrdbm + c.powercombininggain + c.antennagain) - (10. * log10(e.number_of_cc * e.rb_per_cc)) - b.correction_value as EIRPPerRB
        |      FROM SCHEDULE a, SCENARIO_NR_RU b, NRSECTORPARAMETER c, MOBILE_PARAMETER d, NRSYSTEM e
        |     WHERE a.schedule_id = $scheduleId
        |--       and b.ru_id = :SCEN_RU_ID.RU_ID
        |       AND a.scenario_id = b.scenario_id
        |       and b.scenario_id = c.scenario_id
        |       and b.ru_id = c.ru_id
        |       and a.scenario_id = d.scenario_id
        |       and a.scenario_id = e.scenario_id
        |    ),
        |    SectorPara as
        |    (
        |    select a.ru_id, a.xposition as SitePositionX, a.yposition as SitePositionY,
        |           a.radius, c.height as rh, a.height as th, d.txtotalheight as tz,
        |           a.resolution
        |      from scenario_nr_ru a, schedule b, MOBILE_PARAMETER c, SCENARIO_NR_RU_AVG_HEIGHT d
        |      where b.schedule_id = $scheduleId
        |--       and a.ru_id = :SCEN_RU_ID.RU_ID
        |       and a.scenario_id = b.scenario_id
        |       and b.scenario_id = c.scenario_id
        |       and b.scenario_id = d.scenario_id
        |       and a.ru_id = d.ru_id
        |    ),
        |    SectorParaAnt as
        |    (
        |    select a.ru_id, a.antena_seq, a.orientation as AntOri, a.tilting as AntTilt
        |      from SCENARIO_NR_ANTENNA a, schedule b
        |      where b.schedule_id = $scheduleId
        |--        and a.ru_id = :SCEN_RU_ID.RU_ID
        |        and a.scenario_id = b.scenario_id
        |        and a.antena_seq > 0
        |    ),
        |    BinSector as
        |    (
        |        select ru_id, binX, binY, rx_tm_xpos, rx_tm_ypos, rz as rg
        |             , dPhi+dPhiAlpha as RadianPhi
        |             , case when (dTheta*180./PI() Between 0 and 180) THEN dTheta
        |                    else 0.
        |               end as RadianTheta
        |          from
        |        (
        |        select ta.ru_id, (ta.rx_tm_xpos + tb.resolution / 2.) as binX, (ta.rx_tm_ypos + tb.resolution / 2.) as binY, ta.rx_tm_xpos, ta.rx_tm_ypos, ta.rz
        |             , atan2((ta.rx_tm_ypos + tb.resolution / 2.) - tb.SitePositionY, (ta.rx_tm_xpos + tb.resolution / 2.) - tb.SitePositionX) as dPhi
        |             , case when ((ta.rx_tm_xpos + tb.resolution / 2.) >= tb.SitePositionX AND (ta.rx_tm_ypos + tb.resolution / 2.) >= tb.SitePositionY) THEN 3./2.*PI()
        |                    when ((ta.rx_tm_xpos + tb.resolution / 2.) <= tb.SitePositionX AND (ta.rx_tm_ypos + tb.resolution / 2.) >= tb.SitePositionY) THEN -1./2.*PI()
        |                    else 3./2.*PI()
        |                end as dPhiAlpha
        |             , atan2(tb.tz - (ta.rz + tb.rh), sqrt(power((ta.rx_tm_xpos + tb.resolution / 2.) - tb.SitePositionX,2) + power((ta.rx_tm_ypos + tb.resolution / 2.) - tb.SitePositionY,2)))
        |               + PI()/2. as dTheta
        |         from ${RESULT_NR_2D_PATHLOSS_RU} ta, SectorPara tb
        |         where ta.schedule_id = $scheduleId
        |           and ta.ru_id = tb.ru_id
        |        ) a
        |    ),
        |    BinDegree as
        |    (
        |    select ru_id, antena_seq, binX, binY, rx_tm_xpos, rx_tm_ypos
        |         , floor(PMOD(if(ChangedPhi=180, 360 - VDegree, VDegree), 360)) as VDegree
        |         , floor(HDegree) as HDegree
        |     from
        |        (
        |        select ru_id, antena_seq, binX, binY, rx_tm_xpos, rx_tm_ypos, rg, TmpPhi, sinTheta, sinPhiPrime, cosPhiPrime, cosAntTilt, cosTheta, sinAntTilt, tmpResult
        |             , PMOD(ChangedPhi+ if(TmpPhi > PI()*1./2. AND TmpPhi < PI()*3./2. AND ChangedPhi != 180, 180, 0) + 360 , 360) as ChangedPhi
        |             , if (ChangedTheta between 0 and 180, ChangedTheta, 0) as ChangedTheta
        |             , PMOD(ChangedTheta - 90 + 360, 360) as VDegree
        |             , if(ChangedTheta=0 or ChangedTheta=180, 0, PMOD(360 - ChangedPhi, 360)) as HDegree
        |          from
        |          (
        |            select ru_id, antena_seq, binX, binY, rx_tm_xpos, rx_tm_ypos, rg, TmpPhi, sinTheta, sinPhiPrime, cosPhiPrime, cosAntTilt, cosTheta, sinAntTilt
        |                 , ROUND(
        |                    case when (sinTheta*sinPhiPrime = 0 OR (sinTheta * cosPhiPrime * cosAntTilt - cosTheta * sinAntTilt) = 0) then TmpPhi
        |                         else
        |                            (
        |                                atan(
        |                                    (sinTheta * sinPhiPrime)
        |                                    /
        |                                    ((sinTheta * cosPhiPrime * cosAntTilt) - (cosTheta * sinAntTilt))
        |                                    )
        |                            ) * 180/PI()
        |                    end
        |                 , 1) as ChangedPhi
        |                 , (cosTheta * cosAntTilt + sinTheta * cosPhiPrime * sinAntTilt) as tmpResult
        |                 , ROUND(
        |                    acos(cosTheta * cosAntTilt + sinTheta * cosPhiPrime * sinAntTilt) * 180/PI()
        |                    , 1
        |                 ) as ChangedTheta
        |              from
        |              (
        |                    select ru_id, antena_seq, binX, binY, rx_tm_xpos, rx_tm_ypos, rg, TmpPhi
        |                         , if (sinTheta < 0.00000000000001 AND sinTheta > -0.00000000000001, 0., sinTheta) as sinTheta
        |                         , if (sinPhiPrime < 0.00000000000001 AND sinPhiPrime > -0.00000000000001, 0., sinPhiPrime) as sinPhiPrime
        |                         , if (cosPhiPrime < 0.00000000000001 AND cosPhiPrime > -0.00000000000001, 0., cosPhiPrime) as cosPhiPrime
        |                         , if (cosAntTilt < 0.00000000000001 AND cosAntTilt > -0.00000000000001, 0., cosAntTilt) as cosAntTilt
        |                         , if (cosTheta < 0.00000000000001 AND cosTheta > -0.00000000000001, 0., cosTheta) as cosTheta
        |                         , if (sinAntTilt < 0.00000000000001 AND sinAntTilt > -0.00000000000001, 0., sinAntTilt) as sinAntTilt
        |                    from
        |                    (
        |                    select ta.ru_id, tb.antena_seq, ta.binX, ta.binY, ta.rx_tm_xpos, ta.rx_tm_ypos, ta.rg
        |                         , ta.RadianPhi+tb.AntOri as TmpPhi
        |                         , SIN(ta.RadianTheta) as sinTheta
        |                         , SIN(ta.RadianPhi+tb.AntOri) as sinPhiPrime
        |                         , COS(ta.RadianPhi+tb.AntOri) as cosPhiPrime
        |                         , COS(tb.AntTilt) as cosAntTilt
        |                         , COS(ta.RadianTheta) as cosTheta
        |                         , SIN(tb.AntTilt) as sinAntTilt
        |                     from BinSector ta, SectorParaAnt tb
        |                     where ta.ru_id = tb.ru_id
        |                    ) a
        |              ) a
        |          ) a
        |        ) a
        |    ),
        |    BinHDegree as
        |    (
        |    select a.ru_id, a.antena_seq, a.binX, a.binY, a.rx_tm_xpos, a.rx_tm_ypos,
        |           a.HDegree, nvl(b.horizontal, 0) as horizontal
        |      from BinDegree a left outer join ANTENABASE_PATTERN b
        |        on (a.antena_seq = b.antena_seq and HDegree = b.degree)
        |    ),
        |    BinVDegree as
        |    (
        |    select a.ru_id, a.antena_seq, a.binX, a.binY, a.rx_tm_xpos, a.rx_tm_ypos,
        |           a.VDegree, nvl(b.vertical, 0) as vertical
        |      from BinDegree a left outer join ANTENABASE_PATTERN b
        |        on (a.antena_seq = b.antena_seq and VDegree = b.degree)
        |    ),
        |    BinTemp as
        |    (
        |    select a.ru_id, a.antena_seq, a.rx_tm_xpos, a.rx_tm_ypos,
        |           a.HDegree, a.horizontal,
        |           b.VDegree, b.vertical
        |      from BinHDegree a, BinVDegree b
        |     where a.ru_id = b.ru_id
        |       and a.antena_seq = b.antena_seq
        |       and a.rx_tm_xpos = b.rx_tm_xpos
        |       and a.rx_tm_ypos = b.rx_tm_ypos
        |    ),
        |    ANTGAIN as
        |    (
        |    select ru_id, rx_tm_xpos, rx_tm_ypos,
        |           nvl(sum(horizontal) + sum(vertical), 0) as antgain,
        |           null as los,
        |           null as pathloss,
        |           null as plprime,
        |           null as rsrppilot
        |      from BinTemp
        |     group by ru_id, rx_tm_xpos, rx_tm_ypos
        |    ),
        |    PLPRIME_temp AS
        |    (
        |    SELECT PATHLOSS.scenario_id, PATHLOSS.ru_id, PARAM.enb_id, PARAM.cell_id,
        |           PATHLOSS.rx_tm_xpos, PATHLOSS.rx_tm_ypos, PATHLOSS.los, PATHLOSS.pathloss,
        |           PATHLOSS.pathloss -
        |           (
        |           0 -- set antenna gain after.
        |           + PARAM.all_loss
        |           - PARAM.ru_fade_margin
        |           - PARAM.ru_feeder_loss
        |           - PARAM.beammismatchmargin
        |           )
        |           + IF (PATHLOSS.los = 1, PARAM.losbeamformingloss, PARAM.nlosbeamformingloss) as pathlossprime,
        |           PARAM.EIRPPerRB,
        |           PARAM.number_of_sc_per_rb,
        |           PATHLOSS.schedule_id
        |      FROM ${RESULT_NR_2D_PATHLOSS_RU} PATHLOSS, PARAM
        |     WHERE PATHLOSS.schedule_id = $scheduleId
        |       and PATHLOSS.schedule_id = PARAM.schedule_id
        |       AND PATHLOSS.ru_id = PARAM.ru_id
        |    )
        |    -- insert into ${RESULT_NR_2D_RSRPPILOT_RU} partition (schedule_id)
        |    select PLPRIME_temp.scenario_id, PLPRIME_temp.ru_id, PLPRIME_temp.enb_id, PLPRIME_temp.cell_id,
        |           PLPRIME_temp.rx_tm_xpos, PLPRIME_temp.rx_tm_ypos,
        |           PLPRIME_temp.los, PLPRIME_temp.pathloss,
        |           ANTGAIN.antgain,
        |           PLPRIME_temp.pathloss - nvl(ANTGAIN.antgain,0) as pathlossprime,
        |           PLPRIME_temp.EIRPPerRB - 10. * log10(PLPRIME_temp.number_of_sc_per_rb) - (PLPRIME_temp.pathloss - nvl(ANTGAIN.antgain,0)) as RSRPPilot,
        |           ANTGAIN.antgain, ANTGAIN.los, ANTGAIN.pathloss, ANTGAIN.plprime, ANTGAIN.rsrppilot,
        |           PLPRIME_temp.schedule_id
        |      from PLPRIME_temp left outer join ANTGAIN
        |        on (PLPRIME_temp.rx_tm_xpos = ANTGAIN.rx_tm_xpos and PLPRIME_temp.rx_tm_ypos = ANTGAIN.rx_tm_ypos and PLPRIME_temp.ru_id = ANTGAIN.ru_id)
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_2D_RSRPPILOT_RU)
  }

}
