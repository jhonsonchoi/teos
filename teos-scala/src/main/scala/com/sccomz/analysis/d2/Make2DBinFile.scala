package com.sccomz.analysis.d2

import java.sql.Statement

import com.sccomz.analysis.MakeBinFile
import com.sccomz.comm.util.DateUtil
import com.sccomz.rdb.OracleCon
import com.sccomz.serialize.{Byte4, MakeBinFileSql}
import com.sccomz.wf.{HiveSchema, MyEnv, MyRU}
import org.apache.spark.internal.Logging
import org.apache.spark.sql.SparkSession

import scala.collection.Map

/*
     , B.USER_ID
     , A.ENB_ID
     , A.PCI
     , A.PCI_PORT
     , A.RU_ID

 */
//case class RU(id: String, enbId: String, pci: String, pciPort: String)

object Make2DBinFile extends HiveSchema with MakeBinFile with Logging {

  val resultRoot = "/user/icpap/result"


  def main(args: Array[String]): Unit = {
    if (args.length == 1) {
      println("usage: MakeBinFile <schedule_id>")
    }

    val scheduleId = args(0)


    val conn = OracleCon.getCon()
    implicit val stmt = conn.createStatement()


    val schedule = MyEnv.getSchedule(scheduleId)
    val scenario = MyEnv.getScenario(schedule.scenarioId)

    val userId = scenario.userId
    val ruInfo = MyEnv.makeResultPath(scenario.id)
    val targetPath = s"$resultRoot/${DateUtil.getDate("yyyyMMdd")}/$userId/$scheduleId"

    val ruPathInfo = ruInfo.map { ru =>
      (ru._1, s"ENB_${ru._2.enbId}/PCI_${ru._2.pci}_PORT_${ru._2.pciPort}_${ru._2.id}")
    }


    implicit val spark: SparkSession = SparkSession.builder()
      .appName(s"MakeBinFile $scheduleId"
      ).getOrCreate()

//    executeEngResult(scheduleId)
    //executeEngResult("8460062");

    // separate this function from the job
    //    val ruInfo = MakeFolder.makeResultPath(scheduleId)
    //    val sectorPath = ruInfo.getOrElse("SECTOR_PATH", "")

    if (schedule.scheduleType == "SC051") {

    } else {
      // 스케쥴 단위

      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "LOS", RESULT_NR_2D_LOS, "LOS", "Int")
      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "PATHLOSS", RESULT_NR_2D_PATHLOSS, "PATHLOSS", "Float")
      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "BESTSERVER", RESULT_NR_2D_BESTSERVER, "RU_SEQ", "Int")
      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "PILOT_EC", RESULT_NR_2D_RSRP, "RSRP", "Float")
      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "RSSI", RESULT_NR_2D_RSSI, "RSSI", "Float")
      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "C2I", RESULT_NR_2D_SINR, "SINR", "Float")
      Make2DBinFile.makeScheduleResult(scheduleId, targetPath, "DOWNLINK_THROUGHPUT", RESULT_NR_2D_THROUGHPUT, "THROUGHPUT", "Float")

      // ru 단위

      Make2DBinFile.makeRuResult(scheduleId, targetPath, "LOS", RESULT_NR_2D_LOS_RU, "VALUE", "Int", ruInfo, ruPathInfo)
      Make2DBinFile.makeRuResult(scheduleId, targetPath, "PATHLOSS", RESULT_NR_2D_PATHLOSS_RU, "PATHLOSS", "Float", ruInfo, ruPathInfo)
      Make2DBinFile.makeRuResult(scheduleId, targetPath, "PILOT_EC", RESULT_NR_2D_RSRP_RU, "RSRP", "Float", ruInfo, ruPathInfo)
      Make2DBinFile.makeRuResult(scheduleId, targetPath, "RSSI", RESULT_NR_2D_RSSI_RU, "RSSI", "Float", ruInfo, ruPathInfo)
      Make2DBinFile.makeRuResult(scheduleId, targetPath, "C2I", RESULT_NR_2D_SINR_RU, "SINR", "Float", ruInfo, ruPathInfo)
    }


    spark.stop()

    stmt.close()
    conn.close()
  }

  /**
    * 2D 섹터 결과
    *
    * /user/icpap/result/<yyyyMMdd>/<USER_ID>/<SCHEDULE_ID>/<alg file name>.bin
    *
    * @param spark
    * @param scheduleId
    * @param targetPath
    */
  def makeScheduleResult(scheduleId: String, targetPath: String, targetFileName: String, tableName: String, colName: String, colType: String)(implicit stmt: Statement, spark: SparkSession) = {

    val scheduleSize = MyEnv.getScheduleSize(scheduleId)

    //---------------------------------------------------
     logInfo(s"""[SEL] value ${scheduleId}""");
    //---------------------------------------------------

    // data

    val qry = MakeBinFileSql.selectSectorResult(scheduleId, tableName, colName)
    println(qry)
    val valueDF = spark.sql(qry).repartition(1)

    //---------------------------------------------------------------------------------------------------------
     logInfo(s"""파일  write start ${scheduleId}""");
    //---------------------------------------------------------------------------------------------------------

    valueDF.foreachPartition { p =>
      println("partition start ==============================================================================>")

      val xSize = scheduleSize.binXCnt
      val ySize = scheduleSize.binYCnt

      val bin = initialArray(xSize, ySize, colType)


      p.foreach { row =>
        // X_POINT, Y_POINT, VALUE
        val xPos = row(0).asInstanceOf[Int]
        val yPos = row(1).asInstanceOf[Int]
        val value = row(2)

        setBinPoint(bin, xPos, yPos, xSize, value, colType)
      }

      logDebug("writing to file")

      writeBinToHDFS(bin, s"${targetPath}/${targetFileName}.bin")

      logDebug("partition end")
    }
  }

  /**
    * 2D RU별 결과
    *
    * /user/icpap/result/<yyyyMMdd>/<USER_ID>/<SCHEDULE_ID>/ENB_<ENB_ID>/PCI_<PCI>_PORT_<PCI_PORT>_<RU_ID>/<alg file name>.bin
    *
    * @param spark
    * @param scheduleId
    * @param targetPath
    * @param ruPathInfo
    */
  def makeRuResult(scheduleId: String, targetPath: String, targetFileName: String, tableName: String, colName: String, colType: String, ruInfo: Map[String, MyRU], ruPathInfo: Map[String, String])(implicit stmt: Statement, spark: SparkSession) = {

    //---------------------------------------------------
       logInfo(s"""[SEL] RU_VALUE ${scheduleId}""");
    //---------------------------------------------------

    // data

    import spark.implicits._

    val qry = MakeBinFileSql.selectRuResultAll(scheduleId, tableName, colName)
    println(qry)
    val valueDF = spark.sql(qry).repartition($"RU_ID")
    //qry = MakeBinFileSql.selectRuResultAll(scheduleId, tabNm, colNm); logInfo(qry); val vDf = spark.sql(qry).repartition(1);
    //vDF.cache.createOrReplaceTempView("RU_VALUE");

    ////---------------------------------------------------
    //   logInfo(s"""[SEL] RU_BIN_CNT ${scheduleId}""");
    ////---------------------------------------------------
    //def getRuBinCounts(scheduleId: String, ruId: (String, String)): (Int, Int) = {
    //  val qry = MakeBinFileSql.select2dRuBinCnt(scheduleId, ruId._1); logInfo(qry); val cntDf = spark.sql(qry)
    //  val row = cntDf.collect.last
    //  val x_bin_cnt = row(0).asInstanceOf[Int]
    //  val y_bin_cnt = row(1).asInstanceOf[Int]
    //  (x_bin_cnt, y_bin_cnt)
    //}

    //---------------------------------------------------------------------------------------------------------
       logInfo(s"""파일  write start ${scheduleId}""");
    //---------------------------------------------------------------------------------------------------------
    //qry = MakeBinFileSql.selectAllRuResult2(); logInfo(qry);
    //import spark.implicits._
    //val sqlDf2 = spark.sql(qry).repartition($"RU_ID")

    valueDF.foreachPartition { p =>
      println("partition start ==============================================================================>")

      // bin 초기화
      var ruId: Option[String] = None
      var xSize: Option[Int] = None
      var ySize: Option[Int] = None
      var bin: Option[Array[Byte4]] = None

      println("p.foreach")


      p.foreach { row =>
        // RU_ID, X_BIN_CNT, Y_BIN_CNT, X_POINT, Y_POINT, VALUE, RU_PATH
        val ru_id = row(0).asInstanceOf[String] // RU_ID
        val xPos = row(3).asInstanceOf[Int]    // X_POINT
        val yPos = row(4).asInstanceOf[Int]    // Y_POINT
        val value = row(5)                     // VALUE
        /*
                val ru_path = row(6).asInstanceOf[String] // RU_PATH
        */

        if (ruId.isEmpty) {
          ruId = Some(ru_id)
          xSize = Some(ruInfo(ru_id).xBinCount)
          ySize = Some(ruInfo(ru_id).yBinCount)
          bin = Some(initialArray(xSize.get, ySize.get, colType))
        }

        setBinPoint(bin.get, xPos, yPos, xSize.get, value, colType)
      }

      // ruId.get
      if (ruId.isDefined) {
        writeBinToHDFS(bin.get, s"${targetPath}/${ruPathInfo(ruId.get)}/${targetFileName}.bin")
      }

      println("partition end")
    }
  }

}
