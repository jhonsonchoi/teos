package com.sccomz.analysis.d2

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 2D LOS
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object LOS extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: LOS <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"LOS $scheduleId").
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeScenario(scheduleId)

    spark.close()
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_2D_LOS, scheduleId)

    val qry =
      s"""
        |with AREA as
        |(
        |select a.scenario_id, b.schedule_id,
        |       a.tm_startx div a.resolution * a.resolution as tm_startx,
        |       a.tm_starty div a.resolution * a.resolution as tm_starty,
        |       a.tm_endx div a.resolution * a.resolution as tm_endx,
        |       a.tm_endy div a.resolution * a.resolution as tm_endy,
        |       a.resolution
        |  from SCENARIO a, SCHEDULE b
        | where b.schedule_id = ${scheduleId}
        |   and a.scenario_id = b.scenario_id
        |)
        |-- insert into table ${RESULT_NR_2D_LOS} partition (schedule_id=${scheduleId})
        |select max(AREA.scenario_id) as scenario_id,
        |       RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution as rx_tm_xpos,
        |       RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution as rx_tm_ypos,
        |       (RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution - AREA.tm_startx) / AREA.resolution as x_point,
        |       (RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution - AREA.tm_starty) / AREA.resolution as y_point,
        |       case when sum(case when RSLT.value = 1 then 1 else 0 end) > 0 then 1 else 0 end as los,
        |       max(AREA.schedule_id) as schedule_id
        |  from AREA, ${RESULT_NR_2D_LOS_RU} RSLT
        | where RSLT.schedule_id = AREA.schedule_id
        |   --and RSLT.ru_id = 1011760123
        |   and AREA.tm_startx <= RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution and RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution < AREA.tm_endx
        |   and AREA.tm_starty <= RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution and RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution < AREA.tm_endy
        |  group by RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution, RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution,
        |           (RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution - AREA.tm_startx) / AREA.resolution, (RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution - AREA.tm_starty) / AREA.resolution
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_2D_LOS)
  }

}
