package com.sccomz.analysis.d2

import com.sccomz.analysis.AbstractJob
import com.sccomz.wf.HiveSchema
import org.apache.spark.internal.Logging
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * 2D BestServer
 *
 * @author Jhonson Choi (jhonsonchoi@gmail.com)
 */
object BestServer extends AbstractJob with HiveSchema with Logging {

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("usage: BestServer <schedule_id>")
      System.exit(1)
    }

    val scheduleId = args(0)

    logInfo(s"schedule_id=$scheduleId")


    implicit val spark: SparkSession = SparkSession.builder().
      master("yarn").
      appName(s"BestServer $scheduleId").
      //      config("queue", queueNm).
      config("spark.sql.warehouse.dir", warehouseDir).
      enableHiveSupport().
      getOrCreate()

    setHiveExecEnv()
    executeScenario(scheduleId)

    spark.close()
  }

  def executeScenario(scheduleId: String)(implicit spark: SparkSession) = {
//    dropTablePartition(RESULT_NR_2D_BESTSERVER, scheduleId)

    val qry =
      s"""
        |
        |with AREA as
        |(
        |select a.scenario_id, b.schedule_id,
        |       a.tm_startx div a.resolution * a.resolution as tm_startx,
        |       a.tm_starty div a.resolution * a.resolution as tm_starty,
        |       a.tm_endx div a.resolution * a.resolution as tm_endx,
        |       a.tm_endy div a.resolution * a.resolution as tm_endy,
        |       a.resolution
        |  from SCENARIO a, SCHEDULE b
        | where b.schedule_id = ${scheduleId}
        |   and a.scenario_id = b.scenario_id
        |),
        |RSLT as
        |(
        |SELECT a.scenario_id, a.schedule_id, a.rx_tm_xpos, a.rx_tm_ypos, b.ru_seq
        |  FROM
        |	(
        |	select scenario_id, schedule_id, rx_tm_xpos, rx_tm_ypos, ru_id
        |	  from
        |		(
        |		select scenario_id, schedule_id, rx_tm_xpos, rx_tm_ypos,
        |		       ru_id, rsrppilot,
        |		       row_number() over(partition by rx_tm_xpos, rx_tm_ypos order by rsrppilot desc) rsrppilot_ord
        |		  from ${RESULT_NR_2D_RSRPPILOT_RU}
        |		 where schedule_id = ${scheduleId}
        |		) a
        |	 where a.rsrppilot_ord = 1
        |	) a,
        |	(
        |	SELECT a.schedule_id, b.ru_id, b.ru_seq
        |	  from SCHEDULE a, SCENARIO_NR_RU b
        |	 WHERE a.schedule_id = ${scheduleId}
        |	   AND a.scenario_id = b.scenario_id
        |	) b
        |where a.schedule_id = b.schedule_id
        |  and a.ru_id = b.ru_id
        |)
        |-- insert into ${RESULT_NR_2D_BESTSERVER} partition (schedule_id)
        |select max(AREA.scenario_id) as scenario_id,
        |       RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution as rx_tm_xpos,
        |       RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution as rx_tm_ypos,
        |       (RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution - AREA.tm_startx) / AREA.resolution as x_point,
        |       (RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution - AREA.tm_starty) / AREA.resolution as y_point,
        |       max(RSLT.ru_seq) as ru_seq,
        |       max(AREA.schedule_id) as schedule_id
        |  from AREA, RSLT
        | where RSLT.schedule_id = AREA.schedule_id
        |   and AREA.tm_startx <= RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution and RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution < AREA.tm_endx
        |   and AREA.tm_starty <= RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution and RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution < AREA.tm_endy
        |  group by RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution, RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution,
        |           (RSLT.rx_tm_xpos div AREA.resolution * AREA.resolution - AREA.tm_startx) / AREA.resolution, (RSLT.rx_tm_ypos div AREA.resolution * AREA.resolution - AREA.tm_starty) / AREA.resolution
        |""".stripMargin

    println(qry)

    val df = spark.sql(qry)

    df.write
      .format("hive")
      .mode(SaveMode.Overwrite)
      .insertInto(RESULT_NR_2D_BESTSERVER)
  }

}
