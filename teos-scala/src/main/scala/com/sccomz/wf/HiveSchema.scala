package com.sccomz.wf

import com.typesafe.config.ConfigFactory

trait HiveSchema {
  val cf = ConfigFactory.load()

  val SCENARIO = cf.getString("hive.schema.scenario") //"SCENARIO"
  val SCENARIO_NR_RU = cf.getString("hive.schema.scenario_nr_ru") //"SCENARIO_NR_RU"
  val SCENARIO_NR_ANTENNA = cf.getString("hive.schema.scenario_nr_antenna") //"SCENARIO_NR_ANTENNA"
  val MOBILE_PARAMETER = cf.getString("hive.schema.mobile_parameter") //"MOBILE_PARAMETER"
  val NRUETRAFFIC = cf.getString("hive.schema.nruetraffic") //"NRUETRAFFIC"
  val NRSECTORPARAMETER = cf.getString("hive.schema.nrsectorparameter") //"NRSECTORPARAMETER"
  val NRSYSTEM = cf.getString("hive.schema.nrsystem") //"NRSYSTEM"
  val SCHEDULE = cf.getString("hive.schema.schedule") //"SCHEDULE"

  val SCENARIO_NR_RU_AVG_HEIGHT = cf.getString("hive.schema.scenario_nr_ru_avg_height") //"SCENARIO_NR_RU_AVG_HEIGHT"

  // todo 임시
  val RESULT_NR_2D_LOS_RU = cf.getString("hive.schema.result_nr_2d_los_ru") // "RESULT_NR_2D_LOS_RU_JHONSON"

  val RESULT_NR_2D_LOS = cf.getString("hive.schema.result_nr_2d_los") // "RESULT_NR_2D_LOS_JHONSON"
  val RESULT_NR_2D_PATHLOSS_RU = cf.getString("hive.schema.result_nr_2d_pathloss_ru") // "RESULT_NR_2D_PATHLOSS_RU"
  val RESULT_NR_2D_PATHLOSS = cf.getString("hive.schema.result_nr_2d_pathloss") // "RESULT_NR_2D_PATHLOSS"
  val RESULT_NR_2D_RSRPPILOT_RU = cf.getString("hive.schema.result_nr_2d_rsrppilot_ru") // "RESULT_NR_2D_RSRPPILOT_RU"
  val RESULT_NR_2D_RSRP_RU = cf.getString("hive.schema.result_nr_2d_rsrp_ru") // "RESULT_NR_2D_RSRP_RU"
  val RESULT_NR_2D_RSRP = cf.getString("hive.schema.result_nr_2d_rsrp") // "RESULT_NR_2D_RSRP"
  val RESULT_NR_2D_BESTSERVER = cf.getString("hive.schema.result_nr_2d_bestserver") // "RESULT_NR_2D_BESTSERVER"
  val RESULT_NR_2D_RSSI_RU = cf.getString("hive.schema.result_nr_2d_rssi_ru") // "RESULT_NR_2D_RSSI_RU"
  val RESULT_NR_2D_RSSI = cf.getString("hive.schema.result_nr_2d_rssi") // "RESULT_NR_2D_RSSI"
  val RESULT_NR_2D_SINR_RU = cf.getString("hive.schema.result_nr_2d_sinr_ru") // "RESULT_NR_2D_SINR_RU"
  val RESULT_NR_2D_SINR = cf.getString("hive.schema.result_nr_2d_sinr") // "RESULT_NR_2D_SINR"
  val RESULT_NR_2D_THROUGHPUT = cf.getString("hive.schema.result_nr_2d_throughput") // "RESULT_NR_2D_THROUGHPUT"

  // todo 임시
  val RESULT_NR_BF_RU_HEADER = "RESULT_NR_BF_RU_HEADER"
  val RESULT_NR_BF_SCEN_HEADER = "RESULT_NR_BF_SCEN_HEADER"

  val RESULT_NR_BF_LOS_RU = cf.getString("hive.schema.result_nr_bf_los_ru") // "RESULT_NR_BF_LOS_RU_JHONSON"

  val RESULT_NR_BF_LOS =  cf.getString("hive.schema.result_nr_bf_los") // "RESULT_NR_BF_LOS"
  val RESULT_NR_BF_PATHLOSS_RU =  cf.getString("hive.schema.result_nr_bf_pathloss_ru") // "RESULT_NR_BF_PATHLOSS_RU"
  val RESULT_NR_BF_PATHLOSS =  cf.getString("hive.schema.result_nr_bf_pathloss") // "RESULT_NR_BF_PATHLOSS"
  val RESULT_NR_BF_RSRPPILOT_RU =  cf.getString("hive.schema.result_nr_bf_rsrppilot_ru") // "RESULT_NR_BF_RSRPPILOT_RU"
  val RESULT_NR_BF_RSRP_RU =  cf.getString("hive.schema.result_nr_bf_rsrp_ru") // "RESULT_NR_BF_RSRP_RU"
  val RESULT_NR_BF_RSRP =  cf.getString("hive.schema.result_nr_bf_rsrp") // "RESULT_NR_BF_RSRP"
  val RESULT_NR_BF_BESTSERVER =  cf.getString("hive.schema.result_nr_bf_bestserver") // "RESULT_NR_BF_BESTSERVER"
  val RESULT_NR_BF_RSSI_RU =  cf.getString("hive.schema.result_nr_bf_rssi_ru") // "RESULT_NR_BF_RSSI_RU"
  val RESULT_NR_BF_RSSI =  cf.getString("hive.schema.result_nr_bf_rssi") // "RESULT_NR_BF_RSSI"
  val RESULT_NR_BF_SINR_RU =  cf.getString("hive.schema.result_nr_bf_sinr_ru") // "RESULT_NR_BF_SINR_RU"
  val RESULT_NR_BF_SINR =  cf.getString("hive.schema.result_nr_bf_sinr") // "RESULT_NR_BF_SINR"
  val RESULT_NR_BF_THROUGHPUT =  cf.getString("hive.schema.result_nr_bf_throughput") // "RESULT_NR_BF_THROUGHPUT"

  val RESULT_NR_BF_TBDKEY =  cf.getString("hive.schema.result_nr_bf_tbdkey") // "RESULT_NR_BF_TBDKEY"
  val RESULT_NR_BF_RU_TBDKEY =  cf.getString("hive.schema.result_nr_bf_ru_tbdkey") // "RESULT_NR_BF_RU_TBD_KEY"
}
