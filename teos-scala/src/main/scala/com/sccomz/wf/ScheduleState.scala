package com.sccomz.wf

trait ScheduleState {
  val SCHEDULE_STATE_EDITED = ("10001",  "임시저장")
  val SCHEDULE_STATE_REQUESTED = ("20001",  "분석요청")
  val SCHEDULE_STATE_TEMP = ("10002",  "분석준비") // 굳이 사용하지 않아도 됨
  val SCHEDULE_STATE_ONGOING = ("20003",  "분석중")
  val SCHEDULE_STATE_COMPLETE = ("10004","분석완료")
  val SCHEDULE_STATE_FAILED = ("10009","분석실패")
}
