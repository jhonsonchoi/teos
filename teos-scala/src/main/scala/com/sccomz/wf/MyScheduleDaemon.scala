package com.sccomz.wf

import java.io.File
import java.sql.Statement

import com.sccomz.rdb.OracleCon
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.sys.process._

/**
  * 분석 요청 중인 시나리오의 분석을 실행한다.
  * 스케쥴 아이디는 사용자가 파라미터로 지정한 것을 사용한다.
  * 사용자가 지정하지 않은 경우, 데이터베이스에서 분석요청상태인 것을 선택한다.
  */
object MyScheduleDaemon extends ScheduleState {

  val logger = LoggerFactory.getLogger(getClass)


  def main(args: Array[String]): Unit = {
    val scheduleIdsCSV = if (args.length == 1) Some(args(0)) else None

    val con = OracleCon.getCon()

    implicit val stmt = con.createStatement()

    try {
      scheduleIdsCSV match {
        case Some(csv) =>
          execute(csv.split(","))
        case None =>
          execute()
      }
    } catch {
      case e: Throwable => logger.error(e.getLocalizedMessage, e)
    } finally {
      try { stmt.close() }
      try { con.close() }
    }
  }

  /**
   * DB에서 분석요청중인 스케쥴을 찾아 분석을 실행한다.
   *
   * @param stmt
   */
  def execute()(implicit stmt: Statement): Unit = {
    val reqs = MyEnv.selectScheduleReqs(SCHEDULE_STATE_ONGOING._1)

    reqs.foreach { req =>
      runSchedule(req)
    }
  }

  /**
   * 사용자가 지정한 스케쥴의 분석을 실행한다.
   * 분석을 시작하기 전에, 스케쥴이 분석요청중인가 검증한다.
   *
   * @param scheduleIds
   * @param stmt
   */
  def execute(scheduleIds: Array[String])(implicit stmt: Statement): Unit = {
    val reqs = MyEnv.selectScheduleReqs(scheduleIds)

    // todo 필터 형태로 변경

    reqs.foreach { req =>
      runSchedule(req)
    }
  }

  /**
    * todo use thread pool
    *
    * @param req
    * @param stmt
    */
  def runSchedule(req: ScheduleReq)(implicit stmt: Statement): Unit = {
//    val rrq = selectRichReq(req.scenarioId)
    new Thread(new ScheduleRunner(req.scheduleId)).run()
  }

}


/**
 * 지정된 스케쥴에 대한 분석을 실행한다.
 * 00-98 까지 단계가 있으며, 스크립트가 연속적으로 존재하면 단계별로 실행시켜 준다.
 *
 * @param scheduleId
 */
class ScheduleRunner(scheduleId: String) extends Runnable with ScheduleState with WorkflowState {

  val logger = LoggerFactory.getLogger(getClass)

  val cf = ConfigFactory.load()

  val wfCommandRoot = cf.getString("wf-schedule.command-root")


  override def run(): Unit = {

    val con = OracleCon.getCon()

    implicit val stmt = con.createStatement()

    try {
      // update schedule state 분석중
//      updateScheduleState(scheduleId, SCHEDULE_STATE_ONGOING._1, SCHEDULE_STATE_ONGOING._2)

      // 강제 시작한 경우를 위한 조치
      MyEnv.deleteScheduleStep(scheduleId)

      // 마지막 단계 99 가 아니면 1씩 증가시키면서 다음 단계의 스크립트가 있는 경우 다음 단계를 실행한다.
      // 단계는 00-99 까지 있을 수 있으며, 00 단계의 스크립트는 반드시 존재해야 한다.
      // 99 단계는 종료단계로 스크립트가 실행되지 않는다.

      // 시작 단계 설정
      MyEnv.insertScheduleStep(scheduleId, WORKFLOW_STARTED)


      var complete = false
      var failed = false

      while (!complete) {
        val state = MyEnv.selectScheduleStep(scheduleId)

        try {
          val command = s"${wfCommandRoot}/${state} ${scheduleId}"
          //      val res = Process(command).lineStream

          logger.info(command)

          val exitCode = command !

          if (exitCode != 0) logger.warn(s"실행결과코드가 0이 아님: $exitCode")

          val nextState = f"${state.toInt + 1}%02d"

          val f = new File(s"$wfCommandRoot/$nextState")

          val finalNextState =
            if (f.exists()) {
              nextState
            } else {
              "99"
            }
          MyEnv.updateScheduleStep(scheduleId, finalNextState, "")

          complete = finalNextState == WORKFLOW_STOPPED
        } catch {
          case e: Throwable =>
            logger.warn(e.getLocalizedMessage, e)
            // 분석실패 설정
            MyEnv.updateScheduleStep(scheduleId, WORKFLOW_STOPPED, e.getLocalizedMessage)
            complete = true
            failed = true
        }

        // todo 필요 없을지도
        if (!complete) {
          Thread.sleep(1000)
        }
      }

      // 분석 종료 상태 설정
      // 단계 실행 도중 실패가 있었던 경우, 시나리오에 속한 모든 스케쥴의 상태를 실패 상태로 변경하고 바로 종료한다.
      val finalState =
        if (failed)
          SCHEDULE_STATE_FAILED
        else
          SCHEDULE_STATE_COMPLETE

      MyEnv.updateScheduleState(scheduleId, finalState._1, finalState._2)
    } catch {
      case e: Throwable =>
        logger.error(e.getLocalizedMessage, e)
    } finally {
      try { stmt.close() }
      try { con.close() }
    }
  }

}

