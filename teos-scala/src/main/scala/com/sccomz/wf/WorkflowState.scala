package com.sccomz.wf

trait WorkflowState {
  val WORKFLOW_STARTED = "00"
  val WORKFLOW_STOPPED = "99"
}
