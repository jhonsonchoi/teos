package com.sccomz.wf

import java.io.File
import java.sql.Statement

import com.sccomz.rdb.OracleCon
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

import scala.sys.process._


/**
  * 분석 요청된 시나리오의 분석을 실행한다.
  * 시나리오 아이디는 사용자가 파라미터로 지정한 것을 사용한다.
  * 사용자가 지정한 시나리오라고 해도 상태가 분석요청중이어야 한다.
  * 사용자가 지정하지 않은 경우, 데이터베이스에서 분석요청상태인 것을 선택한다.
  *
  * 시나리오에 속한 스케쥴 실행에 필요한 준비 단계를 실행한다.
  */
object MyScenarioDaemon extends ScheduleState {

  val logger = LoggerFactory.getLogger(getClass)


  def main(args: Array[String]): Unit = {
    val scenarioIdsCSV = if (args.length == 1) Some(args(0)) else None

    val con = OracleCon.getCon()

    implicit val stmt = con.createStatement()

    try {
      scenarioIdsCSV match {
        case Some(csv) =>
          execute(csv.split(","))
        case None =>
          execute()
      }
    } catch {
      case e: Throwable => logger.error(e.getLocalizedMessage, e)
    } finally {
      try { stmt.close() }
      try { con.close() }
    }
  }

  /**
   * 분석요청된 시나리오를 구해 시나리오 분석을 실행한다.
   *
   * @param stmt
   */
  def execute()(implicit stmt: Statement): Unit = {
    val reqs = MyEnv.selectScenarioReqs(SCHEDULE_STATE_REQUESTED._1)

    reqs.foreach { req =>
      println(req)
      runScenario(req)
    }
  }

  /**
   * 시나리오 아이디 목록을 가지고 시나리오 분석을 실행한다.
   * 또한, 시나리오가 정말 분석요청중인가 다시 확인한다.
   *
   * @param scenarioIds
   * @param stmt
   */
  def execute(scenarioIds: Array[String])(implicit stmt: Statement): Unit = {
    val reqs = MyEnv.selectScenarioReqs(scenarioIds)

    // todo 필터 형태로 변경

    reqs.foreach { req =>
      runScenario(req)
    }
  }

  /**
   * 시나리오 분석을 실행한다.
   *
    * todo use thread pool
    *
    * @param req
    * @param stmt
    */
  def runScenario(req: ScenarioReq)(implicit stmt: Statement): Unit = {
//    val rrq = selectRichReq(req.scenarioId)
    new Thread(new ScenarioRunner(req.scenarioId)).run()
  }

}


/**
 * 지정된 시나리오에 대한 분석을 실행한다.
 * 시나리오 상태는 시나리오 별로 관리하지 않고, 시나리오에 속한 스케쥴의 상태로 관리한다.
 * 00-98 까지 단계가 있으며, 스크립트가 연속적으로 존재하면 단계별로 실행시켜 준다.
 *
 * @param scenarioId
 */
class ScenarioRunner(scenarioId: String) extends Runnable with ScheduleState with WorkflowState {

  val logger = LoggerFactory.getLogger(getClass)

  val cf = ConfigFactory.load()

  val wfCommandRoot = cf.getString("wf-scenario.command-root")


  override def run(): Unit = {

    val con = OracleCon.getCon()

    implicit val stmt = con.createStatement()

    try {
      // 시나리오에 속한 모든 스케쥴의 진행 상태로 분석진행중으로 변경한다.
      MyEnv.updateScenarioState(scenarioId, SCHEDULE_STATE_ONGOING._1, SCHEDULE_STATE_ONGOING._2)

      // 강제 재시작을 위한 청소
      MyEnv.deleteScenarioStep(scenarioId)
      MyEnv.deleteScheduleStepByScenario(scenarioId)

      // 마지막 단계 99 가 아니면 1씩 증가시키면서 다음 단계의 스크립트가 있는 경우 다음 단계를 실행한다.
      // 단계는 00-99 까지 있을 수 있으며, 00 단계의 스크립트는 반드시 존재해야 한다.
      // 99 단계는 종료단계로 스크립트가 실행되지 않는다.

      // 시작 단계 설정
      MyEnv.insertScenarioStep(scenarioId, WORKFLOW_STARTED)


      var complete = false
      var failed = false

      while (!complete) {
        val state = MyEnv.selectScenarioStep(scenarioId)

        try {
          val command = s"${wfCommandRoot}/${state} ${scenarioId}"
          //      val res = Process(command).lineStream

          logger.info(command)

          // 워크플로 커맨드 실행
          val exitCode = command !

          if (exitCode != 0) logger.warn(s"실행결과코드가 0이 아님: $exitCode")

          val nextState = f"${state.toInt + 1}%02d"

          val f = new File(s"$wfCommandRoot/$nextState")

          // 시나리오의 다음 단계를 설정한다.
          val finalNextState =
            if (f.exists()) {
              nextState
            } else {
              "99"
            }
          MyEnv.updateScenarioStep(scenarioId, finalNextState, "")

          complete = finalNextState == WORKFLOW_STOPPED
        } catch {
          case e: Throwable =>
            logger.warn(e.getLocalizedMessage, e)
            MyEnv.updateScenarioStep(scenarioId, WORKFLOW_STOPPED, e.getLocalizedMessage)
            complete = true
            failed = true
        }

        // todo 필요 없을지도
        if (!complete) {
          Thread.sleep(1000)
        }
      }

      // 분석실패 처리
      // 단계 실행 도중 실패가 있었던 경우, 시나리오에 속한 모든 스케쥴의 상태를 실패 상태로 변경하고 바로 종료한다.
      if (failed)
        MyEnv.updateScenarioState(scenarioId, SCHEDULE_STATE_FAILED._1, SCHEDULE_STATE_FAILED._2)

    } catch {
      case e: Throwable =>
        logger.error(e.getLocalizedMessage, e)
    } finally {
      try { stmt.close() }
      try { con.close() }
    }
  }

}
