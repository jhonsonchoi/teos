package com.sccomz.wf

import java.sql.Statement

import com.sccomz.etl.extract.oracle.sql.ExtractOraScenarioNrRuSql
import com.sccomz.schedule.real.{ExecuteJobSql, ScheduleDaemonSql}
import com.sccomz.serialize.{MakeBfBinFileSql, MakeBinFileSql}
import org.slf4j.LoggerFactory

import scala.collection.mutable.ListBuffer

object MyEnv {

  val logger = LoggerFactory.getLogger(getClass)

  val SCENARIO_TYPE_SIMPLE = "simple"
  val SCENARIO_TYPE_COMPLEX = "complex"

  def getScenario(scenarioId: String)(implicit stmt: Statement): Scenario = {
    val qry = s"SELECT * FROM SCENARIO WHERE SCENARIO_ID = ${scenarioId}"

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val userId = rs.getString("USER_ID")
    //      val wfId = rs.getString("SCHEDULE_ID")

    val scenario = Scenario(scenarioId, userId)

    rs.close()

    scenario
  }

  /**
   * 분석요청 중인 시나리오 목록
   *
   * @param stmt
   * @return
   */
  def selectScenarioReqs(state: String = "10001")(implicit stmt: Statement): List[ScenarioReq] = {
    val reqs = ListBuffer.empty[ScenarioReq]

    val qry = s"SELECT SCENARIO_ID FROM SCHEDULE WHERE PROCESS_CD = ${state} GROUP BY SCENARIO_ID"

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val scenarioId = rs.getString("SCENARIO_ID")
//      val wfId = rs.getString("SCHEDULE_ID")

      reqs += ScenarioReq(scenarioId)
    }

    rs.close()

    reqs.toList
  }

  /**
   * 사용자가 지정한 시나리오 목록을 바탕으로 존재 검증한 시나리오 목록
   *
   * @param scenarioIds
   * @param stmt
   * @return
   */
  def selectScenarioReqs(scenarioIds: Array[String])(implicit stmt: Statement): List[ScenarioReq] = {
    val reqs = ListBuffer.empty[ScenarioReq]

    val qry = s"SELECT SCENARIO_ID FROM SCENARIO WHERE SCENARIO_ID IN (${scenarioIds.mkString(",")})"

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val scenarioId = rs.getString("SCENARIO_ID")
//      val wfId = rs.getString("SCHEDULE_ID")
      reqs += ScenarioReq(scenarioId)
    }

    rs.close()

    reqs.toList
  }

  /**
   * 분석요청 중인 스케쥴 목록
   *
   * 스케쥴이 분석중이면서 시나리오 스텝이 99인 스케쥴
   *
   * @param stmt
   * @return
   */
  def selectScheduleReqs(scheduleState: String)(implicit stmt: Statement): List[ScheduleReq] = {
    val reqs = ListBuffer.empty[ScheduleReq]

    // 분석중인 스케쥴 목록
    val qry =
      s"""
        |SELECT A.SCHEDULE_ID SCHEDULE_ID -- , A.PROCESS_CD SCHEDULE_PROCESS_CD, C.TYPE_STEP_CD SCHEDULE_TYPE_STEP_CD, B.TYPE_STEP_CD SCENARIO_TYPE_STEP_CD
        |FROM SCHEDULE A
        |     JOIN SCENARIO_STEP B ON A.SCENARIO_ID = B.SCENARIO_ID AND B.TYPE_STEP_CD = '99'
        |     LEFT OUTER JOIN SCHEDULE_STEP C ON A.SCHEDULE_ID = C.SCHEDULE_ID
        |WHERE A.PROCESS_CD = ${scheduleState} AND C.TYPE_STEP_CD IS NULL -- AND A.SCHEDULE_ID = 8463307
        |""".stripMargin

    logger.info(qry)


    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val scenarioId = rs.getString("SCHEDULE_ID")
      //      val wfId = rs.getString("SCHEDULE_ID")

      reqs += ScheduleReq(scenarioId)
    }

    rs.close()

    reqs.toList
  }

  /**
   * 사용자가 지정한 스케쥴 목록을 바탕으로 존재 검증한 스케쥴 목록
   *
   * @param scheduleIds
   * @param stmt
   * @return
   */
  def selectScheduleReqs(scheduleIds: Array[String])(implicit stmt: Statement): List[ScheduleReq] = {
    val reqs = ListBuffer.empty[ScheduleReq]

    val qry = s"SELECT SCHEDULE_ID FROM SCHEDULE WHERE SCHEDULE_ID IN (${scheduleIds.mkString(",")})"

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
//      val scenarioId = rs.getString("SCENARIO_ID")
      val scheduleId = rs.getString("SCHEDULE_ID")

      reqs += ScheduleReq(scheduleId)
    }

    rs.close()

    reqs.toList
  }

  def selectRichReq(scheduleId: String)(implicit stmt: Statement): RichScenarioReq = {
    val qry = ScheduleDaemonSql.selectBinRuCount(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val rrq = RichScenarioReq(rs.getString("SCHEDULE_ID"), rs.getString("SCENARIO_ID"), rs.getInt("BIN_X_CNT"), rs.getInt("BIN_Y_CNT"), rs.getString("AREA"), rs.getInt("RU_CNT"), rs.getString("JOB_WEIGHT"), rs.getString("JOB_THRESHOLD"))

    rs.close()

    rrq
  }

  def selectScenarioType(scenarioId: String)(implicit stmt: Statement): String = {
    val qry = s"SELECT COUNT(*) FROM SCHEDULE WHERE TYPE_CD = 'SC051' AND SCENARIO_ID = $scenarioId"

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val count = rs.getInt(1)

    rs.close()

    if (count > 0) SCENARIO_TYPE_COMPLEX else SCENARIO_TYPE_SIMPLE
  }

  def updateScenarioState(scenarioId: String, processCd: String, processMsg: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.updateScenarioState(scenarioId, processCd, processMsg)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def updateScheduleState(scheduleId: String, processCd: String, processMsg: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.updateScheduleProcessCd(scheduleId, processCd, processMsg)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def selectScenarioStep(scenarioId: String)(implicit stmt: Statement): String = {
    val qry = ExecuteJobSql.selectScenarioStep(scenarioId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val state = rs.getString("TYPE_STEP_CD")

    rs.close()

    state
  }

  def insertScenarioStep(scenarioId: String, state: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.insertScenarioStep(scenarioId, state)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def updateScenarioStep(scenarioId: String, state: String, message: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.updateScenarioStep(scenarioId, state, message)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def deleteScenarioStep(scenarioId: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.deleteScenarioStep(scenarioId)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def selectScheduleStep(scheduleId: String)(implicit stmt: Statement): String = {
    val qry = ExecuteJobSql.selectScheduleStep(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val state = rs.getString("TYPE_STEP_CD")

    rs.close()

    state
  }

  def insertScheduleStep(scheduleId: String, state: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.insertScheduleStep(scheduleId, state)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def updateScheduleStep(scheduleId: String, state: String, message: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.updateScheduleStep(scheduleId, state, message)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def deleteScheduleStep(scheduleId: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.deleteScheduleStep(scheduleId)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def deleteScheduleStepByScenario(scenarioId: String)(implicit stmt: Statement): Int = {
    val qry = ExecuteJobSql.deleteScheduleStepByScenario(scenarioId)

    logger.info(qry)

    stmt.executeUpdate(qry)
  }

  def getSchedule(scheduleId: String)(implicit stmt: Statement): Schedule = {
    val qry = ExecuteJobSql.getSchedule(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val schedule = Schedule(scheduleId, rs.getString("SCENARIO_ID"), rs.getString("TYPE_CD"))

    rs.close()

    schedule
  }

  def selectSchedule(scheduleId: String)(implicit stmt: Statement): MySchedule = {
    val qry = ExecuteJobSql.getSchedule(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val schedule = MySchedule(scheduleId, rs.getString("SCENARIO_ID"), rs.getString("TYPE_CD"))

    rs.close()

    schedule
  }

  def selectSchedules(scenarioId: String)(implicit stmt: Statement): List[MySchedule] = {
    val lb = ListBuffer.empty[MySchedule]

    val qry = ExecuteJobSql.selectSchedulesInfo(scenarioId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val scheduleId = rs.getString("SCHEDULE_ID")
      val typeCode = rs.getString("TYPE_CD")

      lb += MySchedule(scheduleId, scenarioId, typeCode)
    }

    rs.close()

    lb.toList
  }


  def prepareScheduleRun(scheduleId: String, resultPath: String, xSize: Int, ySize: Int, ruCount: Int)(implicit stmt: Statement): Unit = {
    val qry = ScheduleDaemonSql.updateScheduleBinRuCnt(scheduleId, resultPath, xSize, ySize, ruCount)

    logger.info(qry)

    stmt.execute(qry)
  }

  // select * from SCENARIO_NR_RU WHERE  SCENARIO_ID = '5116767';
  def countRU(scenarioId: String)(implicit stmt: Statement): Int = {
    val qry =
      s"""
        |SELECT COUNT(*)
        |FROM SCENARIO_NR_RU
        |WHERE SCENARIO_ID = '${scenarioId}'
        |""".stripMargin

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val count = rs.getInt(1)

    rs.close()

    count
  }
  /**
   * Postgres
   *
   * @param scenarioId
   * @param stmt
   * @return
   */
  def checkGISJobState(scenarioId: String, state: Int)(implicit stmt: Statement): List[GISJob] = {
    val rus = ListBuffer.empty[GISJob]

    val qry =
      s"""
        |SELECT *
        |FROM JOB_DIS A
        |WHERE A.STAT = $state AND A.SCENARIO_ID = '$scenarioId'
        |""".stripMargin

    println(qry)

    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      val state =
        rus += GISJob(rs.getString("job_id"), rs.getString("ru_id"), rs.getInt("stat"), rs.getString("cluster_name"))
    }

    rs.close()

    rus.toList
  }

  def getScheduleSize(scheduleId: String)(implicit stmt: Statement): ScheduleSize = {
    val qry = MakeBinFileSql.selectBinCnt(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val size = ScheduleSize(scheduleId, rs.getInt("BIN_X_CNT"), rs.getInt("BIN_Y_CNT"))

    rs.close()

    size
  }

  def getResolution(scheduleId: String)(implicit stmt: Statement): Int = {
    val qry = MakeBfBinFileSql.selectResolution(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val resolution = rs.getInt("RESOLUTION")

    rs.close()

    resolution
  }

  def getPenetrationLoss(scheduleId: String)(implicit stmt: Statement): Int = {
    val qry = MakeBfBinFileSql.selectPenetrationLoss(scheduleId)

    logger.info(qry)

    val rs = stmt.executeQuery(qry)

    rs.next()

    val penetrationLoss = rs.getInt("PENETRATIONLOSS")

    rs.close()

    penetrationLoss
  }

  def makeResultPath(scenarioId: String)(implicit stmt: Statement): Map[String, MyRU] = {
    // scenario_nr_ru 에서 ru 정보 취득
    var ruInfo = Map.empty[String, MyRU]
    /*
    B.SCHEDULE_ID
     , A.X_BIN_CNT
     , A.Y_BIN_CNT
     , B.USER_ID
     , A.ENB_ID
     , A.PCI
     , A.PCI_PORT
     , A.RU_ID

     */
//    val qry = MakeBinFileSql.selectRUBinFilePath(scheduleId)
    val qry = ExtractOraScenarioNrRuSql.selectScenarioNrRu(scenarioId)
    println(qry)
    val rs = stmt.executeQuery(qry)

    while (rs.next()) {
      // RU 정보 생성
      ruInfo += (rs.getString("RU_ID") -> MyRU(rs.getString("RU_ID"), rs.getInt("X_BIN_CNT"), rs.getInt("Y_BIN_CNT"), rs.getString("ENB_ID"), rs.getString("PCI"), rs.getString("PCI_PORT")))
    }

    rs.close()

    ruInfo
  }

}

case class Scenario(id: String, userId: String)

case class Schedule(id: String, scenarioId: String, scheduleType: String)

case class ScenarioReq(scenarioId: String)

case class RichScenarioReq(scheduleId: String, scenarioId: String, binXCnt: Int, binYCnt: Int, area: String, ruCnt: Int, jobWeight: String, jobThreshold: String)

case class ScheduleReq(scheduleId: String)

case class MySchedule(id: String, scenarioId: String, scheduleType: String)

case class MyScenario(id: String, schedules: List[MySchedule])

case class MyRU(id: String, xBinCount: Int, yBinCount: Int, enbId: String, pci: String, pciPort: String)

case class GISJob(id: String, ruId: String, state: Int, place: String)

case class ScheduleSize(scheduleId: String, binXCnt: Int, binYCnt: Int)