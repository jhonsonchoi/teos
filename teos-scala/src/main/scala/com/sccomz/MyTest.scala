package com.sccomz

import java.sql.{Connection, DriverManager, Statement}

import com.sccomz.schedule.real.ScheduleDaemonSql
import com.typesafe.config.ConfigFactory

object MyTest {

  def main(args: Array[String]): Unit = {
    val cf = ConfigFactory.load()


    val url = s"jdbc:oracle:thin:@${cf.getString("oracle.host")}:${cf.getString("oracle.port")}/${cf.getString("oracle.service")}"
    val user = cf.getString("oracle.user")
    val password = cf.getString("oracle.password")

    println(s"url=$url, user=$user, password=$password")

    val con = DriverManager.getConnection(url, user, password)
    val stat = con.createStatement()

    val qry = ScheduleDaemonSql.selectSchedule10001()

    println(s"sql=$qry")

    val rs = stat.executeQuery(qry)

    while (rs.next()) {
      val scheduleId = rs.getString("SCHEDULE_ID")
      val scenarioId = rs.getString("SCENARIO_ID")
      println(s"SCHEDULE_ID=$scheduleId : SCENARIO_ID=$scenarioId")

    }
  }
}
