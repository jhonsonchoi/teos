개요

Spark 를 활용한 EOS 분석 애플리케이션에 관한 설명임.

분석 공간에 따라 2종류로 나뉘어져 있으며,
2D 분석 시나리오는 1개의 스케쥴, 3D 분석 시나리오는 2 개의 스케쥴과 관련 있음.

시나리오 단위로 분석을 시작하지만 내부적으로 2개의 스케쥴이 병렬로 실행될 수 있음.

따라서, 시나리오 단위의 공통 준비 단계와 개별 스케쥴 단위의 실행 단계로 나눴으며, 각각이 잡 스케쥴링 되는 구조임.
시나리오 분석 준비 => 스케쥴 분석 

워크플로우는 단계별 잡을 차례대로 실행함.
워크플로우는 00 ~ 98 까지의 단계를 제공함. 연속적인 단계만 실행됨에 주의. 

실제, 시나리오 분석 준비 워크플로우는 00, 01 두 단계로 이루어져 있으며, 
스케쥴 분석 워크플로우는 00 한 단계로 이루어져 있음.  


추가적인 참고 사항

3D 시나리오의 3D 스케쥴의 경우, RU 별 SINR 분석 단계에서 스파크 내부 아키텍쳐 문제로 데이터를 처리할 수 없어, Spark 프로세스에서
떼어내 Hive 로 처리함
Spark 프로세스 => Hive 프로세스 => Spark 프로세스의 복잡한 3단계 형태임.



분석 애플리케이션 디렉토리 레이아웃

디렉토리 구조는 아래와 같으며, /home/icpap/t-eos 를 루트로 삼아 애플리케이션을 설치해 놓았음.

참고사항: 실제 디렉토리에는 스펙에 없는 파일이 존재할 수 있는데, 테스트용으로 임시로 만든 파일이므로 무시해도 좋음.

<애플리케이션 홈> 			                애플리케이션 홈 디렉토리
    scenario                                시나리오 분석 준비
        bin                                 bash 스크립트 위주의 실행파일 디렉토리
            wf.sh                           시나리오 분석 준비 데몬 실행 스크립트
            00  -> set-scenario-path        분석 준비 워크플로의 첫번째 실행 스크립트: bin 파일 폴더 설정
            01  -> run-scenario             분석 준비 워크플로의 두번째 실행 스크립트
            set-scenario-path               bin 파일 위치 설정
            run-scenario                    PostGIS 에 분석 요청. Hive에 분석에 필요한 파일 ETL.
        lib     -> ../lib 
        conf    -> ../conf
    schedule                                스케쥴 분석
        bin                                 bash 스크립트 위주의 실행파일 디렉토리
            wf.sh                           스케쥴 분석 데몬 실행 스크립트
            00  -> run-schedule             분석 워크플로의 첫번째 실행 스크립트
            run-schedule                    스케쥴 분석: LOS 부터 bin 파일 생성까지의 분석.
            sinr-ru.sql                     Hive 실행용 SQL.
        lib     -> ../lib 
        conf    -> ../conf
    conf                                    공통 애플리케이션 설정 파일 디렉토리
        application.conf                    애플리케이션 설정 파일
        spark.conf                          spark 설정 파일
    lib                                     공통 애플리케이션 라이브러리 디렉토리
        config-1.3.3.jar
        hiveJdbc11.jar
        log4j-1.2.17.jar
        ojdbc7.jar
        postgresql4.jar
        scala-library-2.11.8.jar
        slf4j-api-1.7.16.jar
        slf4j-log4j12-1.7.16.jar
        teos-java-1.0-SNAPSHOT.jar
        teos-scala-1.0-SNAPSHOT.jar         애플리케이션 메인 패키지
          


분석 단계별 수동 실행

시나리오 분석 준비 워크플로우 각 단계의 수동 실행 방법

$ cd t-eos/scenario
$ bin/00 <scenario_id>
$ bin/01 <scenario_id>

스케쥴 분석 워크플로우 각 단계의 수동 실행 방법

$ cd t-eos/schedule
$ bin/00 <schedule_id>



워크플로우 데몬 실행 관리

시나리오 분석 준비 워크플로우 데몬 실행방법

$ cd t-eos/scenario
$ bin/wf.sh

스케쥴 분석 워크플로우 데몬 실행

$ cd t-eos/schedule
$ bin/wf.sh            


워크플로우 데몬 종료방법

각 프로세스 아이디를 구해서, 정지시켜야 함

$ ps aux | grep MyScenarioDaemon		
$ kill -9 <pid>             		

$ ps aux | grep MyScheduleDaemon		
$ kill -9 <pid>             		
