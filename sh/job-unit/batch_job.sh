HOME=/home/icpap/jhonson
LIB=$HOME/lib
CONF=$HOME/conf

WF_ID=$1
SCENARIO_ID=$2

# postAvgToHDFS method
spark2-submit --jars $LIB/ojdbc7.jar,$LIB/postgresql4.jar,$LIB/config-1.3.3.jar --class com.sccomz.etl.load.LoadHdfsLosManager $LIB/teos-scala-1.0-SNAPSHOT.jar $WF_ID $SCENARIO_ID

