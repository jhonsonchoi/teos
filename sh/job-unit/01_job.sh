HOME=/home/icpap/jhonson
LIB=$HOME/lib
CONF=$HOME/conf

WF_ID=$1
SCENARIO_ID=$2

java -cp $LIB/teos-scala-1.0-SNAPSHOT.jar:$LIB/scala-library-2.11.8.jar:$LIB/teos-java-1.0-SNAPSHOT.jar:$LIB/ojdbc7.jar:$LIB/slf4j-api-1.7.16.jar:$LIB/slf4j-log4j12-1.7.16.jar:$LIB/log4j-1.2.17.jar:$LIB/config-1.3.3.jar:$CONF com.sccomz.scenario.ETLOracleToHDFS $WF_ID $SCENARIO_ID

export HADOOP_USER=icpap

spark2-submit --jars $LIB/ojdbc7.jar --class com.sccomz.schedule.job.SparkOracleToHDFS $LIB/teos-scala-1.0-SNAPSHOT.jar $WF_ID $SCENARIO_ID
